-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 19, 2020 at 12:27 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `madvision`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_designation` varchar(50) NOT NULL,
  `trans_type_id` int(11) NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `categorie_ibfk_1` (`trans_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`cat_id`, `cat_designation`, `trans_type_id`) VALUES
(1, 'Mvola', 2),
(2, 'Jirama', 2),
(3, 'Hofan-trano', 2),
(4, 'Karama', 2),
(5, 'Autre', 2);

-- --------------------------------------------------------

--
-- Table structure for table `centre`
--

DROP TABLE IF EXISTS `centre`;
CREATE TABLE IF NOT EXISTS `centre` (
  `centre_id` int(2) NOT NULL AUTO_INCREMENT,
  `centre_nom` varchar(25) NOT NULL,
  PRIMARY KEY (`centre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `depense`
--

DROP TABLE IF EXISTS `depense`;
CREATE TABLE IF NOT EXISTS `depense` (
  `depense_id` int(11) NOT NULL AUTO_INCREMENT,
  `depense_design` varchar(255) NOT NULL,
  `depense_date` date NOT NULL,
  `centre_id` int(11) NOT NULL,
  PRIMARY KEY (`depense_id`),
  KEY `centre_id` (`centre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `eleve`
--

DROP TABLE IF EXISTS `eleve`;
CREATE TABLE IF NOT EXISTS `eleve` (
  `eleve_id` int(7) NOT NULL AUTO_INCREMENT,
  `eleve_nom` varchar(50) NOT NULL,
  `eleve_prenom` varchar(50) DEFAULT NULL,
  `eleve_date_naiss` date NOT NULL,
  `eleve_lieu_naiss` varchar(50) NOT NULL,
  `eleve_adresse` varchar(100) NOT NULL,
  `eleve_tel` varchar(10) DEFAULT NULL,
  `sexe` varchar(5) NOT NULL,
  `eleve_img` varchar(255) DEFAULT NULL,
  `date_rentree` date NOT NULL,
  `terminer` varchar(25) DEFAULT NULL,
  `groupe_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`eleve_id`),
  KEY `groupe_id` (`groupe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `eleve`
--

INSERT INTO `eleve` (`eleve_id`, `eleve_nom`, `eleve_prenom`, `eleve_date_naiss`, `eleve_lieu_naiss`, `eleve_adresse`, `eleve_tel`, `sexe`, `eleve_img`, `date_rentree`, `terminer`, `groupe_id`) VALUES
(2, 'RAKOTO', 'Jean Paul', '2019-04-06', 'Toliara', 'Fianarantsoa', '0341000101', 'Homme', NULL, '2019-04-05', 'OK', NULL),
(3, 'NATACHA', 'Romanoff', '1994-09-23', 'Fianarantsoa', 'Fianarantsoa', '0341056356', 'Femme', NULL, '2019-04-05', 'OK', NULL),
(4, 'BRUCE', 'Willis', '1990-01-17', 'Ambalapaiso', 'Ambalapaiso', '0340580001', 'Homme', NULL, '2019-04-05', 'OK', NULL),
(5, 'TONI', 'Stark', '1990-04-04', 'Ambalavao', 'Ambalapaiso', '0340580002', 'Homme', NULL, '2019-04-05', 'OK', NULL),
(6, 'RABE', 'Jean Claude', '2019-04-19', 'Talata Ampano', 'Andrainjato', '', 'Homme', NULL, '2019-04-19', 'OK', NULL),
(8, 'TEST', 'TEST', '1994-03-07', 'TEST', 'TEST', '', 'Homme', NULL, '2020-02-19', 'OK', NULL),
(9, 'RABEKOTO', 'Test', '2020-02-17', 'Ambositra', 'Fianarantsoa', '', 'Femme', NULL, '2020-02-21', NULL, NULL),
(10, 'test', 'test', '2004-07-07', 'test', 'test', '', 'Homme', NULL, '2020-02-19', 'OK', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `formation`
--

DROP TABLE IF EXISTS `formation`;
CREATE TABLE IF NOT EXISTS `formation` (
  `form_id` int(2) NOT NULL AUTO_INCREMENT,
  `form_nom` varchar(25) NOT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `formation`
--

INSERT INTO `formation` (`form_id`, `form_nom`) VALUES
(1, 'Informatique bureautique'),
(2, 'Coiffure'),
(3, 'Coupe et couture'),
(4, 'Français');

-- --------------------------------------------------------

--
-- Table structure for table `formationeleve`
--

DROP TABLE IF EXISTS `formationeleve`;
CREATE TABLE IF NOT EXISTS `formationeleve` (
  `fe_id` int(2) NOT NULL AUTO_INCREMENT,
  `type_id` int(2) NOT NULL,
  `form_id` int(2) NOT NULL,
  `eleve_id` int(7) NOT NULL,
  `certificat` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fe_id`),
  KEY `formationtype_ibfk_1` (`type_id`),
  KEY `form_id` (`form_id`),
  KEY `eleve_id` (`eleve_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `formationeleve`
--

INSERT INTO `formationeleve` (`fe_id`, `type_id`, `form_id`, `eleve_id`, `certificat`) VALUES
(2, 1, 1, 2, 1),
(3, 1, 1, 3, 1),
(4, 1, 1, 4, 1),
(5, 1, 1, 5, 1),
(6, 1, 1, 6, 1),
(8, 1, 1, 8, 1),
(9, 1, 1, 9, 0),
(10, 1, 1, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `groupe_id` int(2) NOT NULL AUTO_INCREMENT,
  `groupe_libelle` varchar(25) NOT NULL,
  PRIMARY KEY (`groupe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `livre`
--

DROP TABLE IF EXISTS `livre`;
CREATE TABLE IF NOT EXISTS `livre` (
  `livre_id` int(11) NOT NULL AUTO_INCREMENT,
  `livre_date` date NOT NULL,
  `livre_qte` int(11) NOT NULL,
  `livre_stock` int(11) NOT NULL,
  `vendu` tinyint(1) NOT NULL DEFAULT '0',
  `arret` tinyint(1) NOT NULL DEFAULT '0',
  `prix` int(11) NOT NULL DEFAULT '10000',
  PRIMARY KEY (`livre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `livre`
--

INSERT INTO `livre` (`livre_id`, `livre_date`, `livre_qte`, `livre_stock`, `vendu`, `arret`, `prix`) VALUES
(5, '2020-02-13', 10, 0, 1, 1, 10000),
(6, '2020-02-13', 20, 19, 1, 1, 10000),
(8, '2020-02-14', 9, 6, 0, 0, 10000);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `module_id` int(2) NOT NULL AUTO_INCREMENT,
  `module_nom` varchar(50) NOT NULL,
  `form_id` int(2) NOT NULL,
  PRIMARY KEY (`module_id`),
  KEY `module_ibfk_1` (`form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`module_id`, `module_nom`, `form_id`) VALUES
(18, 'Bureautique', 1),
(19, 'Bur+ et Internet', 1),
(20, 'Bureautique', 1);

-- --------------------------------------------------------

--
-- Table structure for table `moduletype`
--

DROP TABLE IF EXISTS `moduletype`;
CREATE TABLE IF NOT EXISTS `moduletype` (
  `module_id` int(2) NOT NULL,
  `type_id` int(2) NOT NULL,
  `droit` int(7) NOT NULL,
  KEY `module_id` (`module_id`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `moduletype`
--

INSERT INTO `moduletype` (`module_id`, `type_id`, `droit`) VALUES
(18, 1, 20000),
(19, 1, 25000),
(20, 2, 60000);

-- --------------------------------------------------------

--
-- Table structure for table `solde`
--

DROP TABLE IF EXISTS `solde`;
CREATE TABLE IF NOT EXISTS `solde` (
  `solde_id` int(11) NOT NULL AUTO_INCREMENT,
  `solde_init` int(10) NOT NULL DEFAULT '0',
  `solde_date` date NOT NULL,
  PRIMARY KEY (`solde_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `solde`
--

INSERT INTO `solde` (`solde_id`, `solde_init`, `solde_date`) VALUES
(1, 100000, '2019-04-17'),
(2, 200000, '2019-04-17'),
(3, 100000, '2019-04-22'),
(4, 120000, '2019-09-26'),
(5, 120000, '2019-09-26'),
(6, 20000, '2020-02-17'),
(7, 100000, '2020-02-17'),
(8, 200000, '2020-02-17'),
(9, 0, '2020-02-19'),
(10, 5000, '2020-02-19');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_design` varchar(100) NOT NULL,
  `trans_date` date NOT NULL,
  `trans_montant` int(10) NOT NULL,
  `arret` tinyint(1) NOT NULL DEFAULT '0',
  `trans_type_id` int(11) NOT NULL,
  PRIMARY KEY (`trans_id`),
  KEY `trans_type_id` (`trans_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`trans_id`, `trans_design`, `trans_date`, `trans_montant`, `arret`, `trans_type_id`) VALUES
(1, 'designation_', '2019-04-17', 50000, 1, 1),
(2, 'Boky x5', '2019-04-17', 50000, 1, 1),
(3, 'Désignation .....', '2019-04-17', 20000, 1, 2),
(4, 'Designation /', '2019-04-17', 15000, 1, 2),
(5, 'test_designation', '2019-04-17', 30000, 1, 2),
(6, 'Test_crédit', '2019-04-18', 3400, 1, 1),
(7, 'Mvola', '2019-04-18', 160000, 1, 1),
(8, 'Boky Info', '2019-04-18', 180000, 1, 2),
(9, 'Réparation ordi', '2019-04-18', 50000, 1, 1),
(10, 'test_debit', '2019-04-18', 80000, 1, 2),
(12, 'test 2', '2019-04-18', 300000, 1, 1),
(14, 'Mvola', '2019-10-28', 50000, 1, 2),
(15, 'Mvola', '2019-10-26', 13000, 1, 2),
(16, 'Jirama', '2019-10-26', 20000, 1, 2),
(17, 'Mvola', '2020-02-18', 50000, 1, 2),
(18, 'Mvola', '2020-02-19', 50000, 1, 2),
(19, 'Mvola', '2020-02-19', 10000, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `trans_type`
--

DROP TABLE IF EXISTS `trans_type`;
CREATE TABLE IF NOT EXISTS `trans_type` (
  `trans_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_type_nom` varchar(25) NOT NULL,
  PRIMARY KEY (`trans_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trans_type`
--

INSERT INTO `trans_type` (`trans_type_id`, `trans_type_nom`) VALUES
(1, 'Crédit'),
(2, 'Débit');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `type_id` int(2) NOT NULL AUTO_INCREMENT,
  `type_nom` varchar(25) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`type_id`, `type_nom`) VALUES
(1, 'Normal'),
(2, 'Particulier');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `login`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `versement`
--

DROP TABLE IF EXISTS `versement`;
CREATE TABLE IF NOT EXISTS `versement` (
  `vers_id` int(8) NOT NULL AUTO_INCREMENT,
  `vers_motif` varchar(255) NOT NULL,
  `vers_montant` int(8) NOT NULL,
  `vers_date` date NOT NULL,
  `num_recu` int(4) NOT NULL,
  `eleve_id` int(8) NOT NULL,
  `arret` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vers_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `versement`
--

INSERT INTO `versement` (`vers_id`, `vers_motif`, `vers_montant`, `vers_date`, `num_recu`, `eleve_id`, `arret`) VALUES
(1, 'Bureautique', 20000, '2019-04-11', 100, 2, 1),
(2, 'Bureautique', 15000, '2019-04-12', 101, 3, 1),
(3, 'Bur+ et Internet', 25000, '2019-04-12', 103, 2, 1),
(4, 'Bureautique', 5000, '2019-04-13', 108, 3, 1),
(7, 'Bur+ et Internet', 15000, '2019-04-13', 105, 3, 1),
(8, 'Bureautique', 30000, '2019-04-20', 106, 6, 1),
(9, 'Bureautique', 20000, '2019-04-20', 107, 4, 1),
(10, 'Bureautique', 20000, '2019-04-21', 109, 5, 1),
(11, 'Bur+ et Internet', 10000, '2019-04-24', 105, 3, 1),
(12, 'Bur+ et Internet', 25000, '2019-04-27', 110, 4, 1),
(13, 'Bur+ et Internet', 20000, '2019-12-03', 109, 5, 1),
(14, 'Bureautique', 25000, '2020-02-10', 200, 7, 1),
(15, 'Bureautique', 25000, '2020-02-16', 200, 8, 1),
(17, 'Bur+ et Internet', 20000, '2020-02-16', 200, 8, 1),
(18, 'Bur+ et Internet', 5000, '2020-02-17', 210, 5, 1),
(19, 'Bur+ et Internet', 15000, '2020-02-17', 206, 6, 1),
(20, 'Bureautique', 25000, '2020-02-17', 123, 9, 1),
(21, 'Bureautique', 25000, '2020-02-19', 340, 10, 1),
(22, 'Bur+ et Internet', 20000, '2020-02-19', 340, 10, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categorie`
--
ALTER TABLE `categorie`
  ADD CONSTRAINT `categorie_ibfk_1` FOREIGN KEY (`trans_type_id`) REFERENCES `trans_type` (`trans_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `depense`
--
ALTER TABLE `depense`
  ADD CONSTRAINT `depense_ibfk_1` FOREIGN KEY (`centre_id`) REFERENCES `centre` (`centre_id`);

--
-- Constraints for table `eleve`
--
ALTER TABLE `eleve`
  ADD CONSTRAINT `eleve_ibfk_1` FOREIGN KEY (`groupe_id`) REFERENCES `groupe` (`groupe_id`);

--
-- Constraints for table `formationeleve`
--
ALTER TABLE `formationeleve`
  ADD CONSTRAINT `formationeleve_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `type` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formationeleve_ibfk_2` FOREIGN KEY (`form_id`) REFERENCES `formation` (`form_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formationeleve_ibfk_3` FOREIGN KEY (`eleve_id`) REFERENCES `eleve` (`eleve_id`);

--
-- Constraints for table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `module_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `formation` (`form_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `moduletype`
--
ALTER TABLE `moduletype`
  ADD CONSTRAINT `moduletype_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `moduletype_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `type` (`type_id`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`trans_type_id`) REFERENCES `trans_type` (`trans_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
