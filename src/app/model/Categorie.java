/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

/**
 *
 * @author Finaritra
 */
public class Categorie {
    
    private int cat_id;
    private String cat_designation;
    
    private int trans_type_id = 1;

    public Categorie() {
    }    
    
    public Categorie(int cat_id, String nom_cat, int trans_type) {
        this.cat_id = cat_id;
        this.cat_designation = nom_cat;
        trans_type_id = trans_type;
    } 
    
    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_designation() {
        return cat_designation;
    }

    public void setCat_designation(String cat_designation) {
        this.cat_designation = cat_designation;
    }

    public int getTrans_type_id() {
        return trans_type_id;
    }

    public void setTrans_type_id(int trans_type_id) {
        this.trans_type_id = trans_type_id;
    }
    
}
