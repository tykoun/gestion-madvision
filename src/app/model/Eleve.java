package app.model;

import java.time.LocalDate;
import java.util.Date;

public class Eleve {
	private int eleve_id;
	private String eleve_nom;
	private String eleve_prenom;
	private Date eleve_date_naiss;
	private String eleve_lieu_naiss;
	private String eleve_adresse;
	private String eleve_tel;
	//private String eleve_img;
	private Date date_rentree;
	private Boolean terminer;
	private String sexe;
//	private int age;
	
	public Eleve(int eleve_id, String eleve_nom, String eleve_prenom, Date eleve_date_naiss, String eleve_lieu_naiss , String eleve_adresse,
			String eleve_tel, Date date_rentrée, Boolean terminer, String sexe) {
		this.eleve_id = eleve_id;
		this.eleve_nom = eleve_nom;
		this.eleve_prenom = eleve_prenom;
		this.eleve_date_naiss = eleve_date_naiss;
		this.eleve_lieu_naiss = eleve_lieu_naiss;
		this.eleve_adresse = eleve_adresse;
		this.eleve_tel = eleve_tel;
		this.date_rentree = date_rentrée;
		this.terminer = terminer;
		this.sexe = sexe;
	}


	public int getEleve_id() {
		return eleve_id;
	}

	public void setEleve_id(int eleve_id) {
		this.eleve_id = eleve_id;
	}

	public String getEleve_nom() {
		return eleve_nom;
	}

	public void setEleve_nom(String eleve_nom) {
		this.eleve_nom = eleve_nom;
	}

	public String getEleve_lieu_naiss() {
		return eleve_lieu_naiss;
	}


	public void setEleve_lieu_naiss(String eleve_lieu_naiss) {
		this.eleve_lieu_naiss = eleve_lieu_naiss;
	}


	public Date getDate_rentree() {
		return date_rentree;
	}


	public void setDate_rentree(Date date_rentree) {
		this.date_rentree = date_rentree;
	}


	public String getEleve_prenom() {
		return eleve_prenom;
	}

	public void setEleve_prenom(String eleve_prenom) {
		this.eleve_prenom = eleve_prenom;
	}

	public Date getEleve_date_naiss() {
		return eleve_date_naiss;
	}

	public void setEleve_date_naiss(Date eleve_date_naiss) {
		this.eleve_date_naiss = eleve_date_naiss;
	}

	public String getEleve_adresse() {
		return eleve_adresse;
	}

	public void setEleve_adresse(String eleve_adresse) {
		this.eleve_adresse = eleve_adresse;
	}

	public String getEleve_tel() {
		return eleve_tel;
	}

	public void setEleve_tel(String eleve_tel) {
		this.eleve_tel = eleve_tel;
	}

	public Date getDate_rentrée() {
		return date_rentree;
	}

	public void setDate_rentrée(Date date_rentrée) {
		this.date_rentree = date_rentrée;
	}

	public Boolean getTerminer() {
		return terminer;
	}

	public void setTerminer(Boolean terminer) {
		this.terminer = terminer;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

        public int getAge() {          
            return LocalDate.now().getYear() - eleve_date_naiss.getYear();
        }
}
