package app.model;

public class Type {
	private int type_id;
	private String type_nom;
	
	public Type(int type_id, String type_nom) {
		super();
		this.type_id = type_id;
		this.type_nom = type_nom;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	public String getType_nom() {
		return type_nom;
	}

	public void setType_nom(String type_nom) {
		this.type_nom = type_nom;
	}
	
	
}
