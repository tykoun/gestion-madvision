package app.model;

public class Module {
	private int module_id;
	private String module_nom;
	private int form_id;
	
	public Module(int module_id, String module_nom, int form_id) {
		super();
		this.module_id = module_id;
		this.module_nom = module_nom;
		this.form_id = form_id;
	}

	public int getModule_id() {
		return module_id;
	}

	public void setModule_id(int module_id) {
		this.module_id = module_id;
	}

	public String getModule_nom() {
		return module_nom;
	}

	public void setModule_nom(String module_nom) {
		this.module_nom = module_nom;
	}

	public int getForm_id() {
		return form_id;
	}

	public void setForm_id(int form_id) {
		this.form_id = form_id;
	}
	
	
}
