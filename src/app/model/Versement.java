package app.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Versement {
	private int vers_id;
	private int eleve_id;
	private int num_recu;
	private int vers_montant;
	private String vers_motif;
	private String vers_date;
	private String vers_date_formatted;
	private String form_nom;
	private String nom;
	private String noms;
	private String prenom;
	private String type;
	private boolean arret;
	
	public String getNoms() {
		return noms;
	}

	public void setNoms(String noms) {
		this.noms = noms;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Versement(int vers_id, int eleve_id, int num_recu, int vers_montant, String vers_motif, String vers_date,
			String form_nom, String nom, String prenom, String type_nom, boolean arret) {
		this.vers_id = vers_id;
		this.eleve_id = eleve_id;
		this.num_recu = num_recu;
		this.vers_montant = vers_montant;
		this.vers_motif = vers_motif;
		this.vers_date = vers_date;
		this.form_nom = form_nom;
		this.nom = nom;
		this.prenom = prenom;
		this.type = type_nom;
		this.noms = nom + " " + prenom;
                
                
                if(vers_date != null){
                    LocalDate dateR = LocalDate.parse(vers_date);
                    DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
                    String date = dateR.format(dateformatter);
                    this.vers_date_formatted = convert(date);
                } else {
                    LocalDate dateR = LocalDate.parse(LocalDate.now().toString());
                    DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
                    String date = dateR.format(dateformatter);
                    this.vers_date_formatted = convert(date);
                }
                this.arret = arret;
        }

        	public Versement(int vers_id, int eleve_id, int num_recu, int vers_montant, String vers_motif, String vers_date,
			String form_nom, String nom, String prenom, String type_nom) {
		this.vers_id = vers_id;
		this.eleve_id = eleve_id;
		this.num_recu = num_recu;
		this.vers_montant = vers_montant;
		this.vers_motif = vers_motif;
		this.vers_date = vers_date;
		this.form_nom = form_nom;
		this.nom = nom;
		this.prenom = prenom;
		this.type = type_nom;
		this.noms = nom + " " + prenom;
                
                
                if(vers_date != null){
                    LocalDate dateR = LocalDate.parse(vers_date);
                    DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
                    String date = dateR.format(dateformatter);
                    this.vers_date_formatted = convert(date);
                } else {
                    LocalDate dateR = LocalDate.parse(LocalDate.now().toString());
                    DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
                    String date = dateR.format(dateformatter);
                    this.vers_date_formatted = convert(date);
                }
        }

	public Versement(int vers_id, String vers_motif) {
		this.vers_id = vers_id;
		this.vers_motif = vers_motif;
	}
        
        static String convert(String str)
        {
        // Create a char array of given String
        char ch[] = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {
        // If first character of a word is found
        if (i == 0 && ch[i] != ' ' ||
        ch[i] != ' ' && ch[i - 1] == ' ') {
        // If it is in lower-case
        if (ch[i] >= 'a' && ch[i] <= 'z') {
        // Convert into Upper-case
        ch[i] = (char)(ch[i] - 'a' + 'A');
        }
        }
        // If apart from first character
        // Any one is in Upper-case
        else if (ch[i] >= 'A' && ch[i] <= 'Z')
        // Convert into Lower-Case
        ch[i] = (char)(ch[i] + 'a' - 'A');
        }
        // Convert the char array to equivalent String
        String st = new String(ch);
        return st;
        } 
        
	public int getVers_id() {
		return vers_id;
	}

	public void setVers_id(int vers_id) {
		this.vers_id = vers_id;
	}

	public int getEleve_id() {
		return eleve_id;
	}

	public void setEleve_id(int eleve_id) {
		this.eleve_id = eleve_id;
	}

	public int getNum_recu() {
		return num_recu;
	}

	public void setNum_recu(int num_recu) {
		this.num_recu = num_recu;
	}

	public int getVers_montant() {
		return vers_montant;
	}

	public void setVers_montant(int vers_montant) {
		this.vers_montant = vers_montant;
	}

	public String getVers_motif() {
		return vers_motif;
	}

	public void setVers_motif(String vers_motif) {
		this.vers_motif = vers_motif;
	}

	public String getVers_date() {
		return vers_date;
	}

	public void setVers_date(String vers_date) {
		this.vers_date = vers_date;
	}

	public String getForm_nom() {
		return form_nom;
	}

	public void setForm_nom(String form_nom) {
		this.form_nom = form_nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

        public String getVers_date_formatted() {
            return vers_date_formatted;
        }

        public boolean isArret() {
            return arret;
        }
        public String getArret() {
            if(arret == true) return "Arrêté";
            return "_";
        }

}
