package app.model;

public class FormationModule {
	private int form_id;
	private String form_nom;
	private int module_id;
	private String module_nom;
	private int type_id;
	private String type_nom;
	private String droit;
	
	public FormationModule(int form_id, String form_nom, int module_id, String module_nom, int type_id, String type_nom,
			String droit) {
		this.form_id = form_id;
		this.form_nom = form_nom;
		this.module_id = module_id;
		this.module_nom = module_nom;
		this.type_id = type_id;
		this.type_nom = type_nom;
		this.droit = droit;
	}

	public int getForm_id() {
		return form_id;
	}

	public void setForm_id(int form_id) {
		this.form_id = form_id;
	}

	public String getForm_nom() {
		return form_nom;
	}

	public void setForm_nom(String form_nom) {
		this.form_nom = form_nom;
	}

	public int getModule_id() {
		return module_id;
	}

	public void setModule_id(int module_id) {
		this.module_id = module_id;
	}

	public String getModule_nom() {
		return module_nom;
	}

	public void setModule_nom(String module_nom) {
		this.module_nom = module_nom;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	public String getType_nom() {
		return type_nom;
	}

	public void setType_nom(String type_nom) {
		this.type_nom = type_nom;
	}

	public String getDroit() {
		return droit;
	}

	public void setDroit(String droit) {
		this.droit = droit;
	}
		
}
