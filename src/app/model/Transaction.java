package app.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Transaction {

	private int trans_id;

	private String trans_design;

	private String trans_date_string;
	
	private LocalDate trans_date;

	private int trans_montant;
		
	private Boolean arret;
        
	private String arret_string = "-";

	private int typetrans_id;
	
	public Transaction(int trans_id, String trans_design, String trans_date_string, int trans_montant, Boolean arret,
			int typetrans_id) {
		DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
		this.trans_id = trans_id;
		this.trans_design = trans_design;
		LocalDate date = LocalDate.parse(trans_date_string);
		this.trans_date_string = date.format(dateformatter);
		this.trans_montant = trans_montant;
		this.arret = arret;
		this.typetrans_id = typetrans_id;
                this.trans_date = LocalDate.parse(trans_date_string);
	}

        public String getTrans_date_localToString() {
            DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date = this.trans_date;
            String toString = date.format(dateformatter);
            return toString;
	}
                
	public int getTrans_id() {
		return trans_id;
	}

	public void setTrans_id(int trans_id) {
		this.trans_id = trans_id;
	}

	public String getTrans_design() {
		return trans_design;
	}

	public void setTrans_design(String trans_design) {
		this.trans_design = trans_design;
	}

	public String getTrans_date_string() {
		return trans_date_string;
	}

	public void setTrans_date_string(String trans_date_string) {
		this.trans_date_string = trans_date_string;
	}

	public LocalDate getTrans_date() {
		return trans_date;
	}

	public void setTrans_date(LocalDate trans_date) {
		this.trans_date = trans_date;
	}

	public int getTrans_montant() {
		return trans_montant;
	}

	public void setTrans_montant(int trans_montant) {
		this.trans_montant = trans_montant;
	}

	public Boolean getArret() {
		return arret;
	}

	public void setArret(Boolean arret) {
		this.arret = arret;
	}

	public int getTypetrans_id() {
		return typetrans_id;
	}

	public void setTypetrans_id(int typetrans_id) {
		this.typetrans_id = typetrans_id;
	}

        public String getArret_string() {
            if(this.arret) return "OK";
            return arret_string;
        }

        public void setArret_string(String arret_string) {
            this.arret_string = arret_string;
        }
}
