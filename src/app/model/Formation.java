package app.model;

public class Formation {
	private int form_id;
	private String form_nom;
	
	public Formation(int form_id, String form_nom) {
		this.form_id = form_id;
		this.form_nom = form_nom;
	}

	public Formation(String form_nom) {
		this.form_nom = form_nom;
	}
	
	public int getForm_id() {
		return form_id;
	}


	public void setForm_id(int form_id) {
		this.form_id = form_id;
	}


	public String getForm_nom() {
		return form_nom;
	}


	public void setForm_nom(String form_nom) {
		this.form_nom = form_nom;
	}
	
	
}
