package app.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import app.utils.ConvertString;

public class FormationEleve {
	private int eleve_id;
	private String eleve_noms;
	private String eleve_nom;
	private String eleve_prenom;
	private String eleve_date_naiss;
	private String eleve_lieu_naiss;
	private String date_naiss_sql;
	private String eleve_adresse;
	private String eleve_tel;
	private String sexe;
	private int formation_id;
	private String formation_nom;
	private int type_id;
	private String type_nom;
	private String date_rentree;
	private String date_rentree_sql;
	private String terminer;
	private Boolean certificat;
	private String certificat_str;
	private Boolean annee_vers;
        
	public FormationEleve(int type_id, int form_id, int eleve_id) {
		this.type_id=type_id;
		this.formation_id=form_id;
		this.eleve_id=eleve_id;
	}
	
	public FormationEleve(int eleve_id, String eleve_noms, String eleve_nom, String eleve_prenom,
			String eleve_date_naiss, String eleve_lieu_naiss, String eleve_adresse, String eleve_tel, String eleve_sexe, int formation_id, String formation_nom, int type_id,
			String type_nom, String date_rentree, String terminer) {
		super();
		this.eleve_id = eleve_id;
		this.eleve_noms = eleve_noms;
		this.formation_id = formation_id;
		this.formation_nom = formation_nom;
		this.type_id = type_id;
		this.type_nom = type_nom;
    	DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    	LocalDate date = LocalDate.parse(date_rentree);
    	//date = 
		this.date_rentree = ConvertString.convert((date.format(dateformatter)));
		this.date_rentree_sql = date_rentree;
		this.date_naiss_sql = eleve_date_naiss;
		this.eleve_lieu_naiss = eleve_lieu_naiss;
		this.terminer = terminer;
		this.eleve_nom = eleve_nom;
		this.eleve_prenom = eleve_prenom;
		this.eleve_date_naiss = eleve_date_naiss;
		this.eleve_adresse = eleve_adresse;
		this.eleve_tel = eleve_tel;
		this.sexe = eleve_sexe;
	}
        public FormationEleve(int eleve_id, String eleve_noms, String eleve_nom, String eleve_prenom,
			String eleve_date_naiss, String eleve_lieu_naiss, String eleve_adresse, String eleve_tel, String eleve_sexe, int formation_id, String formation_nom, int type_id,
			String type_nom, String date_rentree, String terminer, Boolean eleve_certificat, Boolean eleve_annee_vers) {
		super();
		this.eleve_id = eleve_id;
		this.eleve_noms = eleve_noms;
		this.formation_id = formation_id;
		this.formation_nom = formation_nom;
		this.type_id = type_id;
		this.type_nom = type_nom;
    	DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    	LocalDate date = LocalDate.parse(date_rentree);
    	//date = 
		this.date_rentree = ConvertString.convert((date.format(dateformatter)));
		this.date_rentree_sql = date_rentree;
		this.date_naiss_sql = eleve_date_naiss;
		this.eleve_lieu_naiss = eleve_lieu_naiss;
		this.terminer = terminer;
		this.eleve_nom = eleve_nom;
		this.eleve_prenom = eleve_prenom;
		this.eleve_date_naiss = eleve_date_naiss;
		this.eleve_adresse = eleve_adresse;
		this.eleve_tel = eleve_tel;
		this.sexe = eleve_sexe;
		this.certificat = eleve_certificat;
                this.annee_vers = eleve_annee_vers;
	}
	public int getEleve_id() {
		return eleve_id;
	}

	public void setEleve_id(int eleve_id) {
		this.eleve_id = eleve_id;
	}

	public String getEleve_noms() {
		return eleve_nom + " " + eleve_prenom;
	}

	public void setEleve_noms(String eleve_nom) {
		this.eleve_noms = eleve_nom;
	}

	public int getFormation_id() {
		return formation_id;
	}

	public void setFormation_id(int formation_id) {
		this.formation_id = formation_id;
	}

	public String getFormation_nom() {
		return formation_nom;
	}

	public void setFormation_nom(String formation_nom) {
		this.formation_nom = formation_nom;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	public String getType_nom() {
		return type_nom;
	}

	public void setType_nom(String type_nom) {
		this.type_nom = type_nom;
	}

	public String getDate_rentree() {
		return date_rentree;
	}
	
	public String getEleve_nom() {
		return eleve_nom;
	}

	public void setEleve_nom(String eleve_nom) {
		this.eleve_nom = eleve_nom;
	}

	public String getEleve_prenom() {
		return eleve_prenom;
	}

	public void setEleve_prenom(String eleve_prenom) {
		this.eleve_prenom = eleve_prenom;
	}

	public String getEleve_date_naiss() {
		return eleve_date_naiss;
	}

	public void setEleve_date_naiss(String eleve_date_naiss) {
		this.eleve_date_naiss = eleve_date_naiss;
	}

	public String getEleve_adresse() {
		return eleve_adresse;
	}

	public void setEleve_adresse(String eleve_adresse) {
		this.eleve_adresse = eleve_adresse;
	}

	public String getEleve_lieu_naiss() {
		return eleve_lieu_naiss;
	}

	public void setEleve_lieu_naiss(String eleve_lieu_naiss) {
		this.eleve_lieu_naiss = eleve_lieu_naiss;
	}

	public String getEleve_tel() {
		return eleve_tel;
	}

	public void setEleve_tel(String eleve_tel) {
		this.eleve_tel = eleve_tel;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public void setDate_rentree(String date_rentree) {
    	DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    	LocalDate date = LocalDate.parse(date_rentree, dateformatter);
		this.date_rentree = date.toString();
	}

	public String getTerminer() {
		return terminer;
	}

	public void setTerminer(String terminer) {
		this.terminer = terminer;
	}

	public String getDate_naiss_sql() {
		return date_naiss_sql;
	}

	public void setDate_naiss_sql(String date_naiss_sql) {
		this.date_naiss_sql = date_naiss_sql;
	}

	public String getDate_rentree_sql() {
		return date_rentree_sql;
	}

	public void setDate_rentree_sql(String date_rentree_sql) {
		this.date_rentree_sql = date_rentree_sql;
	}

        public boolean isCertificat() {
            return certificat;
        }
        public String getCertificat() {
            return certificat.toString();
        }
        public void setCertificat(boolean certificat) {
            this.certificat = certificat;
        }	
        public int getAge() {          
            return LocalDate.now().getYear() - LocalDate.parse(date_naiss_sql).getYear();
        }

        public Boolean getAnnee_vers() {
            return annee_vers;
        }

        public void setAnnee_vers(Boolean annee_vers) {
            this.annee_vers = annee_vers;
        }
        public String getCertificat_str() {
            if(this.certificat) return "OK";
            else return "_";
        }

        public void setCertificat_str(String certificat_str) {
            this.certificat_str = certificat_str;
        }
}
