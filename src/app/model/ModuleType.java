package app.model;

public class ModuleType {
	private int module_id;
	private int type_id;
	private int droit;
	
	public ModuleType(int module_id, int type_id, int droit) {
		super();
		this.module_id = module_id;
		this.type_id = type_id;
		this.droit = droit;
	}

	public int getModule_id() {
		return module_id;
	}

	public void setModule_id(int module_id) {
		this.module_id = module_id;
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

	public int getDroit() {
		return droit;
	}

	public void setDroit(int droit) {
		this.droit = droit;
	}
	
	
	
}
