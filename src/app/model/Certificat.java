package app.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Certificat {
	private int eleve_id;
	private String nom;
	private String noms;
	private String prenom;
	private int form_id;
	private String eleve_date_naiss;
	private String date_lieu;
	public String getDate_lieu() {
		return date_lieu;
	}
	public void setDate_lieu(String date_lieu) {
		this.date_lieu = date_lieu;
	}
	private String eleve_lieu_naiss;
	private LocalDate eleve_date_rentree;
	private String date_rentree_string;
	private int type_id;
	private Boolean certificat;
	private String certificat_str;
	private String eleve_tel;
	private Boolean annee_vers;
	
	public String getDate_rentree_string() {
		return date_rentree_string;
	}
	public void setDate_rentree_string(String date_rentree_string) {
		this.date_rentree_string = date_rentree_string;
	}
	public Certificat(int eleve_id, String nom, String prenom, int form_id, String eleve_date_naiss,
			String eleve_lieu_naiss, String eleve_date_rentree, int type_id, Boolean certificat, String eleve_tel,
                        Boolean eleve_annee_vers) {
		this.eleve_id = eleve_id;
		this.nom = nom;
		this.prenom = prenom;
		this.form_id = form_id;
		this.eleve_date_naiss = eleve_date_naiss;
		this.eleve_lieu_naiss = eleve_lieu_naiss;
		this.date_rentree_string = eleve_lieu_naiss;
		this.eleve_date_rentree = LocalDate.parse(eleve_date_rentree);
		LocalDate dateR = LocalDate.parse(eleve_date_naiss);
		this.type_id = type_id;
		this.certificat = certificat;
		this.eleve_tel = eleve_tel;
		this.noms=nom+" "+prenom;
                DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
                String date = dateR.format(dateformatter);
                //this.date_lieu = convert(date)+" à "+eleve_lieu_naiss;
                this.annee_vers = eleve_annee_vers;
                if(eleve_annee_vers){
                    this.date_lieu = "Vers "+dateR.getYear()+" à "+eleve_lieu_naiss;
                } else this.date_lieu = convert(date)+" à "+eleve_lieu_naiss;
	}
	
    
    static String convert(String str)
    {
    // Create a char array of given String
    char ch[] = str.toCharArray();
    for (int i = 0; i < str.length(); i++) {
    // If first character of a word is found
    if (i == 0 && ch[i] != ' ' ||
    ch[i] != ' ' && ch[i - 1] == ' ') {
    // If it is in lower-case
    if (ch[i] >= 'a' && ch[i] <= 'z') {
    // Convert into Upper-case
    ch[i] = (char)(ch[i] - 'a' + 'A');
    }
    }
    // If apart from first character
    // Any one is in Upper-case
    else if (ch[i] >= 'A' && ch[i] <= 'Z')
    // Convert into Lower-Case
    ch[i] = (char)(ch[i] + 'a' - 'A');
    }
    // Convert the char array to equivalent String
    String st = new String(ch);
    return st;
    } 
    
	public String getNoms() {
		return noms;
	}
	public void setNoms(String noms) {
		this.noms = noms;
	}
	public int getEleve_id() {
		return eleve_id;
	}
	public void setEleve_id(int eleve_id) {
		this.eleve_id = eleve_id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getForm_id() {
		return form_id;
	}
	public void setForm_id(int form_id) {
		this.form_id = form_id;
	}
	public String getEleve_date_naiss() {
		return eleve_date_naiss;
	}
	public void setEleve_date_naiss(String eleve_date_naiss) {
		this.eleve_date_naiss = eleve_date_naiss;
	}
	public String getEleve_lieu_naiss() {
		return eleve_lieu_naiss;
	}
	public void setEleve_lieu_naiss(String eleve_lieu_naiss) {
		this.eleve_lieu_naiss = eleve_lieu_naiss;
	}
	public LocalDate getEleve_date_rentree() {
		return eleve_date_rentree;
	}
	public void setEleve_date_rentree(LocalDate eleve_date_rentree) {
		this.eleve_date_rentree = eleve_date_rentree;
	}
	public int getType_id() {
		return type_id;
	}
	public void setType_id(int type_id) {
		this.type_id = type_id;
	}
	public Boolean getCertificat() {
		return certificat;
	}
	public void setCertificat(Boolean certificat) {
		this.certificat = certificat;
	}
	public String getEleve_tel() {
		return eleve_tel;
	}
	public void setEleve_tel(String eleve_tel) {
		this.eleve_tel = eleve_tel;
	}
	
        public int getAge() {          
            return LocalDate.now().getYear() - LocalDate.parse(eleve_date_naiss).getYear();
        }

        public Boolean getAnnee_vers() {
            return annee_vers;
        }

        public void setAnnee_vers(Boolean annee_vers) {
            this.annee_vers = annee_vers;
        }

        public String getCertificat_str() {
            if(this.certificat) return "OK";
            else return "Non";
        }

        public void setCertificat_str(String certificat_str) {
            this.certificat_str = certificat_str;
        }
}
