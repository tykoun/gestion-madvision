/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.model;

import java.time.LocalDate;

/**
 *
 * @author Finaritra
 */
public class Livre {
    private int livre_id;
    private LocalDate livre_date;
    private int livre_qte;
    private int livre_stock;
    private boolean vendu;
    private boolean arret;
    private int pu;

    public Livre(int livre_id, LocalDate livre_date, int livre_qte, int livre_stock, boolean vendu, boolean arret, int pu) {
        this.livre_id = livre_id;
        this.livre_date = livre_date;
        this.livre_qte = livre_qte;
        this.livre_stock = livre_stock;
        this.vendu = vendu;
        this.arret = arret;
        this.pu = pu;
    }

    public int getLivre_id() {
        return livre_id;
    }

    public void setLivre_id(int livre_id) {
        this.livre_id = livre_id;
    }

    public LocalDate getLivre_date() {
        return livre_date;
    }

    public void setLivre_date(LocalDate livre_date) {
        this.livre_date = livre_date;
    }

    public int getLivre_qte() {
        return livre_qte;
    }

    public void setLivre_qte(int livre_qte) {
        this.livre_qte = livre_qte;
    }

    public int getLivre_stock() {
        return livre_stock;
    }

    public void setLivre_stock(int livre_stock) {
        this.livre_stock = livre_stock;
    }

    public boolean isVendu() {
        return vendu;
    }

    public void setVendu(boolean vendu) {
        this.vendu = vendu;
    }

    public boolean isArret() {
        return arret;
    }

    public void setArret(boolean arret) {
        this.arret = arret;
    }

    public int getPu() {
        return pu;
    }

    public void setPu(int pu) {
        this.pu = pu;
    }
    
    
}
