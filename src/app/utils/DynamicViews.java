package app.utils;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

public class DynamicViews {

	private DynamicViews() {
		
	}
	
	public static void loadBoarderCenter (BorderPane borderPane, String resource) {

		try {
			Parent view = FXMLLoader.load(new DynamicViews().getClass().getResource("/app/views/"+ resource +".fxml"));
			borderPane.setCenter(view);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}
