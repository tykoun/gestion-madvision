package app.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.ibatis.jdbc.ScriptRunner;

public class ConnectionUtil {
	//Connection conn = null;
        //URL url = getClass().getResource("madvision.sql");
    
	public static Connection conDB() {
		//Connection conn = null;
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//			//Connection con = DriverManager.getConnection("jdbc:mysql://localhost/madvision", "root", "");
//			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/demodb", "root", "");
//			return con;
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			return null;
//		}
                
		 try {
	            // db parameters
	            //String url = "jdbc:sqlite:C:/sqlite/db/chinook.db";
	            //String url = "jdbc:sqlite:C:/sqlite/db/data.sqlite";
	            String url = "jdbc:sqlite:C:/db/data.sqlite";
	            // create a connection to the database
	            Connection conn = DriverManager.getConnection(url);
	            
	            System.out.println("Connection to SQLite has been established.");
	            return conn;
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	            return null;
	        }
	}
            
        
        public static void createNewDatabase(String fileName) {
        String url = new File("").getAbsolutePath();
        String relativePath = "\\src\\app\\madvision.sql";

        String sql = "CREATE DATABASE IF NOT EXISTS DEMODB";
                try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con2 = DriverManager.getConnection("jdbc:mysql://localhost/", "root", "");
                        PreparedStatement stmt = con2.prepareStatement(sql);
                        
                        
                        stmt.execute();
                        Connection con = DriverManager.getConnection("jdbc:mysql://localhost/demodb", "root", "");
                        DatabaseMetaData dbm = con.getMetaData();

                        ResultSet rs = dbm.getTables(null, null, "categorie", null);
                        if (rs.next()) {
                          System.out.println("Table exists"); 
                        } else {
                        //Initialize the script runner
                            ScriptRunner sr = new ScriptRunner(con);
                            //Creating a reader object
    //                        Reader reader = new BufferedReader(new FileReader("C:\\sqlite\\db\\madvision.sql"));
                            Reader reader = new BufferedReader(new FileReader(url+relativePath));
                            //Running the script
                            sr.runScript(reader);
                        }
                        
                        
                        con2.close();
                        
//			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/demodb", "root", "");
                        
                        con.close();
                        stmt.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
    }    
}
