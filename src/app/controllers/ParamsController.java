/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.utils.ConnectionUtil;
import app.utils.Params;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Finaritra
 */
public class ParamsController implements Initializable{

    @FXML
    private JFXButton btnSavePassword;

    @FXML
    private TextField txtName;

    @FXML
    private PasswordField txtNewPwd;

    @FXML
    private PasswordField txtNewPw2;

    @FXML
    private JFXButton btnSaveUsername;

    @FXML
    private JFXPasswordField txtPwdConfirmation;
    
    @FXML
    private JFXButton btnclose;
    
    @FXML
    private Label lbluser;    
    
    private static String nameTxt = "";
    
    private static String pwdTxt = "";
    
    @FXML
    void savePwd(ActionEvent event) {
        pwdTxt = txtNewPwd.getText();
        if(txtNewPwd.getText().length() < 3 || txtNewPwd.getText().length() < 3){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setContentText("Le mot de passe doit contenir au moins 3 caractères");
            alert.setHeaderText(null);

            Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        }        
        else if(!txtNewPwd.getText().equals(txtNewPw2.getText())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setContentText("Non concordance du mot de passe de confirmation. Veuillez réessayer");
            alert.setHeaderText(null);

            Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        } else {
            try {
                    Stage stage;
                    Parent root;

                    if(event.getSource() == btnSavePassword) {
                            stage = new Stage();
                            root = FXMLLoader.load(getClass().getResource("/app/views/ParamsConfirmPwd.fxml"));
                            stage.setScene(new Scene(root));
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.initOwner(btnSavePassword.getScene().getWindow());
                            stage.initStyle(StageStyle.UNDECORATED);
                            stage.showAndWait();
                    } 
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        
    }

    @FXML
    void saveUsername(ActionEvent event) {
        nameTxt = txtName.getText();
        if(txtName.getText().length() < 4){
                Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Erreur");
        	alert.setContentText("Veuillez mettre au moins 3 caractères");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        } else {
            try {
                    Stage stage;
                    Parent root;

                    if(event.getSource() == btnSaveUsername) {
                            stage = new Stage();
                            root = FXMLLoader.load(getClass().getResource("/app/views/ParamsConfirm.fxml"));
                            stage.setScene(new Scene(root));
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.initOwner(btnSaveUsername.getScene().getWindow());
                            stage.initStyle(StageStyle.UNDECORATED);
                            stage.showAndWait();
                    } 
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    

    @FXML
    void close() {
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnclose.getScene().getWindow();
        stage.close();
    }

    @FXML
    void confirm(ActionEvent event) {
        if(!txtPwdConfirmation.getText().equals(Params.password)){
                txtPwdConfirmation.clear();
            
                Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Erreur");
        	alert.setContentText("Mot de passe incorrect");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        } else {
            System.out.println("++++++++++++ txtName : " + nameTxt);
            try {
        	    Connection con = ConnectionUtil.conDB();
                    PreparedStatement preparedStmt = null;
                    String query = "UPDATE user SET login = ? WHERE user_id = ?";
                    preparedStmt = con.prepareStatement(query);
                    preparedStmt.setString(1, nameTxt);
                    preparedStmt.setInt(2, Params.userId);

                    preparedStmt.execute();
                    
                    preparedStmt.close();
                    con.close();
                    
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("INFORMATION");
                    alert.setContentText("Nom d'utilisateur modifié avec succès");
                    alert.setHeaderText(null);

                    Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
                    close();
                
            } catch (Exception e) {
                //printStackTrace(e);
            }
        }
    }
    @FXML
    void confirm2(ActionEvent event) {
        if(!txtPwdConfirmation.getText().equals(Params.password)){
                txtPwdConfirmation.clear();
            
                Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Erreur");
        	alert.setContentText("Mot de passe incorrect");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        } else {
            try {
        	    Connection con = ConnectionUtil.conDB();
                    PreparedStatement preparedStmt = null;
                    String query = "UPDATE user SET password = ? WHERE user_id = ?";
                    preparedStmt = con.prepareStatement(query);
                    preparedStmt.setString(1, pwdTxt);
                    preparedStmt.setInt(2, Params.userId);

                    preparedStmt.execute();
                    
                    preparedStmt.close();
                    con.close();
                    
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("INFORMATION");
                    alert.setContentText("Mot de passe modifié avec succès");
                    alert.setHeaderText(null);

                    Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
                    close();
                
            } catch (Exception e) {
                //printStackTrace(e);
            }
        }
    }    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
            lbluser.setText("@"+Params.username);
    }
    
}
