/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.model.Livre;
import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Finaritra
 */
public class LivreController implements Initializable {

    @FXML
    private Label lblDateRecu;

    @FXML
    private Label lblReste;

    @FXML
    private Label lblNbLivreRecu;

    @FXML
    private Label lbldate;

    @FXML
    private JFXButton btnConfirmer;

    @FXML
    private JFXButton btnConfirmer1;
        
    @FXML
    private JFXButton btnCancel;
        
    @FXML
    private TextField txtNbLivre;
    
    private final ObservableList<Livre> livreData = FXCollections.observableArrayList();    
    
    private int nbLivre_stock = 0;

    GestionCompteController gc;
    DashboardController ds;
    
    public void init(GestionCompteController gcController, DashboardController dsController) {
        gc = gcController;
        ds = dsController;
    }
    
    private void loadView(){
        gc.loadView();
        ds.loadView();
    }
    
    @FXML
    void confirmer(ActionEvent event) {
        if(txtNbLivre.getText().length() == 0) {
    		System.out.println("Hahahahahhahaha");
    	}
        else if(nbLivre_stock < Integer.parseInt(txtNbLivre.getText())){
    		Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Erreur");
        	alert.setContentText("Stock de livres insuffisant");
        	alert.setHeaderText(null);     
                alert.showAndWait();
        }
        else {
    		
    		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Confirmer l'achat de "+ txtNbLivre.getText() +" livre(s)?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
                    
                    if((nbLivre_stock >= Integer.parseInt(txtNbLivre.getText()) && livreData.size() > 0)){
                        String query = "UPDATE livre SET livre_stock = ? WHERE livre_id = ?;";
                        
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setInt(1, nbLivre_stock - Integer.parseInt(txtNbLivre.getText().toString()));
    			preparedStmt.setInt(2, livreData.get(0).getLivre_id());
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Add succesfully !! ");
                    }
                    else con.close();
    			//FormData.add(new Formation(txtFormation.getText()));
//                        categorieData.clear();
//    			fillCategorie();
//                        txtDepense.clear();

    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
                loadData();
                loadView();
            }
        }
    }
    
    @FXML
    void loadData(){
        DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    	String date = LocalDate.now().format(dateformatter);
        
        txtNbLivre.setText("");
        
        lbldate.setText("Le : "+convert(date));
        
    	livreData.clear();
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * FROM livre WHERE vendu=0";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				livreData.add(new Livre(rs.getInt("livre_id"), LocalDate.parse(rs.getString("livre_date")), rs.getInt("livre_qte"), rs.getInt("livre_stock"), false, false, 10000));
			}
			con.close();
			preparedStmt.close();
			rs.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        if(livreData.size() == 0){
            lblDateRecu.setText("Reçus le "+convert(date));
            lblNbLivreRecu.setText("Tsisy boky intsony");
            lblReste.setText("Reste boky : 0");
            nbLivre_stock = 0;
        } else {
            lblDateRecu.setText("Reçu le " + convert(livreData.get(0).getLivre_date().format(dateformatter)));
            lblNbLivreRecu.setText("Nombre : "+ livreData.get(0).getLivre_qte());
            lblReste.setText("Reste : "+ livreData.get(0).getLivre_stock());
            nbLivre_stock = livreData.get(0).getLivre_stock();
        }
    }

    @FXML
    void ajouterLivre(ActionEvent event){
        try {			
        Stage stage;
        Parent root;

        if(event.getSource() == btnConfirmer1) {
                stage = new Stage();
                root = FXMLLoader.load(getClass().getResource("/app/views/ajoutLivre.fxml"));
                stage.setScene(new Scene(root));
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initOwner(btnConfirmer1.getScene().getWindow());
                stage.initStyle(StageStyle.UTILITY);
                stage.showAndWait();

        } else {
                stage = (Stage) btnCancel.getScene().getWindow();
                stage.close();
        }
        //Stage stage1 = new Stage();

        //Parent root1 = FXMLLoader.load(getClass().getResource("/app/views/App.fxml"));
        //Scene scene1 = new Scene(root1);
        //stage1.setScene(scene1);

        //stage.initOwner(stage1);
        //			stage.initModality(Modality.APPLICATION_MODAL);
        //			stage.initOwner(btnSolde.getScene().getWindow());
        //			stage.initStyle(StageStyle.UTILITY);
        //			stage.setTitle("Modifier solde initial");
        //			Parent root = FXMLLoader.load(getClass().getResource("/app/views/solde.fxml"));
        //			Scene scene = new Scene(root, 390, 190);
        //			//scene.getStylesheets().add(getClass().getResource("/app/stylesheet/application.css").toExternalForm());
        //			//primaryStage.initStyle(StageStyle.UNDECORATED);
        //			stage.setScene(scene);
        //			stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
        loadData();
    }
    
    static String convert(String str)
    {
    // Create a char array of given String
    char ch[] = str.toCharArray();
    for (int i = 0; i < str.length(); i++) {
    // If first character of a word is found
    if (i == 0 && ch[i] != ' ' ||
    ch[i] != ' ' && ch[i - 1] == ' ') {
    // If it is in lower-case
    if (ch[i] >= 'a' && ch[i] <= 'z') {
    // Convert into Upper-case
    ch[i] = (char)(ch[i] - 'a' + 'A');
    }
    }
    // If apart from first character
    // Any one is in Upper-case
    else if (ch[i] >= 'A' && ch[i] <= 'Z')
    // Convert into Lower-Case
    ch[i] = (char)(ch[i] + 'a' - 'A');
    }
    // Convert the char array to equivalent String
    String st = new String(ch);
    return st;
    } 

    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loadData();
        txtNbLivre.textProperty().addListener(new ChangeListener<String>() {

                @Override
                public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                        if(!txtNbLivre.getText().matches("\\d{1,15}")) {
                                //txtStock.setStyle("-fx-background-color: #FF514F");
                                txtNbLivre.setText("");
                        }
                        else {
                                txtNbLivre.setStyle("-fx-border-color: WHITE");
                        }
                }
        });
    }

}
