package app.controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import app.model.Formation;
import app.model.FormationModule;
import app.model.Module;
import app.model.Type;
import app.utils.ConnectionUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.StringConverter;

public class FormationController implements Initializable{

    @FXML
    private TableView<Formation> listeFormation;
    
    @FXML
    private TableColumn<Formation, Integer> idForm;
    
    @FXML
    private TableColumn<Formation, String> nomForm;

    @FXML
    private JFXButton btnAddFormation;

    @FXML
    private TextField txtFormation;

    @FXML
    private TableColumn<FormationModule, String> module;

    @FXML
    private TableColumn<FormationModule, String> droit;

    @FXML
    private TableColumn<FormationModule, String> formation;

    @FXML
    private TableColumn<FormationModule, String> type;

    @FXML
    private JFXButton btnEditFormation;

    @FXML
    private TableView<FormationModule> table;

    @FXML
    private JFXButton btnDelFormation;
        
    @FXML
    private JFXComboBox<Formation> cbxFormation;
    
    @FXML
    private JFXComboBox<Type> cbxType;
    @FXML
    private JFXButton btnEditModule;

    @FXML
    private TextField txtNomModule;

    @FXML
    private TextField txtDroit;

    @FXML
    private JFXButton btnAddModule;

    @FXML
    private JFXButton btnDelModule;
    
    @FXML
    private JFXComboBox<Formation> test;
 

    
    private final ObservableList<Formation> FormData = FXCollections.observableArrayList();
    private final ObservableList<Type> TypeCbx = FXCollections.observableArrayList();
    private final ObservableList<FormationModule> ModuleData = FXCollections.observableArrayList();
    
    ElevesController ec;
    VersementController vc;
    CertificatController cc;
    GestionCompteController gc;
    DashboardController dc;
            
    void init(ElevesController elvController, VersementController vrsController, CertificatController crtController, GestionCompteController gcController, DashboardController dashController) {
        ec = elvController;
        vc = vrsController;
        cc = crtController;
        gc = gcController;
        dc = dashController;
    }


    /**
     * Fill table Formation
     */
    @FXML
    private void fillFormation() {
    	    	
		StringConverter<Formation> converter = new StringConverter<Formation>() {

			@Override
			public Formation fromString(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String toString(Formation obj) {
				// TODO Auto-generated method stub
				return obj.getForm_nom();
			}
			
		};
		
    	listeFormation.setItems(null);

    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * from 'formation'";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				//Formation f = new Formation(rs.getInt("form_id"), rs.getString("form_nom"));
				//System.out.println(f.getForm_id() + " - "+ f.getForm_nom());
				FormData.add(new Formation(rs.getInt(1), rs.getString(2)));
			}
			con.close();
			preparedStmt.close();
			rs.close();
						
			cbxFormation.setConverter(converter);
			cbxFormation.setItems(FormData);

			filltype();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	idForm.setCellValueFactory(new PropertyValueFactory<Formation, Integer>("form_id"));
    	nomForm.setCellValueFactory(new PropertyValueFactory<Formation, String>("form_nom"));
    	listeFormation.setItems(FormData);
    	checkFormation();
    	checkModule();

    }

    private void fillModule() {
    	table.setItems(null);

    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT formation.form_id, formation.form_nom, module.module_id, module.module_nom, type.type_id, type.type_nom, moduletype.droit FROM ((formation Join module ON formation.form_id=module.form_id) JOIN moduletype ON module.module_id=moduletype.module_id) JOIN type on moduletype.type_id=type.type_id GROUP BY module.module_id";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				ModuleData.add(new FormationModule(rs.getInt("form_id"), rs.getString("form_nom"), rs.getInt("module_id"), rs.getString("module_nom"), rs.getInt("type_id"), rs.getString("type_nom"), rs.getString("droit")));
			}
			con.close();
			preparedStmt.close();
			rs.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	formation.setCellValueFactory(new PropertyValueFactory<FormationModule, String>("form_nom"));
    	module.setCellValueFactory(new PropertyValueFactory<FormationModule, String>("module_nom"));
    	type.setCellValueFactory(new PropertyValueFactory<FormationModule, String>("type_nom"));
    	droit.setCellValueFactory(new PropertyValueFactory<FormationModule, String>("droit"));
    	table.setItems(ModuleData);
    }
    
    
    private void filltype() {
		// TODO Auto-generated method stub
		StringConverter<Type> converter = new StringConverter<Type>() {

			@Override
			public Type fromString(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String toString(Type obj) {
				// TODO Auto-generated method stub
				return obj.getType_nom();
			}
		};
		
    	listeFormation.setItems(null);
    	PreparedStatement preparedStmt = null;
    	ResultSet rs = null;
    	try {
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * from type";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				TypeCbx.add(new Type(rs.getInt(1), rs.getString(2)));
			}
			con.close();
			preparedStmt.close();
			rs.close();
						
			cbxType.setConverter(converter);
			cbxType.setItems(TypeCbx);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void checkFormation() {
//    	if(txtFormation.getText().length()==0 || listeFormation.getSelectionModel().isEmpty()) {
//    		btnAddFormation.setDisable(true);
//    		btnEditFormation.setDisable(true);
//    		btnDelFormation.setDisable(true);
//
//    	} else if(!listeFormation.getSelectionModel().isEmpty()){
//    		//btnAddFormation.setDisable(false);
//    		btnEditFormation.setDisable(false);
//    		btnDelFormation.setDisable(false);
//    	} else if(listeFormation.getSelectionModel().isEmpty()) {
//    		btnAddFormation.setDisable(false);
//    		btnEditFormation.setDisable(true);
//    		btnDelFormation.setDisable(true);
//    	}
    }
    
    public void checkModule() {
    	if(cbxFormation.getSelectionModel().isEmpty() || cbxType.getSelectionModel().isEmpty() 
    			|| txtNomModule.getText().isEmpty() || txtDroit.getText().isEmpty()) {
    		btnAddModule.setDisable(true);
    		btnEditModule.setDisable(true);
    		btnDelModule.setDisable(true);
    	} else {
    		btnAddModule.setDisable(false);
    		btnEditModule.setDisable(false);
    		btnDelModule.setDisable(false);
    	}
    }
    
    public void AnnulSelectionFormation() {
    	listeFormation.getSelectionModel().clearSelection();
    	txtFormation.setText(null);
		btnAddFormation.setDisable(true);
		btnEditFormation.setDisable(true);
		btnDelFormation.setDisable(true);
    }
    
    public void AnnulSelectionTable() {
    	table.getSelectionModel().clearSelection();
    	//txtFormation.setText(null);
		//btnAddFormation.setDisable(true);
		//btnEditFormation.setDisable(true);
		//btnDelFormation.setDisable(true);
    }
    
    public void AnnulSelection() {
    	AnnulSelectionFormation();
    	AnnulSelectionTable();
    }
    /**
     * Set Formation Text
     */
    public void setFormationTxt() {
    	String selectedItem = listeFormation.getSelectionModel().getSelectedItem().getForm_nom();
    	//selectedIndex = listeFormation.getSelectionModel().getSelectedIndex();
    	txtFormation.setText(selectedItem);
    		btnAddFormation.setDisable(false);
    		btnEditFormation.setDisable(false);
    		btnDelFormation.setDisable(false);
    		AnnulSelectionTable();
    }
    
    /*
     * Set Module Form
     */
    public void setModuleForm() {
    	FormationModule formationModule = table.getSelectionModel().getSelectedItem();
    	/*
    	 * get model Formatin index from table
    	 */
    	int count = 0;
    	int indexType = 0;
    	for(Formation x : FormData) {
    		if(x.getForm_id() == formationModule.getForm_id()) {
    			break;
    		}
    		count++;
    	}
    	
    	for(Type t : TypeCbx) {
    		if(t.getType_id() == formationModule.getType_id()) {
    			break;
    		}
    		indexType++;
    	}

    	txtNomModule.setText(formationModule.getModule_nom());
    	txtDroit.setText(formationModule.getDroit());
    	cbxFormation.getSelectionModel().select(count);
    	cbxType.getSelectionModel().select(indexType);
    	AnnulSelectionFormation();
    }
    
    
    public void refreshTable() {
    	FormData.clear();
    	ModuleData.clear();
    	TypeCbx.clear();
    	//TypeCbx.clear();
    	fillModule();
    	fillFormation();
        
        ec.loadView();
        vc.loadView();
        gc.loadView();
        dc.loadView();
    }
    
    @FXML
    void addFormation() {
        
    	if(txtFormation.getText().length() == 0) {
    		System.out.println("Hahahahahhahaha");
    	}  else {
    		
    		Alert alert = new Alert(AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Voulez-vous ajouter \"" + txtFormation.getText() + "\" dans Formation ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "INSERT INTO formation (form_nom) values(?)";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, txtFormation.getText());
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Add succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
    			fillFormation();
    			
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	refreshTable();
        	txtFormation.clear();
        	checkFormation();
    	}
    }
    }

    @FXML
    void EditFormation() {
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation");
    	alert.setContentText("Voulez-vous vraiment modifier la formation \"" + listeFormation.getSelectionModel().getSelectedItem().getForm_nom().toString() + "\" ?");
    	alert.setHeaderText(null);
    	//alert.showAndWait();
    	
    	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
    	
    	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
    		int formId = listeFormation.getSelectionModel().getSelectedItem().getForm_id();
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "UPDATE formation SET `form_nom`=? WHERE form_id=?";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, txtFormation.getText());
    			preparedStmt.setInt(2, formId);
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Edited Succesfully");
    			//FormData.add(new Formation(txtFormation.getText()));
    			fillFormation();
    	    	txtFormation.clear();
    	    	refreshTable();
    	    	checkFormation();
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}

    }

    @FXML
    public void delFormation() {
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation");
    	alert.setContentText("Voulez-vous vraiment supprimer la formation \"" + listeFormation.getSelectionModel().getSelectedItem().getForm_nom().toString() + "\" ?");
    	alert.setHeaderText(null);
    	//alert.showAndWait();
    	
    	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
    	
    	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
    		int formId = listeFormation.getSelectionModel().getSelectedItem().getForm_id();
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "DELETE FROM `formation` WHERE form_id=?";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setInt(1, formId);
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Deleted Succesfully");
    			//FormData.add(new Formation(txtFormation.getText()));
    			fillFormation();
    	    	txtFormation.clear();
    	    	refreshTable();
    	    	checkFormation();
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}

    }

    
    /**
     * Module functions
     */

    @FXML
    public void selectFormation() {
    	checkModule();
    	//System.out.println("Coucou :)");
    }
    
    @FXML
    public void selectType() {
    	checkModule();
    }

    @FXML
    public void addModule() {
    		
    		Alert alert = new Alert(AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Voulez-vous ajouter le module \"" + txtNomModule.getText() + "\" dans la Formation \"" + cbxFormation.getSelectionModel().getSelectedItem().getForm_nom() + "\"?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
        	try {
        		PreparedStatement preparedStmt = null;
        		ResultSet rs = null;
        	    Connection con = ConnectionUtil.conDB();
        	    con.setAutoCommit(false);
        	    
        	    String query1 = "INSERT INTO module (module_nom, form_id) VALUES(?,?);";
        	    String query2 = "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);";
//        	    String query = "START TRANSACTION;"
//        	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//        	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//        	    		"ROLLBACK;";
    			preparedStmt = con.prepareStatement(query1);
    			preparedStmt.setString(1, txtNomModule.getText());
    			preparedStmt.setInt(2, cbxFormation.getSelectionModel().getSelectedItem().getForm_id());
    			
    			preparedStmt.executeUpdate();

    			preparedStmt = con.prepareStatement(query2);
    			preparedStmt.setInt(1, cbxType.getSelectionModel().getSelectedItem().getType_id());
    			preparedStmt.setInt(2, Integer.parseInt(txtDroit.getText()));
    			    			
    			preparedStmt.executeUpdate();

        	    String query3 = "SELECT * from module";
    			preparedStmt = con.prepareStatement(query3);
    			rs = preparedStmt.executeQuery();
    			while(rs.next()) {
    				Module f = new Module(rs.getInt("module_id"), rs.getString("module_nom"), rs.getInt("form_id"));
    				System.out.println(f.getModule_id() + " - "+ f.getModule_nom() + " - "+ f.getForm_id());
    				//FormData.add(new Formation(rs.getInt(1), rs.getString(2)));
    			}
    			
    			con.commit();
    			
    			con.close();
    			preparedStmt.close();
    			rs.close();
    			
    			System.out.println("Module : Add succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
    			fillModule();

    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        	refreshTable();
        	txtFormation.clear();
        	txtDroit.clear();
        	txtNomModule.clear();
        	cbxFormation.getSelectionModel().clearSelection();;
        	cbxType.getSelectionModel().clearSelection();
        	checkFormation();
    	}
    }

    @FXML
    public void editModule() {
    	FormationModule formationModule = table.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation");
    	alert.setContentText("Voulez-vous modifier le module \"" + txtNomModule.getText() + "\" dans la Formation \"" + cbxFormation.getSelectionModel().getSelectedItem().getForm_nom() + "\"?");
    	alert.setHeaderText(null);
    	
    	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
    	
    	if(action.get() == javafx.scene.control.ButtonType.OK) {
    	try {
    		PreparedStatement preparedStmt = null;
    	    Connection con = ConnectionUtil.conDB();
    	    con.setAutoCommit(false);
    	    
    	    String query1 = "UPDATE module SET module_nom=?, form_id=? WHERE module_id=?;";
    	    String query2 = "UPDATE moduletype SET type_id=?, droit=? WHERE module_id=?";
//    	    String query = "START TRANSACTION;"
//    	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//    	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//    	    		"ROLLBACK;";
			preparedStmt = con.prepareStatement(query1);
			preparedStmt.setString(1, txtNomModule.getText());
			preparedStmt.setInt(2, cbxFormation.getSelectionModel().getSelectedItem().getForm_id());
			preparedStmt.setInt(3, formationModule.getModule_id());
			
			preparedStmt.executeUpdate();

			preparedStmt = con.prepareStatement(query2);
			preparedStmt.setInt(1, cbxType.getSelectionModel().getSelectedItem().getType_id());
			preparedStmt.setInt(2, Integer.parseInt(txtDroit.getText()));
			preparedStmt.setInt(3, formationModule.getModule_id());
    			
			preparedStmt.executeUpdate();
			
			con.commit();
			
			con.close();
			preparedStmt.close();
			
			System.out.println("Module : Edited succesfully !! ");
			//FormData.add(new Formation(txtFormation.getText()));
			fillModule();

		} catch (Exception e) {
			e.printStackTrace();
		}
    	refreshTable();
    	txtFormation.clear();
    	txtDroit.clear();
    	txtNomModule.clear();
    	cbxFormation.getSelectionModel().clearSelection();;
    	cbxType.getSelectionModel().clearSelection();
    	//checkFormation();
	}
    }

    @FXML
    public void delModule() {
    	FormationModule formationModule = table.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation");
    	alert.setContentText("Voulez-vous supprimer le module \"" + txtNomModule.getText() + "\" dans la Formation \"" + cbxFormation.getSelectionModel().getSelectedItem().getForm_nom() + "\"?");
    	alert.setHeaderText(null);
    	
    	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
    	
    	if(action.get() == javafx.scene.control.ButtonType.OK) {
    	try {
    		PreparedStatement preparedStmt = null;
    	    Connection con = ConnectionUtil.conDB();
    	    con.setAutoCommit(false);
    	    
    	    String query1 = "DELETE FROM module WHERE module_id=?;";
    	    String query2 = "DELETE FROM moduletype WHERE module_id=?;";
//    	    String query = "START TRANSACTION;"
//    	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//    	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//    	    		"ROLLBACK;";
			preparedStmt = con.prepareStatement(query1);
			preparedStmt.setInt(1, formationModule.getModule_id());
			
			preparedStmt.executeUpdate();

			preparedStmt = con.prepareStatement(query2);
			preparedStmt.setInt(1, formationModule.getModule_id());
    			
			preparedStmt.executeUpdate();
			
			con.commit();
			
			con.close();
			preparedStmt.close();
			
			System.out.println("Module : Deleted succesfully !! ");
			//FormData.add(new Formation(txtFormation.getText()));
			fillModule();

		} catch (Exception e) {
			e.printStackTrace();
		}
    	refreshTable();
    	txtFormation.clear();
    	txtDroit.clear();
    	txtNomModule.clear();
    	cbxFormation.getSelectionModel().clearSelection();;
    	cbxType.getSelectionModel().clearSelection();
    	checkFormation();
	}
    }
    
    public void clearModule() {
    	
    }
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		fillFormation();
		fillModule();
        txtDroit.textProperty().addListener(new ChangeListener<String>() {

                    @Override
                    public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                            if(!txtDroit.getText().matches("\\d{1,15}")) {
                                    //txtStock.setStyle("-fx-background-color: #FF514F");
                                    txtDroit.setText("");
                            }
                            else {
                                    txtDroit.setStyle("-fx-border-color: WHITE");
                            }
                    }
            });	
        }
}
