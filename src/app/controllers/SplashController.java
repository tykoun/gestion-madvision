/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

/**
 *
 * @author Finaritra
 */
public class SplashController implements Initializable {

    @FXML
    private AnchorPane parent;
    
    @FXML
    private Label lblBienvenue;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FadeTransition fade = new FadeTransition(Duration.seconds(5), parent);
            fade.setToValue(1);
            fade.setFromValue(0.5);
            fade.play();

            fade.setOnFinished(event1 ->{
                try {
                    
                    
                    Parent fxml = FXMLLoader.load(getClass().getResource("/app/views/Login.fxml"));
                    parent.getChildren().removeAll();
                    parent.getChildren().setAll(fxml);
                } catch (Exception e) {
                    //printStackTrace(e);
                }
            });
    }
    
}
