/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXToggleButton;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
/**
 *
 * @author Finaritra
 */
public class FiltreDepenseController implements Initializable {

    @FXML
    private Button btnCancel;

    @FXML
    private JFXDatePicker date1;

    @FXML
    private JFXDatePicker date2;

    @FXML
    private JFXToggleButton toggle3;

    @FXML
    private ToggleGroup GROUP;

    @FXML
    private JFXToggleButton toggle1;

    @FXML
    private JFXToggleButton toggle2;

    @FXML
    private Button btnFiltrer;

    DepenseController dc = new DepenseController();
    
    @FXML
    void close(ActionEvent event) {
        DepenseController.setFilter("none");
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }

    @FXML
    void filtrer(ActionEvent event) {
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }

    @FXML
    void checkToggle(ActionEvent event) {
        if(toggle1.isSelected()){
            DepenseController.setFilter("month");
            toggle2.setSelected(false);
            toggle3.setSelected(false);
            disableDate(true);
        }
        else if(toggle2.isSelected()){
            DepenseController.setFilter("year");
            toggle1.setSelected(false);
            toggle3.setSelected(false);
            disableDate(true);
        }
        else if(toggle3.isSelected()){
            DepenseController.setFilter("dates");
            toggle1.setSelected(false);
            toggle2.setSelected(false);
            disableDate(false);
        } 
        else {
            DepenseController.setFilter("month");
            toggle1.setSelected(true);
            disableDate(true);
        }
        
        
        System.out.println(DepenseController.filterString);
    }
    
    void disableDate(boolean bool){
        date1.setDisable(bool);
        date2.setDisable(bool);
    }
    
    @FXML
    void checkDate1(ActionEvent event) {
        DepenseController.setDate1(date1.getValue().toString());
    }

    @FXML
    void checkDate2(ActionEvent event) {
        DepenseController.setDate1(date2.getValue().toString());
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DepenseController.setFilter("month");
        date1.setValue(LocalDate.now());
        date2.setValue(LocalDate.now());
    }

    void init(DepenseController depenseController) {
        dc = depenseController;
    }
}
