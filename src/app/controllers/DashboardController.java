/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.model.FormationModule;
import app.model.Livre;
import app.model.Transaction;
import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Finaritra
 */
public class DashboardController implements Initializable{
    
    @FXML
    private PieChart pieChart;

    @FXML
    private Label txtSolde;

    @FXML
    private Label dateSoldeInitial;

    @FXML
    private Label txtCredit;

    @FXML
    private Label lblDate;

    @FXML
    private Label txtDebit;

    @FXML
    private Label txtSoldeTotal;

    @FXML
    private Label txtLivreVendu;
    
    @FXML
    private Label txtAvg;

    @FXML
    private JFXTextField txtMontantLivre;

    @FXML
    private LineChart<Number, Number> lineChart1;
    @FXML
    private Label txtNbEleve;

    @FXML
    private Label txtDroitOk;

    @FXML
    private Label txtNbCertificat;

    @FXML
    private Label txtStockLivre;

    @FXML
    private Label txtHomme;

    @FXML
    private Label txtFemme;
    
    ObservableList<PieChart.Data> data = FXCollections. observableArrayList();
    private final ObservableList<FormationModule> formationData = FXCollections.observableArrayList();
    private final ObservableList<Transaction> transData2 = FXCollections.observableArrayList();
    
    private final ObservableList<Livre> livreData = FXCollections.observableArrayList();    
    
    private int nbHomme = 0;
    private int nbFemme = 0;
    private int moyenneDage = 0;
    private int totalMontantLivre = 0;
    
    String jour = LocalDate.now().getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.FRANCE);
    
    DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    String date = LocalDate.now().format(dateformatter);
                    
    static String convert(String str)
    {
    // Create a char array of given String
    char ch[] = str.toCharArray();
    for (int i = 0; i < str.length(); i++) {
    // If first character of a word is found
    if (i == 0 && ch[i] != ' ' ||
    ch[i] != ' ' && ch[i - 1] == ' ') {
    // If it is in lower-case
    if (ch[i] >= 'a' && ch[i] <= 'z') {
    // Convert into Upper-case
    ch[i] = (char)(ch[i] - 'a' + 'A');
    }
    }
    // If apart from first character
    // Any one is in Upper-case
    else if (ch[i] >= 'A' && ch[i] <= 'Z')
    // Convert into Lower-Case
    ch[i] = (char)(ch[i] + 'a' - 'A');
    }
    // Convert the char array to equivalent String
    String st = new String(ch);
    return st;
    } 
    
    
    public void loadView(){
        fillForm();
        loadData();
    }
    
    @FXML
    private void fillForm() {
    	formationData.clear();
        transData2.clear();
    	int solde = 0;
    	int credit = 0;
    	int debit = 0;
        int totalCredit = 0;
        int nbBookVendu = 0;
    	String date = LocalDate.now().toString();

        try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT vers_id, vers_motif, SUM(vers_montant) as 'vers_montant' FROM `versement` WHERE arret = false GROUP BY vers_motif";
    	    String query2 = "SELECT * FROM (`transaction` JOIN trans_type ON \"transaction\".trans_type_id=trans_type.trans_type_id) NATURAL JOIN solde where solde.solde_id = (SELECT MAX(solde_id) FROM solde) AND \"transaction\".arret=0;";
    	    String query3 = "SELECT * FROM `solde` WHERE solde_id=(SELECT MAX(solde_id) FROM solde)";   
            String query4 = "SELECT * FROM livre WHERE vendu=0";

                        preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				formationData.add(new FormationModule(rs.getInt("vers_id"), rs.getString("vers_motif"), rs.getInt("vers_montant"), rs.getString("vers_motif"), rs.getInt("vers_id"), rs.getString("vers_motif"), rs.getString("vers_motif")));
					//credit = credit + rs.getInt("vers_montant");
			}
                        preparedStmt = con.prepareStatement(query2);
			rs = preparedStmt.executeQuery();
                        while(rs.next()) {
				if(rs.getInt("trans_type_id") == 2) {
					transData2.add(new Transaction(rs.getInt("trans_id"), rs.getString("trans_design"), rs.getString("trans_date"), rs.getInt("trans_montant"), rs.getBoolean("arret"), rs.getInt("trans_type_id")));
					debit = debit + rs.getInt("trans_montant");
				}
			}
                        
                        preparedStmt = con.prepareStatement(query3);
			rs = preparedStmt.executeQuery();
                        while(rs.next()){
                            solde = rs.getInt("solde_init");
                            date = rs.getString("solde_date");
                        }
                        
			preparedStmt = con.prepareStatement(query4);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				livreData.add(new Livre(rs.getInt("livre_id"), LocalDate.parse(rs.getString("livre_date")), rs.getInt("livre_qte"), rs.getInt("livre_stock"),  rs.getBoolean("vendu"), rs.getBoolean("arret"), rs.getInt("prix")));
			}
                        
                        preparedStmt.close();
			rs.close();
			con.close();
                        
                        
            int nombreLivreVendu = 0;
            int nombreLivreEnStock = 0;
            int totalLivre = 0;
            for (Livre livre : livreData) {
                nombreLivreEnStock = livre.getLivre_stock();
                totalLivre = livre.getLivre_qte();

                if(totalLivre >=  nombreLivreEnStock){
                    nombreLivreVendu += totalLivre - nombreLivreEnStock;
                    totalMontantLivre += (totalLivre - nombreLivreEnStock)*livre.getPu();
                } else {
                    //totalMontantLivre += (totalLivre - nombreLivreEnStock)*livre.getPu();
                }
            }
            nbBookVendu = nombreLivreVendu;
        
        } catch (Exception except) {
			// TODO Auto-generated catch block
			except.printStackTrace();
		}
    	        
        for (FormationModule transaction : formationData) {
            credit += transaction.getModule_id();
        }
        
                credit = credit + totalCredit + totalMontantLivre;
                
        	LocalDate dateR = LocalDate.parse(date);
        	DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        	txtCredit.setText(Integer.toString(credit));
        	txtDebit.setText(Integer.toString(debit));
        	txtSolde.setText(Integer.toString(solde));
        	dateSoldeInitial.setText(dateR.format(dateformatter));
        	txtSoldeTotal.setText(Integer.toString(solde + credit - debit));
                txtLivreVendu.setText(Integer.toString(nbBookVendu));
    }
    @FXML
    private void loadData(){
        int nbLivre = 0;
        int nbEleve = 0;
        int nbDroit = 0;
        int nbCertif = 0;
    	try {
            PreparedStatement preparedStmt = null;
            ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * FROM livre WHERE arret=0";
    	    String query2 = "Select count(fe_id) as 'nb' from formationeleve where certificat=TRUE";
    	    //String query3 = "SELECT count(eleve.eleve_id) as 'nb' from eleve LEFT JOIN formationeleve where eleve.eleve_id= formationeleve.eleve_id AND formationeleve.certificat=FALSE;";
    	    String query3 = "SELECT count(eleve.eleve_id) as 'nb' from eleve LEFT JOIN formationeleve where eleve.eleve_id= formationeleve.eleve_id AND eleve.terminer='OK'";
    	    //String query4 = "Select count(eleve_id) as 'nb' from eleve where terminer ISNULL;";
    	    String query4 = "Select count(eleve_id) as 'nb' from eleve;";
    	    String query5 = "SELECT count(sexe) as 'nb' from eleve where sexe=\"Homme\"";
    	    String query6 = "SELECT count(sexe) as 'nb' from eleve where sexe=\"Femme\"";
    	    String query7 = "SELECT AVG(date('now') - eleve_date_naiss) as 'nb' from eleve";
            
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				nbLivre = rs.getInt("livre_stock");
			}
                        preparedStmt = con.prepareStatement(query2);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				nbCertif = rs.getInt("nb");
			}
                        preparedStmt = con.prepareStatement(query3);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				nbDroit = rs.getInt("nb");
			}
                        preparedStmt = con.prepareStatement(query4);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				nbEleve = rs.getInt("nb");
			}
                        preparedStmt = con.prepareStatement(query5);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				nbHomme = rs.getInt("nb");
			}
                        preparedStmt = con.prepareStatement(query6);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				nbFemme = rs.getInt("nb");
			}
                        preparedStmt = con.prepareStatement(query7);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				moyenneDage = rs.getInt("nb");
			}                            
                        con.close();
			preparedStmt.close();
			rs.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        int nombreLivreVendu = 0;
        int nombreLivreEnStock = 0;
        int totalLivre = 0;
        txtStockLivre.setText(""+nbLivre);
        txtNbEleve.setText(""+nbEleve);
        txtDroitOk.setText(""+nbDroit);
        txtNbCertificat.setText(""+nbCertif);
        txtHomme.setText(""+nbHomme);
        txtFemme.setText(""+nbFemme);
        txtAvg.setText(""+moyenneDage);
    }
    
    @FXML
    public void loadLineChart1(){
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Year");
        // Customize the x-axis, so points are scattred uniformly
        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(1900);
        xAxis.setUpperBound(2300);
        xAxis.setTickUnit(50);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Effectif des élèves");
        lineChart1 = new LineChart<>(xAxis, yAxis);
        lineChart1.setTitle("Population by Year and Country");
        
        XYChart.Series<Number, Number> seriesIndia = new XYChart.Series<>();
        seriesIndia.setName("India");
        seriesIndia.getData().addAll(new XYChart.Data<>(1950, 358),
        new XYChart.Data<>(2000, 1017),
        new XYChart.Data<>(2050, 1531),
        new XYChart.Data<>(2100, 1458),
        new XYChart.Data<>(2150, 1308));
        
        XYChart.Series<Number, Number> seriesUSA = new XYChart.Series<>();
        seriesUSA.setName("USA");
        seriesUSA.getData().addAll(new XYChart.Data<>(1950, 158),
        new XYChart.Data<>(2000, 285),
        new XYChart.Data<>(2050, 409),
        new XYChart.Data<>(2100, 437),
        new XYChart.Data<>(2150, 453));
        // Set the data for the chart
//        ObservableList<XYChart.Series<Number,Number>> chartData =
//        XYChartDataUtil.getCountrySeries();
        ObservableList<XYChart.Series<Number, Number>> data =
        FXCollections.<XYChart.Series<Number, Number>>observableArrayList();
        data.addAll(seriesIndia, seriesUSA);
        lineChart1.setData(data);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        System.out.println("___________ Jour : " +jour);
        
        loadData();
        fillForm();
        lblDate.setText(convert(jour) + ", "+convert(date));
        
        data.add(new PieChart.Data("Homme", nbHomme));
        data.add(new PieChart.Data("Femme", nbFemme));

        pieChart.setTitle("Répartition des élèves");
        pieChart.setData(data);
        
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Year");
        // Customize the x-axis, so points are scattred uniformly
        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(1900);
        xAxis.setUpperBound(2300);
        xAxis.setTickUnit(50);
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Population (in millions)");
        lineChart1 = new LineChart<>(xAxis, yAxis);
        lineChart1.setTitle("Population by Year and Country");
        
        XYChart.Series<Number, Number> seriesIndia = new XYChart.Series<>();
        seriesIndia.setName("India");
        seriesIndia.getData().addAll(new XYChart.Data<>(1950, 358),
        new XYChart.Data<>(2000, 1017),
        new XYChart.Data<>(2050, 1531),
        new XYChart.Data<>(2100, 1458),
        new XYChart.Data<>(2150, 1308));
        
        XYChart.Series<Number, Number> seriesUSA = new XYChart.Series<>();
        seriesUSA.setName("USA");
        seriesUSA.getData().addAll(new XYChart.Data<>(1950, 158),
        new XYChart.Data<>(2000, 285),
        new XYChart.Data<>(2050, 409),
        new XYChart.Data<>(2100, 437),
        new XYChart.Data<>(2150, 453));
        // Set the data for the chart
//        ObservableList<XYChart.Series<Number,Number>> chartData =
//        XYChartDataUtil.getCountrySeries();
        ObservableList<XYChart.Series<Number, Number>> data =
        FXCollections.<XYChart.Series<Number, Number>>observableArrayList();
        data.addAll(seriesIndia, seriesUSA);
        lineChart1.setData(data);

    }
    
    
}
