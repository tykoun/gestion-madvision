package app.controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import app.model.FormationEleve;
import app.model.Versement;
import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXToggleButton;
import java.sql.SQLException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class VersementController implements Initializable {

    @FXML
    private ComboBox<String> cbxModule;

    @FXML
    private TableColumn<Versement, Integer> montant;
    
    @FXML
    private JFXTextField txtSearch;

    @FXML
    private TableColumn<Versement, String> date_vers;

    @FXML
    private TableColumn<Versement, String> formation;

    @FXML
    private TableColumn<Versement, String> nom;

    @FXML
    private TableColumn<Versement, String> motif;

    @FXML
    private TableColumn<Versement, Integer> numEleve;

    @FXML
    private TableView<Versement> tableVersement;

    @FXML
    private TableColumn<Versement, Integer> vers_id;

    @FXML
    private TableColumn<Versement, Integer> recu;

    @FXML
    private TableColumn<Versement, String> arret;
    
    @FXML
    private TextField txtRecu;

    @FXML
    private JFXTextField txtNum;

    @FXML
    private JFXTextField txtNom;

    @FXML
    private JFXTextField txtPrenom;

    @FXML
    private JFXButton btnAdd;

    @FXML
    private JFXButton btnEdit;

    @FXML
    private JFXTextField txtFormation;

    @FXML
    private TextField txtVersement;

    @FXML
    private JFXTextField txtReste;

    @FXML
    private JFXTextField txtType;

    @FXML
    private JFXTextField txtTotal;

    @FXML
    private JFXButton btnDel;
    @FXML
    private JFXTextField txttotverser;

    @FXML
    private JFXToggleButton termine;
    
    private final ObservableList<FormationEleve> eleveData = FXCollections.observableArrayList();
    private final ObservableList<String> listeModule = FXCollections.observableArrayList();
    private final ObservableList<Versement> listeVersement = FXCollections.observableArrayList();

    ElevesController ec;
    CertificatController cc;
    GestionCompteController gc;
    DashboardController dc;

    @FXML
     public void search() throws SQLException, Exception{
         
         FilteredList<Versement> filterData = new FilteredList<>(listeVersement,p->true);
         txtSearch.textProperty().addListener((observable,oldvalue,newvalue)->{
         
            filterData.setPredicate( produit->{
                
                if(newvalue == null || newvalue.isEmpty()){
                	
                    return true;
                }
                String typedText = newvalue.toLowerCase();
                
                if(produit.getNoms().toLowerCase().contains(typedText)){
                     return true;
                }
//                if(produit.getForm_nom().toLowerCase().contains(typedText)){
//                     return true;
//                }
                if(produit.getVers_motif() != null){
                    if(produit.getVers_motif().toLowerCase().contains(typedText)){
                         return true;
                    }
                    
                    if(produit.getVers_date().toLowerCase().contains(typedText)){
                         return true;
                    }   
                    
                    if( new String().valueOf(produit.getNum_recu()).toLowerCase().contains(typedText)){
                         return true;
                    }
                }
                
                if( new String().valueOf(produit.getEleve_id()).toLowerCase().contains(typedText)){
                     return true;
                }

                return false;
            });
            
            SortedList<Versement> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tableVersement.comparatorProperty());
            tableVersement.setItems(sortedList);
            
        });
     }
    public void loadView() {
        fillTable();
        checkeleve();
    }

    private void loadView2() {
        ec.loadView();
        dc.loadView();
        gc.loadView();
        cc.loadView();
    }

    void init(ElevesController elvController, CertificatController crtController, GestionCompteController gcController, DashboardController dashController) {
        ec = elvController;
        cc = crtController;
        gc = gcController;
        dc = dashController;
    }

    @FXML
    private void fillTable() {
        listeVersement.clear();
        int eleve_id = 0;
        try {
            PreparedStatement preparedStmt = null;
            ResultSet rs = null;
            Connection con = ConnectionUtil.conDB();
//            String query = "SELECT * FROM (((`versement` RIGHT JOIN eleve ON eleve.eleve_id=versement.eleve_id) JOIN formationeleve ON formationeleve.eleve_id=eleve.eleve_id) JOIN formation on formation.form_id=formationeleve.form_id) JOIN type ON type.type_id=formationeleve.type_id";
//SQLite Query

            String query = "SELECT * FROM (((`eleve` LEFT JOIN versement ON eleve.eleve_id=versement.eleve_id) JOIN formationeleve ON formationeleve.eleve_id=eleve.eleve_id) JOIN formation on formation.form_id=formationeleve.form_id) JOIN type ON type.type_id=formationeleve.type_id WHERE versement.arret = false";
            preparedStmt = con.prepareStatement(query);
            rs = preparedStmt.executeQuery();
            while (rs.next()) {
                listeVersement.add(new Versement(rs.getInt("vers_id"), rs.getInt("eleve_id"), rs.getInt("num_recu"), rs.getInt("vers_montant"), rs.getString("vers_motif"), rs.getString("vers_date"), rs.getString("form_nom"), rs.getString("eleve_nom"), rs.getString("eleve_prenom"), rs.getString("type_nom"), rs.getBoolean("arret")));
            }

            rs = null;
            preparedStmt = null;
            query = "SELECT MAX(eleve_id) as 'eleve_id' from eleve";
            preparedStmt = con.prepareStatement(query);
            rs = preparedStmt.executeQuery();
            while (rs.next()) {
                eleve_id = rs.getInt("eleve_id");
            }
            con.close();
            preparedStmt.close();
            rs.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        formation.setCellValueFactory(new PropertyValueFactory<Versement, String>("form_nom"));
        numEleve.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("eleve_id"));
        nom.setCellValueFactory(new PropertyValueFactory<Versement, String>("noms"));
        motif.setCellValueFactory(new PropertyValueFactory<Versement, String>("vers_motif"));
        date_vers.setCellValueFactory(new PropertyValueFactory<Versement, String>("vers_date_formatted"));
        montant.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("vers_montant"));
        vers_id.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("vers_id"));
        recu.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("num_recu"));
        arret.setCellValueFactory(new PropertyValueFactory<Versement, String>("arret"));
        tableVersement.setItems(listeVersement);
        if (eleve_id > 1) {
            txtNum.setText(Integer.toString(eleve_id));
        }
        System.out.println(eleve_id);
    }

    @FXML
    private void checkeleve() {
        listeModule.clear();
        eleveData.clear();
        int total = 0;
        int totVersement = 0;
        String form_nom = "", type_nom = "", eleve_nom = "", eleve_prenom = "";
        if (txtNum.getText().equals(null) || txtNum.getText().equals("")) {
            txtNum.setFocusColor(Color.RED);
            txtNum.setTooltip(new Tooltip("Eleve non trouvé!"));
            return;
        } else {
            int eleve_id = Integer.parseInt(txtNum.getText());
            try {
                txtNum.setFocusColor(Color.GREEN);
                PreparedStatement preparedStmt = null;
                PreparedStatement preparedStmt2 = null;
                ResultSet rs = null;
                ResultSet rs2 = null;
                Connection con = ConnectionUtil.conDB();

                String query = "SELECT * FROM ((((`formationeleve` JOIN formation ON formationeleve.form_id=formation.form_id) JOIN type ON type.type_id=formationeleve.type_id) JOIN module ON module.form_id=formationeleve.form_id) JOIN moduletype ON moduletype.module_id=module.module_id AND moduletype.type_id=formationeleve.type_id) JOIN eleve ON eleve.eleve_id=formationeleve.eleve_id WHERE formationeleve.eleve_id=?";
                preparedStmt = con.prepareStatement(query);
                preparedStmt.setInt(1, eleve_id);

                rs = preparedStmt.executeQuery();

                while (rs.next()) {
                    //Formation f = new Formation(rs.getInt("form_id"), rs.getString("form_nom"));
                    //System.out.println(f.getForm_id() + " - "+ f.getForm_nom());
                    total = total + rs.getInt("droit");
                    listeModule.add(rs.getString("module_nom"));

                    eleveData.add(new FormationEleve(rs.getInt("type_id"), rs.getInt("form_id"), rs.getInt("eleve_id")));
                    form_nom = rs.getString("form_nom");
                    type_nom = rs.getString("type_nom");
                    eleve_nom = rs.getString("eleve_nom");
                    eleve_prenom = rs.getString("eleve_prenom");
                }

                String query2 = "SELECT * FROM versement where eleve_id=?";
                preparedStmt2 = con.prepareStatement(query2);
                preparedStmt2.setInt(1, eleve_id);

                rs2 = preparedStmt2.executeQuery();

                while (rs2.next()) {
                    totVersement = totVersement + rs2.getInt("vers_montant");
                }

                if (eleveData.isEmpty()) {
                    tableVersement.getSelectionModel().clearSelection();
                    txtNum.setFocusColor(Color.RED);
                    txtNum.setTooltip(new Tooltip("Eleve non trouvé!"));
                    refreshTable();
                } else {
                    tableVersement.getSelectionModel().clearSelection();
                    //cbxModule.getSelectionModel().clearSelection();
                    txtRecu.clear();
                    txtVersement.clear();
                    txtFormation.setText(form_nom);
                    txtType.setText(type_nom);
                    cbxModule.setItems(listeModule);
                    txtTotal.setText(Integer.toString(total));
                    txtNom.setText(eleve_nom);
                    txtPrenom.setText(eleve_prenom);
                    txttotverser.setText(Integer.toString(totVersement));
                    txtReste.setText(Integer.toString(total - totVersement));
                }
                con.close();
                preparedStmt.close();
                rs.close();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    @FXML
    public void checkVersement() {
        Versement versement = tableVersement.getSelectionModel().getSelectedItem();
        listeModule.clear();
        //eleveData.clear();
        int total = 0;
        int totVersement = 0;

        if (versement.equals(null)) {
            tableVersement.getSelectionModel().clearSelection();
            txtFormation.clear();
            txtType.clear();
            txtNom.clear();
            txtPrenom.clear();
            txttotverser.clear();
            txtTotal.clear();
            txtReste.clear();
            cbxModule.getSelectionModel().select(-1);
        } else {

            try {
                PreparedStatement preparedStmt = null;
                PreparedStatement preparedStmt2 = null;
                ResultSet rs = null;
                ResultSet rs2 = null;
                Connection con = ConnectionUtil.conDB();

                String query = "SELECT module.module_nom, moduletype.droit FROM ((`formationeleve` JOIN module ON module.form_id=formationeleve.form_id) JOIN moduletype ON moduletype.module_id=module.module_id AND moduletype.type_id=formationeleve.type_id) JOIN eleve ON eleve.eleve_id=formationeleve.eleve_id WHERE formationeleve.eleve_id=?";
                preparedStmt = con.prepareStatement(query);
                preparedStmt.setInt(1, versement.getEleve_id());

                rs = preparedStmt.executeQuery();

                while (rs.next()) {
                    //Formation f = new Formation(rs.getInt("form_id"), rs.getString("form_nom"));
                    //System.out.println(f.getForm_id() + " - "+ f.getForm_nom());
                    total = total + rs.getInt("droit");
                    listeModule.add(rs.getString("module_nom"));
                }
                preparedStmt = null;
                preparedStmt2 = null;
                rs = null;
                rs2 = null;
                String query2 = "SELECT SUM(vers_montant) as 'tot' FROM `versement` WHERE eleve_id=?";
                preparedStmt2 = con.prepareStatement(query2);
                preparedStmt2.setInt(1, versement.getEleve_id());

                rs2 = preparedStmt2.executeQuery();

                while (rs2.next()) {
                    totVersement = totVersement + rs2.getInt("tot");
                }
                preparedStmt2.close();
                rs2.close();
                con.close();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            txtFormation.setText(versement.getForm_nom());
            txtType.setText(versement.getType());
            txtNom.setText(versement.getNom());
            txtPrenom.setText(versement.getPrenom());
            txtNum.setText(Integer.toString(versement.getEleve_id()));
            txtRecu.setText(Integer.toString(versement.getNum_recu()));
            txtVersement.setText(Integer.toString(versement.getVers_montant()));
            txttotverser.setText(Integer.toString(totVersement));
            txtTotal.setText(Integer.toString(total));
            txtReste.setText(Integer.toString(total - totVersement));
            cbxModule.setItems(listeModule);
            cbxModule.getSelectionModel().select(versement.getVers_motif());
            System.out.println(versement.getVers_motif());
        }

    }

    @FXML
    private void addVersement() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setContentText("Ajouter ce versement ?");
        alert.setHeaderText(null);

        Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();

        if (action.get() == javafx.scene.control.ButtonType.OK) {
            try {
                PreparedStatement preparedStmt = null;
                Connection con = ConnectionUtil.conDB();
                con.setAutoCommit(false);

                String query1 = "INSERT INTO versement(vers_motif, vers_montant, vers_date, num_recu, eleve_id) VALUES (?,?,?,?,?);";
//        	    String query2 = "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);";
//        	    String query = "START TRANSACTION;"
//        	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//        	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//        	    		"ROLLBACK;";
                String query2 = "UPDATE eleve SET terminer='OK' WHERE eleve_id=?";

                preparedStmt = con.prepareStatement(query1);
                preparedStmt.setString(1, cbxModule.getSelectionModel().getSelectedItem());
                preparedStmt.setInt(2, Integer.parseInt(txtVersement.getText()));
                preparedStmt.setString(3, LocalDate.now().toString());
                preparedStmt.setInt(4, Integer.parseInt(txtRecu.getText()));
                preparedStmt.setInt(5, Integer.parseInt(txtNum.getText()));
                preparedStmt.executeUpdate();

                if (Integer.parseInt(txtVersement.getText()) - Integer.parseInt(txtReste.getText()) == 0) {
                    preparedStmt = con.prepareStatement(query2);
                    preparedStmt.setInt(1, Integer.parseInt(txtNum.getText()));
                    preparedStmt.executeUpdate();
                }

                if (Integer.parseInt(txtVersement.getText()) + Integer.parseInt(txttotverser.getText()) > Integer.parseInt(txtTotal.getText())) {
                    Alert alert2 = new Alert(AlertType.ERROR);
                    alert2.setTitle("Erreur");
                    alert2.setContentText("Le montant versé est supérieur au total à payer !");
                    alert2.setHeaderText(null);
                    preparedStmt.close();;
                    con.close();
                    alert2.showAndWait();

                } else {
                    con.commit();

                    con.close();
                    preparedStmt.close();

                    System.out.println("Module : Added succesfully !! ");
                    //FormData.add(new Formation(txtFormation.getText()));
                    fillTable();
                    refreshTable();
                    loadView2();
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (txtVersement.getText().length() == 0) {
                    txtVersement.setStyle("-fx-border-color: RED");
                }
                if (txtRecu.getText().length() == 0) {
                    txtRecu.setStyle("-fx-border-color: RED");
                }
                if (cbxModule.getSelectionModel().isEmpty()) {
                    cbxModule.setStyle("-fx-border-color: RED");
                }
            }
            //refreshTable();
//        	txtFormation.clear();
//        	txtDroit.clear();
//        	txtNomModule.clear();
//        	cbxFormation.getSelectionModel().clearSelection();;
//        	cbxType.getSelectionModel().clearSelection();
//        	checkFormation();
        }
    }

    private void refreshTable() {
        txtFormation.clear();
        txtType.clear();
        txtNom.clear();
        txtPrenom.clear();
        txttotverser.clear();
        txtTotal.clear();
        txtReste.clear();
        txtRecu.clear();
        txtVersement.clear();
        cbxModule.getSelectionModel().select(-1);
        txtNum.clear();
    }

    @FXML
    private void editVersement() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setContentText("Voulez-vous vraiment modifier ce Versement ?");
        alert.setHeaderText(null);

        Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();

        if (action.get() == javafx.scene.control.ButtonType.OK) {
            try {
                PreparedStatement preparedStmt = null;
                Connection con = ConnectionUtil.conDB();
                con.setAutoCommit(false);

                String query1 = "UPDATE versement SET vers_motif=?, vers_montant=?, num_recu=? WHERE vers_id=?;";
//        	    String query2 = "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);";
//        	    String query = "START TRANSACTION;"
//        	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//        	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//        	    		"ROLLBACK;";
                String query2 = "UPDATE eleve SET terminer='OK' WHERE eleve_id=?";

                preparedStmt = con.prepareStatement(query1);
                preparedStmt.setString(1, cbxModule.getSelectionModel().getSelectedItem());
                preparedStmt.setInt(2, Integer.parseInt(txtVersement.getText()));
                //preparedStmt.setString(3, LocalDate.now().toString());
                preparedStmt.setInt(3, Integer.parseInt(txtRecu.getText()));
                preparedStmt.setInt(4, tableVersement.getSelectionModel().getSelectedItem().getVers_id());
                preparedStmt.executeUpdate();

                if (Integer.parseInt(txtVersement.getText()) - Integer.parseInt(txtReste.getText()) == 0) {
                    preparedStmt = con.prepareStatement(query2);
                    preparedStmt.setInt(1, Integer.parseInt(txtNum.getText()));
                    preparedStmt.executeUpdate();
                }

                if (Integer.parseInt(txtVersement.getText()) + Integer.parseInt(txttotverser.getText()) - tableVersement.getSelectionModel().getSelectedItem().getVers_montant() > Integer.parseInt(txtTotal.getText())) {
                    Alert alert2 = new Alert(AlertType.ERROR);
                    alert2.setTitle("Erreur");
                    alert2.setContentText("Le montant versé est supérieur au total à payer !");
                    alert2.setHeaderText(null);
                    preparedStmt.close();;
                    con.close();
                    txtVersement.setStyle("-fx-border-color: RED; -fx-border-width:3px;");
                    if (txtRecu.getText().length() == 0) {
                        txtRecu.setStyle("-fx-border-color: RED");
                    }
                    alert2.showAndWait();
                } else {
                    con.commit();

                    con.close();
                    preparedStmt.close();

                    System.out.println("Module : Edited succesfully !! ");
                    //FormData.add(new Formation(txtFormation.getText()));
                    fillTable();
                    refreshTable();
                    loadView2();
                }
//    			con.commit();
//
//    			con.close();
//    			preparedStmt.close();

                //System.out.println("Module : Add succesfully !! ");
                //FormData.add(new Formation(txtFormation.getText()));
                //fillTable();
            } catch (Exception e) {
                e.printStackTrace();
            }

//        	txtFormation.clear();
//        	txtDroit.clear();
//        	txtNomModule.clear();
//        	cbxFormation.getSelectionModel().clearSelection();;
//        	cbxType.getSelectionModel().clearSelection();
//        	checkFormation();
        }
    }

    @FXML
    private void delVersement() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setContentText("Voulez-vous vraiment supprimer ce Versement ?");
        alert.setHeaderText(null);

        Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();

        if (action.get() == javafx.scene.control.ButtonType.OK) {
            try {
                PreparedStatement preparedStmt = null;
                Connection con = ConnectionUtil.conDB();
                con.setAutoCommit(false);

                String query1 = "DELETE FROM versement WHERE vers_id=?;";
//        	    String query2 = "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);";
//        	    String query = "START TRANSACTION;"
//        	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//        	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//        	    		"ROLLBACK;";
                String query2 = "UPDATE eleve SET terminer=NULL WHERE eleve_id=?";

                preparedStmt = con.prepareStatement(query1);
                //preparedStmt.setString(1, cbxModule.getSelectionModel().getSelectedItem());
                //preparedStmt.setInt(2, Integer.parseInt(txtVersement.getText()));
                //preparedStmt.setString(3, LocalDate.now().toString());
                //preparedStmt.setInt(3, Integer.parseInt(txtRecu.getText()));
                preparedStmt.setInt(1, tableVersement.getSelectionModel().getSelectedItem().getVers_id());
                preparedStmt.executeUpdate();

                //if(Integer.parseInt(txtVersement.getText()) - Integer.parseInt(txtReste.getText()) != 0){
                preparedStmt = con.prepareStatement(query2);
                preparedStmt.setInt(1, Integer.parseInt(txtNum.getText()));
                preparedStmt.executeUpdate();
                //}

//    			if(Integer.parseInt(txtVersement.getText()) + Integer.parseInt(txttotverser.getText()) - tableVersement.getSelectionModel().getSelectedItem().getVers_montant() > Integer.parseInt(txtTotal.getText())){
//    				Alert alert2 = new Alert(AlertType.ERROR);
//    	        	alert2.setTitle("Erreur");
//    	        	alert2.setContentText("Le montant versé est supérieur au total à payer !");
//    	        	alert2.setHeaderText(null);
//    	        	preparedStmt.close();;
//    	        	con.close();
//        				txtVersement.setStyle("-fx-border-color: RED; -fx-border-width:3px;");
//        			if(txtRecu.getText().length() == 0) {
//        				txtRecu.setStyle("-fx-border-color: RED");
//        			}
//        			alert2.showAndWait();
//    			} 
//    			
//    			else {
                con.commit();

                con.close();
                preparedStmt.close();
                fillTable();
                refreshTable();
                loadView2();
//    			}
//    			con.commit();
//
//    			con.close();
//    			preparedStmt.close();

                System.out.println("Module : Deleted succesfully !! ");
                //FormData.add(new Formation(txtFormation.getText()));
                //fillTable();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

//    @FXML
//    void checkNum(ActionEvent event) {
//        System.out.println("__________KeyPressed");
//        // force the field to be numeric only
//        txtVersement.textProperty().addListener(new ChangeListener<String>() {
//               @Override
//               public void changed(ObservableValue<? extends String> observable, String oldValue, 
//                   String newValue) {
//                   if (!newValue.matches("\\d*")) {
//                       txtVersement.setText(newValue.replaceAll("[^\\d]", ""));
//                   }
//               }
//        });
//    }
    @FXML
    void switchTermine(ActionEvent event) {
        if(termine.isSelected()){
            checkTerminer(true);
        } else {
            checkTerminer(false);
        }
    }

    @FXML
    void showAll(ActionEvent event) {
        listeVersement.clear();
        int eleve_id = 0;
        try {
            PreparedStatement preparedStmt = null;
            ResultSet rs = null;
            Connection con = ConnectionUtil.conDB();
//            String query = "SELECT * FROM (((`versement` RIGHT JOIN eleve ON eleve.eleve_id=versement.eleve_id) JOIN formationeleve ON formationeleve.eleve_id=eleve.eleve_id) JOIN formation on formation.form_id=formationeleve.form_id) JOIN type ON type.type_id=formationeleve.type_id";
//SQLite Query

            String query = "SELECT * FROM (((`eleve` LEFT JOIN versement ON eleve.eleve_id=versement.eleve_id) JOIN formationeleve ON formationeleve.eleve_id=eleve.eleve_id) JOIN formation on formation.form_id=formationeleve.form_id) JOIN type ON type.type_id=formationeleve.type_id";
            preparedStmt = con.prepareStatement(query);
            rs = preparedStmt.executeQuery();
            while (rs.next()) {
                listeVersement.add(new Versement(rs.getInt("vers_id"), rs.getInt("eleve_id"), rs.getInt("num_recu"), rs.getInt("vers_montant"), rs.getString("vers_motif"), rs.getString("vers_date"), rs.getString("form_nom"), rs.getString("eleve_nom"), rs.getString("eleve_prenom"), rs.getString("type_nom"), rs.getBoolean("arret")));
            }

            rs = null;
            preparedStmt = null;
            query = "SELECT MAX(eleve_id) as 'eleve_id' from eleve";
            preparedStmt = con.prepareStatement(query);
            rs = preparedStmt.executeQuery();
            while (rs.next()) {
                eleve_id = rs.getInt("eleve_id");
            }
            con.close();
            preparedStmt.close();
            rs.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        formation.setCellValueFactory(new PropertyValueFactory<Versement, String>("form_nom"));
        numEleve.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("eleve_id"));
        nom.setCellValueFactory(new PropertyValueFactory<Versement, String>("noms"));
        motif.setCellValueFactory(new PropertyValueFactory<Versement, String>("vers_motif"));
        date_vers.setCellValueFactory(new PropertyValueFactory<Versement, String>("vers_date_formatted"));
        montant.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("vers_montant"));
        vers_id.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("vers_id"));
        recu.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("num_recu"));
        arret.setCellValueFactory(new PropertyValueFactory<Versement, String>("arret"));
       tableVersement.setItems(listeVersement);
        if (eleve_id > 1) {
            txtNum.setText(Integer.toString(eleve_id));
        }
        System.out.println(eleve_id);
    }

    void checkTerminer(boolean termine) {
        listeVersement.clear();
        int eleve_id = 0;
        try {
            PreparedStatement preparedStmt = null;
            ResultSet rs = null;
            Connection con = ConnectionUtil.conDB();
//            String query = "SELECT * FROM (((`versement` RIGHT JOIN eleve ON eleve.eleve_id=versement.eleve_id) JOIN formationeleve ON formationeleve.eleve_id=eleve.eleve_id) JOIN formation on formation.form_id=formationeleve.form_id) JOIN type ON type.type_id=formationeleve.type_id";
//SQLite Query

            String query = "SELECT * FROM (((`eleve` LEFT JOIN versement ON eleve.eleve_id=versement.eleve_id) JOIN formationeleve ON formationeleve.eleve_id=eleve.eleve_id) JOIN formation on formation.form_id=formationeleve.form_id) JOIN type ON type.type_id=formationeleve.type_id WHERE versement.arret="+termine;
            preparedStmt = con.prepareStatement(query);
            rs = preparedStmt.executeQuery();
            while (rs.next()) {
                listeVersement.add(new Versement(rs.getInt("vers_id"), rs.getInt("eleve_id"), rs.getInt("num_recu"), rs.getInt("vers_montant"), rs.getString("vers_motif"), rs.getString("vers_date"), rs.getString("form_nom"), rs.getString("eleve_nom"), rs.getString("eleve_prenom"), rs.getString("type_nom"), rs.getBoolean("arret")));
            }

            rs = null;
            preparedStmt = null;
            query = "SELECT MAX(eleve_id) as 'eleve_id' from eleve";
            preparedStmt = con.prepareStatement(query);
            rs = preparedStmt.executeQuery();
            while (rs.next()) {
                eleve_id = rs.getInt("eleve_id");
            }
            con.close();
            preparedStmt.close();
            rs.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        formation.setCellValueFactory(new PropertyValueFactory<Versement, String>("form_nom"));
        numEleve.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("eleve_id"));
        nom.setCellValueFactory(new PropertyValueFactory<Versement, String>("noms"));
        motif.setCellValueFactory(new PropertyValueFactory<Versement, String>("vers_motif"));
        date_vers.setCellValueFactory(new PropertyValueFactory<Versement, String>("vers_date_formatted"));
        montant.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("vers_montant"));
        vers_id.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("vers_id"));
        recu.setCellValueFactory(new PropertyValueFactory<Versement, Integer>("num_recu"));
        arret.setCellValueFactory(new PropertyValueFactory<Versement, String>("arret"));
        tableVersement.setItems(listeVersement);
        if (eleve_id > 1) {
            txtNum.setText(Integer.toString(eleve_id));
        }
        System.out.println(eleve_id);
    }
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub
        fillTable();
        checkeleve();
        
        
    txtVersement.textProperty().addListener(new ChangeListener<String>() {

                    @Override
                    public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                            if(!txtVersement.getText().matches("\\d{1,15}")) {
                                    //txtStock.setStyle("-fx-background-color: #FF514F");
                                    txtVersement.setText("");
                            }
                            else {
                                    txtVersement.setStyle("-fx-border-color: WHITE");
                            }
                    }
            });
    txtRecu.textProperty().addListener(new ChangeListener<String>() {

                    @Override
                    public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                            if(!txtRecu.getText().matches("\\d{1,15}")) {
                                    //txtStock.setStyle("-fx-background-color: #FF514F");
                                    txtRecu.setText("");
                            }
                            else {
                                    txtRecu.setStyle("-fx-border-color: WHITE");
                            }
                    }
            });
    
        txtNum.textProperty().addListener(new ChangeListener<String>() {

                    @Override
                    public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                            if(!txtNum.getText().matches("\\d{1,15}")) {
                                    //txtStock.setStyle("-fx-background-color: #FF514F");
                                    txtNum.setText("");
                            }
                            else {
                                    txtNum.setStyle("-fx-border-color: WHITE");
                            }
                    }
            });
    }
}
