/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author Finaritra
 */
public class SingInController implements Initializable{
    
    @FXML
    private JFXButton btnAdd;

    @FXML
    private TextField txtName;

    @FXML
    private PasswordField txtPw1;

    @FXML
    private PasswordField txtPw2;

    @FXML
    private JFXButton btnClose;
    
    @FXML
    void close() {
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    boolean checkpwd(){
        if(txtPw1.getText().equals(txtPw2.getText()) && txtName.getText().length() != 0 && txtPw1.getText().length()!=0 && txtPw2.getText().length()!=0) return true;
        return false;
    }
    
    @FXML
    void creer(ActionEvent event) {
        if(checkpwd()){
            PreparedStatement preparedStmt = null;
            
        	try {
        	    Connection con = ConnectionUtil.conDB();
                    //con.setAutoCommit(false);
                    int rest_stock = 0;
                    String query = "INSERT INTO user(login, password) values(? , ?)";
			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, txtName.getText());
    			preparedStmt.setString(2, txtPw1.getText());
			preparedStmt.execute();

			preparedStmt.close();
			con.close();
                        

    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} 
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
        	alert.setTitle("Compte créé");
        	alert.setContentText("Votre compte à été créer avec succès");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
                close();
        } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Erreur");
        	alert.setContentText("Veuillez vérifier vos informations");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        }
        
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }

}

