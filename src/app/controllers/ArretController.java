/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.model.Versement;
import app.utils.ConnectionUtil;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 *
 * @author Finaritra
 */
public class ArretController implements Initializable{
    @FXML
    private TableView<Versement> tableArret;

    @FXML
    private TableColumn<Versement, String> compteColumn;

    @FXML
    private TableColumn selectColumn;

    @FXML
    private Button btnArretCompte;

    @FXML
    private Button btnCancelArret;

    private final ObservableList<String> arretData = FXCollections.observableArrayList("Recettes/Dépenses", "Livres");
    private final ObservableList<Versement> versData = FXCollections.observableArrayList();

    ElevesController ec;
    VersementController vc;
    LivreController lc;
    DepenseController depc;
    DashboardController dashc;
    GestionCompteController gc;
    
    void init(VersementController vrsController, DepenseController depenseController, LivreController livreController, ElevesController elvController, DashboardController dashController, GestionCompteController gcController) {
        ec = elvController;
        vc = vrsController;
        lc = livreController;
        depc = depenseController;
        dashc = dashController;
        gc = gcController;
    }
    
    @FXML
    void arreter(ActionEvent event) {
    		try {
                    PreparedStatement preparedStmt = null;
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "UPDATE `transaction` SET arret=TRUE";
        	    String query2 = "UPDATE `versement` SET `arret`=TRUE";
        	    String query3 = "UPDATE `livre` SET `arret`=TRUE WHERE vendu = TRUE";
        	    String query4 = "UPDATE livre SET livre_qte=livre_stock WHERE arret=FAlSE";
        	    //con.setAutoCommit(false);

    	    			preparedStmt = con.prepareStatement(query);
    	    			preparedStmt.execute();

    	    			preparedStmt = con.prepareStatement(query3);
    	    			preparedStmt.execute();
                                
    	    			preparedStmt = con.prepareStatement(query4);
    	    			preparedStmt.execute();

    	    			preparedStmt = con.prepareStatement(query2);
    	    			preparedStmt.execute();
        		        		
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setContentText("Compte arrêté avec succès");
			alert.showAndWait();
        		preparedStmt.close(); con.close();
                        close();
                        } catch (Exception e2) {
				// TODO: handle exception
			}

    }

    @SuppressWarnings("unchecked")
	@FXML
    private void fillTableArret() {
    	versData.clear();
    	arretData.clear();
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT vers_id, vers_motif FROM `versement` GROUP BY vers_motif";
    	    
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			versData.add(new Versement(0, "Recettes/Dépenses"));
			versData.add(new Versement(0, "Livres"));
			while(rs.next()) {
				versData.add(new Versement(rs.getInt("vers_id"), rs.getString("vers_motif")));
					//credit = credit + rs.getInt("trans_montant");
			}
			preparedStmt.close();
			rs.close();
			con.close();
		} catch (Exception except) {
			// TODO Auto-generated catch block
			except.printStackTrace();
		}
    	
    	if(tableArret == null || tableArret == null) {
    		System.out.println("Null");
    		//tableTransaction.getScene().getWindow();
    		//tableTransaction.setItems(transData);
    	} else {
    	compteColumn.setCellValueFactory(new PropertyValueFactory<Versement, String>("vers_motif"));
    	
    	
    	Callback<TableColumn<Versement, String>, TableCell<Versement, String>> CellFactory = (param) -> {
    		
    		final TableCell<Versement, String> cell= new TableCell<Versement, String>(){
    			@Override
    			public void updateItem(String item, boolean empty) {
    				super.updateItem(item, empty);
    				
    				if(empty) {
    					setGraphic(null);
    					setText(null);
    				} else {
    					final CheckBox checkbx = new CheckBox();
                                        checkbx.setSelected(true);
    					checkbx.setOnAction(event -> {
    						Versement v = getTableView().getItems().get(getIndex());
    						if(checkbx.isSelected()) arretData.add(v.getVers_motif());
    						else arretData.remove(v.getVers_motif());
//    						Alert alert = new Alert(Alert.AlertType.INFORMATION);
//    						alert.setContentText("vous avez checké sur " + v.getVers_motif());
//    						alert.show();
    						//System.out.println(arretData);
    					});
    					
    					setGraphic(checkbx);
    					setText(null);
    				}
    			}
    		};
    		
    		
    		
    		return cell;
    	};
    	
    	selectColumn.setCellFactory(CellFactory);
        tableArret.setItems(versData);
        }
    }
    
    @FXML
    void close() {
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnCancelArret.getScene().getWindow();
        stage.close();

    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
		fillTableArret();

    }
    
}
