package app.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import app.model.Certificat;
import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXToggleButton;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class CertificatController implements Initializable {

    @FXML
    private JFXTextField txtPrenom;

    @FXML
    private TableColumn<Certificat, String> noms;

    @FXML
    private JFXTextField txtNum;

    @FXML
    private DatePicker txtDate;

    @FXML
    private JFXTextField txtSearch;
    
    @FXML
    private JFXButton btnEdit;
    
    @FXML
    private Label lblAge;
    
    @FXML
    private JFXButton btnExport;
    
    @FXML
    private JFXTextField txtLieuNaissance;

    @FXML
    private TableColumn<Certificat, Integer> num;

    @FXML
    private TableColumn<Certificat, String> date_lieu;

    @FXML
    private TableView<Certificat> tableCertificat;

    @FXML
    private TableColumn<Certificat, String> export;

    @FXML
    private JFXTextField txtNom;
    
    @FXML
    private JFXTextField txtTel;
    
    @FXML
    private JFXToggleButton toggleCertificatList;

    @FXML
    private JFXToggleButton toggleCertificatSingle;
    
    private final ObservableList<Certificat> certifData = FXCollections.observableArrayList();
    DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");

    ElevesController ec;
    VersementController vc;
    DashboardController dc;
    
     @FXML
     public void search() throws SQLException, Exception{
         
         FilteredList<Certificat> filterData = new FilteredList<>(certifData,p->true);
         txtSearch.textProperty().addListener((observable,oldvalue,newvalue)->{
         
            filterData.setPredicate( produit->{
                
                if(newvalue == null || newvalue.isEmpty()){
                	
                    return true;
                }
                String typedText = newvalue.toLowerCase();
                
                if(produit.getNoms().toLowerCase().contains(typedText)){
                     return true;
                }
//                if(produit.getForm_nom().toLowerCase().contains(typedText)){
//                     return true;
//                }
                if(produit.getDate_lieu().toLowerCase().contains(typedText)){
                     return true;
                }               
                if( new String().valueOf(produit.getEleve_id()).toLowerCase().contains(typedText)){
                     return true;
                }
                if(produit.getEleve_date_naiss().toLowerCase().contains(typedText)){
                     return true;
                }   
                return false;
            });
            
            SortedList<Certificat> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tableCertificat.comparatorProperty());
            tableCertificat.setItems(sortedList);
            
        });
     }
    
    public void loadView(){
        fillTable();
        ec.loadView();
        vc.loadView();
        dc.loadView();
    }
    
    void init(ElevesController elvController, VersementController vrsController, DashboardController dashController) {
        vc = vrsController;
        ec = elvController;
        dc = dashController;
    } 
    
    @FXML
    void toggleCertificatList(ActionEvent event) {
        if(toggleCertificatList.isSelected()){
            fillTable(certifData, "true");
            btnExport.setDisable(true);
        } else {
            fillTable(certifData, "false");
            btnExport.setDisable(false);
        }
    }

    @FXML
    void toggleCertificatSingle(ActionEvent event) {

    }
    
    @FXML
    private void fillTable(ObservableList<Certificat> data, String terminer) {
    	data.clear();
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * FROM (`formationeleve` JOIN eleve ON eleve.eleve_id=formationeleve.eleve_id) JOIN formation ON formationeleve.form_id=formation.form_id WHERE eleve.terminer='OK' AND formationeleve.certificat="+terminer;
    	    //String query = "SELECT * FROM (`formationeleve` JOIN eleve ON eleve.eleve_id=formationeleve.eleve_id) JOIN formation ON formationeleve.form_id=formation.form_id WHERE eleve.terminer='OK'";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				data.add(new Certificat(rs.getInt("eleve_id"), rs.getString("eleve_nom"),rs.getString("eleve_prenom"), rs.getInt("form_id"), rs.getString("eleve_date_naiss"), rs.getString("eleve_lieu_naiss"), rs.getString("date_rentree"), rs.getInt("type_id"), rs.getBoolean("certificat"), rs.getString("eleve_tel"), rs.getBoolean("eleve_annee_vers")));
			}
			con.close();
			preparedStmt.close();
			rs.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	num.setCellValueFactory(new PropertyValueFactory<Certificat, Integer>("eleve_id"));
    	noms.setCellValueFactory(new PropertyValueFactory<Certificat, String>("noms"));
    	date_lieu.setCellValueFactory(new PropertyValueFactory<Certificat, String>("date_lieu"));
    	export.setCellValueFactory(new PropertyValueFactory<Certificat, String>("certificat_str"));

    	tableCertificat.setItems(data);
        if(data.size() == 0) btnExport.setDisable(true);
    }

    @FXML
    private void fillTable() {
    	certifData.clear();
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * FROM (`formationeleve` JOIN eleve ON eleve.eleve_id=formationeleve.eleve_id) JOIN formation ON formationeleve.form_id=formation.form_id WHERE eleve.terminer='OK' AND formationeleve.certificat=false";
    	    //String query = "SELECT * FROM (`formationeleve` JOIN eleve ON eleve.eleve_id=formationeleve.eleve_id) JOIN formation ON formationeleve.form_id=formation.form_id WHERE eleve.terminer='OK'";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				certifData.add(new Certificat(rs.getInt("eleve_id"), rs.getString("eleve_nom"),rs.getString("eleve_prenom"), rs.getInt("form_id"), rs.getString("eleve_date_naiss"), rs.getString("eleve_lieu_naiss"), rs.getString("date_rentree"), rs.getInt("type_id"), rs.getBoolean("certificat"), rs.getString("eleve_tel"), rs.getBoolean("eleve_annee_vers")));
			}
			con.close();
			preparedStmt.close();
			rs.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	num.setCellValueFactory(new PropertyValueFactory<Certificat, Integer>("eleve_id"));
    	noms.setCellValueFactory(new PropertyValueFactory<Certificat, String>("noms"));
    	date_lieu.setCellValueFactory(new PropertyValueFactory<Certificat, String>("date_lieu"));
    	export.setCellValueFactory(new PropertyValueFactory<Certificat, String>("certificat_str"));

    	tableCertificat.setItems(certifData);
    }
    
    @FXML
    private void setForm() {
    	Certificat cert = tableCertificat.getSelectionModel().getSelectedItem();
    	txtNom.setText(cert.getNom());
    	txtPrenom.setText(cert.getPrenom());
    	txtNum.setText(Integer.toString(cert.getEleve_id()));
    	txtDate.setValue(LocalDate.parse(cert.getEleve_date_naiss()));
    	txtLieuNaissance.setText(cert.getEleve_lieu_naiss());
    	txtTel.setText(cert.getEleve_tel());
        lblAge.setText("("+cert.getAge()+" ans)");
        toggleCertificatSingle.setSelected(cert.getCertificat());
    }
    
    @FXML
    private void update() {
        
        if(tableCertificat.getSelectionModel().isEmpty()) System.out.println("hahaha");
        
        if(txtLieuNaissance.getText().length() == 0 || txtNom.getText().length() == 0 ||
                txtNum.getText().length() == 0) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setContentText("Veuillez remplire toutes les informations");
            alert.setHeaderText(null);
            alert.show();
        } else {
        Certificat fe = tableCertificat.getSelectionModel().getSelectedItem();		
        Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation");
    	alert.setContentText("Mettre à jour les informations sur cet élèves ?");
    	alert.setHeaderText(null);
    	
    	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
    	
    	if(action.get() == javafx.scene.control.ButtonType.OK) {
    	try {
    		PreparedStatement preparedStmt = null;
    	    Connection con = ConnectionUtil.conDB();
    	    con.setAutoCommit(false);
    	    
    	    String query1 = "UPDATE eleve SET eleve_nom=?,eleve_prenom=?,eleve_date_naiss=?,"
    	    		+ "eleve_lieu_naiss=?"
    	    		+ " WHERE eleve_id=?;";
    	    String query2 = "UPDATE formationeleve SET certificat=? WHERE eleve_id=?";
            
			preparedStmt = con.prepareStatement(query1);
			preparedStmt.setString(1, txtNom.getText());
			preparedStmt.setString(2, txtPrenom.getText());
			preparedStmt.setString(3, txtDate.getValue().toString());
			preparedStmt.setString(4, txtLieuNaissance.getText());
			preparedStmt.setInt(5, fe.getEleve_id());
			
			preparedStmt.executeUpdate();

			preparedStmt = con.prepareStatement(query2);
                        preparedStmt.setBoolean(1, toggleCertificatSingle.isSelected());
			preparedStmt.setInt(2, fe.getEleve_id());
			preparedStmt.executeUpdate();
                        
			con.commit();
			
			con.close();
			preparedStmt.close();
			
			System.out.println("Info eleve: Edited succesfully !! ");
			//FormData.add(new Formation(txtFormation.getText()));
			//fillModule();
                        if(toggleCertificatList.isSelected()) {
                            fillTable(certifData, "true");
                        } else fillTable(certifData, "false");
                        
                        vc.loadView();
                        dc.loadView();
                        ec.loadView();
                        
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
        }
}
    
    void exported(){
    	try {
            PreparedStatement preparedStmt = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query2 = "UPDATE `formationeleve` SET `certificat`=true WHERE (SELECT `eleve_id` from eleve WHERE eleve.terminer='OK' AND formationeleve.eleve_id=eleve.eleve_id)";
                        
			preparedStmt = con.prepareStatement(query2);
			preparedStmt.execute();
                        
			con.close();
			preparedStmt.close();

                        
                        vc.loadView();
                        dc.loadView();
                        ec.loadView();
        
        } catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
    }
    
    @FXML
    private void export() {
    	Window stage = btnExport.getScene().getWindow();
    	FileChooser fc = new FileChooser();
    	fc.setTitle("Exporter Certificat");
    	fc.setInitialFileName("Certificat-"+ LocalDate.now().format(dateformatter).toString());
    	fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Fichier excel", "*.xls"));
    	fc.setInitialDirectory(new File(System.getProperty("user.home")+"\\Documents"));
    	
    	File file = fc.showSaveDialog(stage);
    	
    	//System.out.println(file.getAbsolutePath());
    	
    	try {
        	HSSFWorkbook workbook = new HSSFWorkbook();
        	HSSFSheet sheet = workbook.createSheet("Certificat");
        	
        	int ligne = 0;
        	for(Certificat x : certifData) {
        		HSSFRow row = sheet.createRow(ligne);
        		HSSFCell cell = row.createCell(0);
        		cell.setCellValue(x.getNoms());
        		
        		cell = row.createCell(1);
        		cell.setCellValue(x.getDate_lieu());
        		sheet.autoSizeColumn(0);
        		sheet.autoSizeColumn(1);
        		ligne++;
        	}
        		FileOutputStream fileOut = new FileOutputStream(file.getAbsolutePath());
			workbook.write(fileOut);
			fileOut.close();
			workbook.close();
                        
                        exported();
                        loadView();
                        
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Exportation réussie");
                alert.setContentText("Exportation des données effectuée avec succès");
                alert.setHeaderText(null);

                alert.showAndWait();
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
    	System.out.println("exported");
    }

    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		fillTable();
                if(certifData.size() == 0) btnExport.setDisable(true);
	}

}
