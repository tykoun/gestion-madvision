/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.model.Categorie;
import app.utils.ConnectionUtil;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 *
 * @author Finaritra
 */
public class CategorieController implements Initializable{
    @FXML
    private TextField txtDepense;

    @FXML
    private TableView<Categorie> table;

    @FXML
    private TableColumn<Categorie, String> columnCategorie;
    @FXML
    private Button btnCancel;
    private final ObservableList<Categorie> categorieData = FXCollections.observableArrayList();

    DepenseController dc;
    
    void init(DepenseController depenseController) {
        dc = depenseController;
    }
    
    @FXML
    void ajouter(ActionEvent event) {
        if(txtDepense.getText().length() == 0 || table.getSelectionModel().isEmpty()) {
    		System.out.println("Hahahahahhahaha");
    	} else {
    		
    		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Voulez-vous ajouter \"" + txtDepense.getText() + "\" dans Catégorie ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "INSERT INTO categorie (cat_designation, trans_type_id) values(?, ?)";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, txtDepense.getText());
    			preparedStmt.setInt(2, 2);
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Add succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
                        categorieData.clear();
    			fillCategorie();
                        txtDepense.clear();

//    			dc.loadView();
                        
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	//refreshTable();
            }
        }
    }

    @FXML
    void modifier(ActionEvent event) {
        if(txtDepense.getText().length() == 0 || table.getSelectionModel().isEmpty()) {
    		System.out.println("Hahahahahhahaha");
    	} else {
    		
    		Alert alert = new Alert(Alert.AlertType.WARNING);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Voulez-vous vraiment modifier la catégorie \"" + table.getSelectionModel().getSelectedItem().getCat_designation().toString()+ "\" ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
    		int catId = table.getSelectionModel().getSelectedItem().getCat_id();

        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "UPDATE categorie set cat_designation=? WHERE cat_id = ?";
        	    String query2 = "UPDATE 'transaction' set trans_design=? WHERE trans_design = ?";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, txtDepense.getText());
    			preparedStmt.setInt(2, catId);
    			
    			preparedStmt.execute();
                        
    			preparedStmt = con.prepareStatement(query2);
    			preparedStmt.setString(1, txtDepense.getText());
    			preparedStmt.setString(2, table.getSelectionModel().getSelectedItem().getCat_designation());
    			
    			preparedStmt.execute();
                        preparedStmt.close();
    			con.close();
    			System.out.println("Edited succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
                        categorieData.clear();
    			fillCategorie();
                        txtDepense.clear();

    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	//refreshTable();
            }
        }
    }

    @FXML
    void supprimer(ActionEvent event) {
        if(txtDepense.getText().length() == 0 || table.getSelectionModel().isEmpty()) {
    		System.out.println("Hahahahahhahaha");
    	} else {
    		
    		Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Tous les enregistrements liés à "+ table.getSelectionModel().getSelectedItem().getCat_designation().toString()+" seront supprimés\n Cette action est irreversible");
        	alert.setHeaderText("Voulez-vous vraiment Supprimer\"" + table.getSelectionModel().getSelectedItem().getCat_designation().toString()+ "\" ?");
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
    		int catId = table.getSelectionModel().getSelectedItem().getCat_id();

        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "DELETE FROM categorie WHERE cat_id = ?";
        	    String query2 = "DELETE FROM transaction WHERE trans_design = ?";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setInt(1, catId);
    			
    			preparedStmt.execute();
                        
    			preparedStmt = con.prepareStatement(query2);
    			preparedStmt.setString(1, table.getSelectionModel().getSelectedItem().getCat_designation());
    			
    			preparedStmt.execute();                        
    			preparedStmt.close();
    			con.close();
    			System.out.println("Deleted succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
                        categorieData.clear();
    			fillCategorie();
                        txtDepense.clear();

//    			dc.loadView();
                        
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	//refreshTable();
            }
        }
    }
    
    @FXML
    void setFormTxt(){
    	String selectedItem = table.getSelectionModel().getSelectedItem().getCat_designation().toString();
    	//selectedIndex = listeFormation.getSelectionModel().getSelectedIndex();
    	txtDepense.setText(selectedItem);
    }
    
    @FXML
    void close(ActionEvent event) {
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();

    }
    
        //FUNCTIONS ...
    @FXML
    void fillCategorie(){
        StringConverter<Categorie> converter = new StringConverter<Categorie>() {

            @Override
            public Categorie fromString(String arg0) {
                    // TODO Auto-generated method stub
                    return null;
            }

            @Override
            public String toString(Categorie obj) {
                return obj.getCat_designation();
            }
			
        };
    	
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * from categorie";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				categorieData.add(new Categorie(rs.getInt("cat_id"), rs.getString("cat_designation"), rs.getInt("trans_type_id")));
			}
			con.close();
			preparedStmt.close();
			rs.close();
                        
                        columnCategorie.setCellValueFactory(new PropertyValueFactory<Categorie, String>("cat_designation"));
                        table.setItems(categorieData);
                        

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fillCategorie();
    }

}
