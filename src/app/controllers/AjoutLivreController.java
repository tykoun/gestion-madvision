/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author Finaritra
 */
public class AjoutLivreController implements Initializable{
    @FXML
    private JFXButton btnCancel;

    @FXML
    private TextField txtNumber;

    private int latest_livre_id = 0;
    
    @FXML
    void close() {
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }
    
    @FXML
    void ajouter(){
        if(txtNumber.getText().length() == 0) {
    		System.out.println("Hahahahahhahaha");
    	} else {
    		
    		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Ajouter \"" + txtNumber.getText() + "\" livre ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
                ResultSet rs = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
                    //con.setAutoCommit(false);
                    int rest_stock = 0;
                    String query = "SELECT * FROM livre WHERE vendu=0";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
                            latest_livre_id = rs.getInt("livre_id");
                            rest_stock = rs.getInt("livre_stock");
			}
			//con.close();
			//preparedStmt.close();
			//rs.close();
                        
                    if(latest_livre_id == 0){
                        String query2 = "INSERT INTO livre (livre_date, livre_qte, livre_stock) values(?, ?, ?)";
    			preparedStmt = con.prepareStatement(query2);
    			preparedStmt.setString(1, LocalDate.now().toString());
    			preparedStmt.setInt(2, Integer.parseInt(txtNumber.getText()));
    			preparedStmt.setInt(3, Integer.parseInt(txtNumber.getText()));
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Add succesfully !! => new initial insertion ");
    			//FormData.add(new Formation(txtFormation.getText()));    	
                        close();
                    }
                    
                    else {
                        rest_stock += Integer.parseInt(txtNumber.getText());
                         String query3 = "UPDATE livre SET vendu = 1";
                         preparedStmt = con.prepareStatement(query3);
                         preparedStmt.execute();
                         
                         String query2 =" INSERT INTO livre (livre_date, livre_qte, livre_stock) values(?, ?, ?);";
                            preparedStmt = con.prepareStatement(query2);
                            //preparedStmt.setInt(1, latest_livre_id);
                            preparedStmt.setString(1, LocalDate.now().toString());
                            preparedStmt.setInt(2, Integer.parseInt(txtNumber.getText()));
                            preparedStmt.setInt(3, rest_stock);

                            preparedStmt.execute();
                            
                            preparedStmt.close();
                            con.close();
                            System.out.println("Add succesfully !! => existed stock livre");
                            //FormData.add(new Formation(txtFormation.getText()));    	
                            close();
                    }
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	//refreshTable();
            }
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txtNumber.textProperty().addListener(new ChangeListener<String>() {

                        @Override
                        public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                                if(!txtNumber.getText().matches("\\d{1,15}")) {
                                        //txtStock.setStyle("-fx-background-color: #FF514F");
                                        txtNumber.setText("");
                                }
                                else {
                                        txtNumber.setStyle("-fx-border-color: WHITE");
                                }
                        }
                });
    }
    
}
