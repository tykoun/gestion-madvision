/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

/**
 *
 * @author Finaritra
 */
import app.GMadvision;
import app.utils.ConnectionUtil;
import app.utils.Params;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LoginController implements Initializable {

    @FXML
    private HBox box_username;

    @FXML
    private TextField username;

    @FXML
    private HBox box_password;

    @FXML
    private PasswordField password;

    @FXML
    private JFXButton btnSeConnecter;

    @FXML
    private JFXButton btnSeConnecter1;
        
    @FXML
    private Label lbl_error;
    
    @FXML
    private JFXButton btnClose;

    GMadvision main;
    
    Stage stage;

    String user = "admin";
    String pw = "admin";
    String checkUser, checkPw;
    
    private final ObservableList<String> usersData = FXCollections.observableArrayList();
    private final ObservableList<Integer> usersIDData = FXCollections.observableArrayList();
 
    public void main(Stage stage, GMadvision aThis) {
    	this.stage = stage;
    	this.main = aThis;
    }   
    @FXML
    void close() {
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    
    @FXML
    void connect(ActionEvent event) throws IOException {
        boolean wrong = true;
        String login = username.getText() + password.getText();
        for (int i = 0; i < usersData.size(); i++) {
        if(login.equals(usersData.get(i)) || (password.getText().equals(pw) && username.getText().equals(user))){
            btnSeConnecter.setDisable(true);
            Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
            
            Params.username = username.getText();
            Params.password = password.getText();
            Params.userId = usersIDData.get(i);
            
            try {
                Stage stage = new Stage();
		Parent root;
            
                root = FXMLLoader.load(getClass().getResource("/app/App.fxml"));
                //Scene scene = new Scene(root);
		stage.setScene(new Scene(root));
                //stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.initStyle(StageStyle.DECORATED);
                //stage.setScene(scene);
                stage.setX(visualBounds.getMinX());
                stage.setY(visualBounds.getMinY());
                stage.setWidth(visualBounds.getWidth());
                stage.setHeight(visualBounds.getHeight());
                stage.show(); 
            } catch (Exception e) {
            }
            
            close();
            wrong = false;
            break;
        } 
    }
        if(wrong){
            System.out.println(login);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setContentText("Verifier votre nom ou mot de passe");
            alert.setHeaderText(null);
            alert.show();
        }    

}
    
    @FXML
    void openSingInPage(ActionEvent event) {
        try {			
        Stage stage;
        Parent root;

        if(event.getSource() == btnSeConnecter1) {
                stage = new Stage();
                root = FXMLLoader.load(getClass().getResource("/app/views/SingInPage.fxml"));
                stage.setScene(new Scene(root));
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initOwner(btnSeConnecter1.getScene().getWindow());
                stage.initStyle(StageStyle.UTILITY);
                stage.showAndWait();

        } else {
                stage = (Stage) btnClose.getScene().getWindow();
                stage.close();
                checkUsers();
        }
        //Stage stage1 = new Stage();

        //Parent root1 = FXMLLoader.load(getClass().getResource("/app/views/App.fxml"));
        //Scene scene1 = new Scene(root1);
        //stage1.setScene(scene1);

        //stage.initOwner(stage1);
        //			stage.initModality(Modality.APPLICATION_MODAL);
        //			stage.initOwner(btnSolde.getScene().getWindow());
        //			stage.initStyle(StageStyle.UTILITY);
        //			stage.setTitle("Modifier solde initial");
        //			Parent root = FXMLLoader.load(getClass().getResource("/app/views/solde.fxml"));
        //			Scene scene = new Scene(root, 390, 190);
        //			//scene.getStylesheets().add(getClass().getResource("/app/stylesheet/application.css").toExternalForm());
        //			//primaryStage.initStyle(StageStyle.UNDECORATED);
        //			stage.setScene(scene);
        //			stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
        checkUsers();
    }

    void checkUsers(){
            PreparedStatement preparedStmt = null;
            ResultSet rs = null;
            
        	try {
        	    Connection con = ConnectionUtil.conDB();
                    //con.setAutoCommit(false);
                    int nbUser = 0;
                    String query = "SELECT * FROM user";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
                            nbUser++;
                            usersData.add(rs.getString("login")+rs.getString("password"));
                            usersIDData.add(rs.getInt("user_id"));
			}
			//con.close();
			//preparedStmt.close();
			//rs.close();
                        if(nbUser > 1){
                            btnSeConnecter1.setVisible(false);
                        }

    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}        
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        checkUsers();
    }
}