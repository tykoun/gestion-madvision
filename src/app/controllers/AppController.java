package app.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jfoenix.controls.JFXButton;

import dashboard.FXMLDocumentController;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class AppController implements Initializable {
    @FXML
    private JFXButton btnDashboard;
    
    @FXML
    private AnchorPane holderPane;

    @FXML
    private JFXButton btnProduit;

    @FXML
    private JFXButton btnBonEntree;

    @FXML
    private JFXButton btnBonSortie;

    @FXML
    private JFXButton btnEtatStock;

    AnchorPane eleve,versement,certificat,mvtStock, dashBoard, login, formation, depense, livre, params;

    @FXML
    private JFXButton btnMvtStock;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
        try {
        	FXMLLoader formationLoader = new FXMLLoader(getClass().getResource("/app/views/Formation.fxml"));
        	FXMLLoader eleveLoader = new FXMLLoader(getClass().getResource("/app/views/Eleves.fxml"));
        	FXMLLoader versementLoader = new FXMLLoader(getClass().getResource("/app/views/Versements.fxml"));
        	FXMLLoader certificatLoader = new FXMLLoader(getClass().getResource("/app/views/Certificat.fxml"));
        	FXMLLoader mvtStockLoader = new FXMLLoader(getClass().getResource("/app/views/GestionCompte.fxml"));
        	FXMLLoader loginLoader = new FXMLLoader(getClass().getResource("/app/views/Login.fxml"));
        	FXMLLoader dashboardLoader = new FXMLLoader(getClass().getResource("/app/views/Dashboard.fxml"));
        	FXMLLoader depenseLoader = new FXMLLoader(getClass().getResource("/app/views/Depense.fxml"));
        	FXMLLoader livreLoader = new FXMLLoader(getClass().getResource("/app/views/Livre.fxml"));
        	FXMLLoader categorieLoader = new FXMLLoader(getClass().getResource("/app/views/Categorie.fxml"));
        	FXMLLoader arretLoader = new FXMLLoader(getClass().getResource("/app/views/Arret.fxml"));
        	FXMLLoader filtreDepLoader = new FXMLLoader(getClass().getResource("/app/views/FiltreDepense.fxml"));
        	FXMLLoader paramsLoader = new FXMLLoader(getClass().getResource("/app/views/Params.fxml"));
                
        	formation = formationLoader.load();
        	eleve = eleveLoader.load();
        	versement = versementLoader.load();
        	certificat = certificatLoader.load();
        	mvtStock = mvtStockLoader.load();
        	login = loginLoader.load();
                dashBoard = dashboardLoader.load();
                depense = depenseLoader.load();
                livre = livreLoader.load();
                params = paramsLoader.load();
                categorieLoader.load();
                arretLoader.load();
                filtreDepLoader.load();
                
                FormationController frmController = formationLoader.getController();
                ElevesController elvController = eleveLoader.getController();
                VersementController vrsController = versementLoader.getController();
                CertificatController crtController = certificatLoader.getController();
                GestionCompteController gcController = mvtStockLoader.getController();
                DashboardController dashController = dashboardLoader.getController();
                DepenseController depenseController = depenseLoader.getController();
                LivreController livreController = livreLoader.getController();
                CategorieController categorieController = categorieLoader.getController();
                ArretController arretController = arretLoader.getController();
                FiltreDepenseController filtreDepenseController = filtreDepLoader.getController();
                
                frmController.init(elvController, vrsController, crtController, gcController, dashController);
                elvController.init(vrsController, crtController, dashController);
                vrsController.init(elvController, crtController, gcController, dashController);
                crtController.init(elvController, vrsController, dashController);
                gcController.init(vrsController, depenseController, livreController, elvController, dashController);
                //dashController.init(vrsController, depenseController, livreController, elvController, dashController);
                depenseController.init(gcController, dashController);
                categorieController.init(depenseController);
                filtreDepenseController.init(depenseController);
                arretController.init(vrsController, depenseController, livreController, elvController, dashController, gcController);
                livreController.init(gcController, dashController);
                
                setNode(dashBoard);
                //setNode(depense);
       } catch (IOException ex) {
           Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
       }
    }

    @FXML
    private void switchFormation(ActionEvent event) {
        setNode(formation);
    }

    @FXML
    void switchDashboard(ActionEvent event) {
        setNode(dashBoard);
    }
    
    @FXML
    private void switchEleve(ActionEvent event) {
        setNode(eleve);
    }

    @FXML
    private void switchVersement(ActionEvent event) {
        setNode(versement);
    }

    @FXML
    private void switchCertificat(ActionEvent event) {
        setNode(certificat);
    }
    @FXML
    private void switchDepense(ActionEvent event) {
        setNode(depense);
    }
    @FXML
    private void switchCompte(ActionEvent event) {
        setNode(mvtStock);
    }
    @FXML
    private void switchLivre(ActionEvent event) {
        setNode(livre);
    }
    
    @FXML
    private void switchSwithParams(ActionEvent event) {
        setNode(params);
    }
                
	private void setNode(Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add((Node) node);
        AnchorPane.setLeftAnchor(node, 0.0);
        AnchorPane.setRightAnchor(node, 0.00);
        AnchorPane.setBottomAnchor(node, 0.00);
        AnchorPane.setTopAnchor(node, 0.00);
        FadeTransition ft = new FadeTransition(Duration.millis(1500));
        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();
	}
}
