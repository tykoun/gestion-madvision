package app.controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;

import app.model.FormationModule;
import app.model.Livre;
import app.model.Transaction;
import app.model.Versement;
import app.utils.ConnectionUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

public class GestionCompteController implements Initializable{

    @FXML
    private TableColumn<Versement, String> compteColumn;

    @FXML
    private Button btnCancelArret;

    @SuppressWarnings("rawtypes")
	@FXML
    private TableColumn selectColumn;

    @FXML
    private Button btnArretCompte;

    @FXML
    private TableView<Versement> tableArret;
    
    @FXML
    private TableView<FormationModule> tableFormtaion;
    
    @FXML
    private TableColumn<FormationModule, String> formationColumn;
    
    @FXML
    private TableColumn<FormationModule, Integer> totalColumn;
    
    @FXML
    private Label lblErreurDebitDesign;

    @FXML
    private Label lblErreurDebit;
	
    @FXML
    private Label lblErreurCredit;

    @FXML
    private Label lblErreurDesign;

    @FXML
    private Button btnCancelDebit;

    @FXML
    private TextField txtMontantDebit;

    @FXML
    private Button btnSaveDebit;

    @FXML
    private TextArea txtDesignDebit;
    
    @FXML
    private TextField txtMontantCredit;

    @FXML
    private Button btnSaveCredit;

    @FXML
    private Button btnAnnulerCredit;

    @FXML
    private TextArea txtDesignCredit;
    
    @FXML
    private Label lblErreur;  
    
    @FXML
    private Label dateSoldeInitial;
    
	@FXML
    private JFXButton btnCancel;

    @FXML
    private JFXButton btnSave;

    @FXML
    private JFXTextField txtMontant;
    
    @FXML
    private JFXButton btnArret;

    @FXML
    private JFXTextField txtCredit;

    @FXML
    private JFXButton btnSolde;

    @FXML
    private JFXButton btnCredit;

    @FXML
    private JFXButton btnDebit;

    @FXML
    private JFXTextField txtSearch;

    @FXML
    private JFXTextField txtSolde;
    
    @FXML
    private JFXTextField txtSoldeTotal;

    @FXML
    private JFXTextField txtDebit;

    
//    @FXML
//    private TableColumn<Transaction, String> designation_column;
//
//    @FXML
//    private TableColumn<Transaction, Integer> credit_column;
//
//    @FXML
//    private TableColumn<Transaction, Integer> debit_column;
//
//    @FXML
//    private TableColumn<Transaction, String> date_column;
//
//    @FXML
//    private TableColumn<Transaction, Integer> solde_column;
//
//    @FXML
//    private TableView<Transaction> tableTransaction;
//    
    @FXML
    private TableColumn<Transaction, String> designation_column1;

    @FXML
    private TableColumn<Transaction, Integer> credit_column1;

    @FXML
    private TableColumn<Transaction, Integer> debit_column1;

    @FXML
    private TableColumn<Transaction, String> date_column1;

    @FXML
    private TableColumn<Transaction, Integer> solde_column1;

    @FXML
    private TableView<Transaction> tableTransaction1;
    
    @FXML
    private Label txtNombreVendu;

    @FXML
    private JFXTextField txtMontantLivre;
    private final ObservableList<Transaction> transData = FXCollections.observableArrayList();
    private final ObservableList<Transaction> transData2 = FXCollections.observableArrayList();    
    private final ObservableList<Livre> livreData = FXCollections.observableArrayList();    
    private final ObservableList<FormationModule> formationData = FXCollections.observableArrayList();    
    private final ObservableList<Versement> versData = FXCollections.observableArrayList();
    private final ObservableList<String> arretData = FXCollections.observableArrayList();
    private int totalCredit = 0; 
    private int totalMontantLivre = 0;

    ElevesController ec;
    VersementController vc;
    LivreController lc;
    DepenseController depc;
    DashboardController dashc;
    
    void init(VersementController vrsController, DepenseController depenseController, LivreController livreController, ElevesController elvController, DashboardController dashController) {
        ec = elvController;
        vc = vrsController;
        lc = livreController;
        depc = depenseController;
        dashc = dashController;
    }    
    
    public void loadView(){
        loadLivre();
        fillForm();
        fillData();
        //fillTableArret();
    }    
    @FXML
    private void fillData() {
    	int solde = 0;
    	int credit = 0;
    	int debit = 0;
    	int i = 0;
    	String date = LocalDate.now().toString();
    	transData.clear();
    	transData2.clear();
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * FROM (`transaction` JOIN trans_type ON \"transaction\".trans_type_id=trans_type.trans_type_id) NATURAL JOIN solde where solde.solde_id = (SELECT MAX(solde_id) FROM solde) AND \"transaction\".arret=0;";
    	    String query2 = "SELECT * FROM `solde` WHERE solde_id=(SELECT MAX(solde_id) FROM solde)";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
//				if(rs.getInt("trans_type_id") == 1) { //credit
//					transData.add(new Transaction(rs.getInt("trans_id"), rs.getString("trans_design"), rs.getString("trans_date"), rs.getInt("trans_montant"), rs.getBoolean("arret"), rs.getInt("trans_type_id")));
//					credit = credit + rs.getInt("trans_montant");
//				}
				if(rs.getInt("trans_type_id") == 2) {
					transData2.add(new Transaction(rs.getInt("trans_id"), rs.getString("trans_design"), rs.getString("trans_date"), rs.getInt("trans_montant"), rs.getBoolean("arret"), rs.getInt("trans_type_id")));
					debit = debit + rs.getInt("trans_montant");
				}
				
//				if(i == 0) {
//					solde = rs.getInt("solde_init");
//					date = rs.getString("solde_date");
//					i++;
//				}
			}
                        //solde
			preparedStmt = con.prepareStatement(query2);
			rs = preparedStmt.executeQuery();
                        while(rs.next()){
                            solde = rs.getInt("solde_init");
                            date = rs.getString("solde_date");
                        }
                        preparedStmt.close();
			rs.close();
			con.close();
		} catch (Exception except) {
			// TODO Auto-generated catch block
			except.printStackTrace();
		}
    	
    	if(tableTransaction1 == null) {
    		System.out.println("Null");
    		//tableTransaction.getScene().getWindow();
    		//tableTransaction.setItems(transData);
    	} else {
//        	date_column.setCellValueFactory(new PropertyValueFactory<Transaction, String>("trans_date_string"));
//        	designation_column.setCellValueFactory(new PropertyValueFactory<Transaction, String>("trans_design"));
//        	credit_column.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("trans_montant"));
//        	debit_column.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("trans_montant"));
//        	solde_column.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("trans_montant"));
//        	tableTransaction.setItems(transData);
//        	
        	date_column1.setCellValueFactory(new PropertyValueFactory<Transaction, String>("trans_date_string"));
        	designation_column1.setCellValueFactory(new PropertyValueFactory<Transaction, String>("trans_design"));
        	credit_column1.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("trans_montant"));
        	debit_column1.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("trans_montant"));
        	solde_column1.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("trans_montant"));
        	tableTransaction1.setItems(transData2);
        	
                credit = credit + totalCredit + totalMontantLivre;
                
        	LocalDate dateR = LocalDate.parse(date);
        	DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        	txtCredit.setText(Integer.toString(credit));
        	txtDebit.setText(Integer.toString(debit));
        	txtSolde.setText(Integer.toString(solde));
        	dateSoldeInitial.setText(dateR.format(dateformatter));
        	txtSoldeTotal.setText(Integer.toString(solde + credit - debit));
    	}
    }
    
    @FXML
    void loadLivre(){
        livreData.clear();
        totalMontantLivre = 0;
    	try {
            PreparedStatement preparedStmt = null;
            ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * FROM livre WHERE arret=0";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				livreData.add(new Livre(rs.getInt("livre_id"), LocalDate.parse(rs.getString("livre_date")), rs.getInt("livre_qte"), rs.getInt("livre_stock"),  rs.getBoolean("vendu"), rs.getBoolean("arret"), rs.getInt("prix")));
			}
			con.close();
			preparedStmt.close();
			rs.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        int nombreLivreVendu = 0;
        int nombreLivreEnStock = 0;
        int totalLivre = 0;
        for (Livre livre : livreData) {
            nombreLivreEnStock = livre.getLivre_stock();
            totalLivre = livre.getLivre_qte();
            
            if(totalLivre >=  nombreLivreEnStock){
                nombreLivreVendu += totalLivre - nombreLivreEnStock;
                totalMontantLivre += (totalLivre - nombreLivreEnStock)*livre.getPu();
            } else {
                //totalMontantLivre += (totalLivre - nombreLivreEnStock)*livre.getPu();
            }
        }

        if(txtNombreVendu != null || txtMontantLivre != null){
            txtNombreVendu.setText(Integer.toString(nombreLivreVendu));
            txtMontantLivre.setText(""+totalMontantLivre);
        }
    }
    
    @FXML
    private void fillForm() {
    	formationData.clear();
        totalCredit = 0;
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT vers_id, vers_motif, SUM(vers_montant) as 'vers_montant' FROM `versement` WHERE arret = false GROUP BY vers_motif";
    	    
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				formationData.add(new FormationModule(rs.getInt("vers_id"), rs.getString("vers_motif"), rs.getInt("vers_montant"), rs.getString("vers_motif"), rs.getInt("vers_id"), rs.getString("vers_motif"), rs.getString("vers_motif")));
					//credit = credit + rs.getInt("trans_montant");
			}
			preparedStmt.close();
			rs.close();
			con.close();
		} catch (Exception except) {
			// TODO Auto-generated catch block
			except.printStackTrace();
		}
    	
    	if(tableFormtaion == null || tableFormtaion == null) {
    		System.out.println("Null");
    		//tableTransaction.getScene().getWindow();
    		//tableTransaction.setItems(transData);
    	} else {
    	   	
    	formationColumn.setCellValueFactory(new PropertyValueFactory<FormationModule, String>("form_nom"));
    	totalColumn.setCellValueFactory(new PropertyValueFactory<FormationModule, Integer>("module_id"));
        tableFormtaion.setItems(formationData);
        }
        
        for (FormationModule transaction : formationData) {
            totalCredit += transaction.getModule_id();
        }
    }
    
    @FXML
    private void openSoldeDial(ActionEvent event) {
		try {			
			Stage stage;
			Parent root;
			
			if(event.getSource() == btnSolde) {
				stage = new Stage();
				root = FXMLLoader.load(getClass().getResource("/app/views/solde.fxml"));
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initOwner(btnSolde.getScene().getWindow());
				stage.initStyle(StageStyle.UNDECORATED);
				stage.showAndWait();

                        } else {
				stage = (Stage) btnCancel.getScene().getWindow();
				stage.close();
			}
			//Stage stage1 = new Stage();
			
			//Parent root1 = FXMLLoader.load(getClass().getResource("/app/views/App.fxml"));
			//Scene scene1 = new Scene(root1);
			//stage1.setScene(scene1);

			//stage.initOwner(stage1);
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.initOwner(btnSolde.getScene().getWindow());
//			stage.initStyle(StageStyle.UTILITY);
//			stage.setTitle("Modifier solde initial");
//			Parent root = FXMLLoader.load(getClass().getResource("/app/views/solde.fxml"));
//			Scene scene = new Scene(root, 390, 190);
//			//scene.getStylesheets().add(getClass().getResource("/app/stylesheet/application.css").toExternalForm());
//			//primaryStage.initStyle(StageStyle.UNDECORATED);
//			stage.setScene(scene);
//			stage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
		fillData();
    }
    
    @FXML
    private void openCreditDial(ActionEvent event) {
		try {			
			Stage stage;
			Parent root;
			
			if(event.getSource() == btnCredit) {
				stage = new Stage();
				root = FXMLLoader.load(getClass().getResource("/app/views/CreditDial.fxml"));
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initOwner(btnCredit.getScene().getWindow());
				stage.initStyle(StageStyle.UTILITY);
				stage.showAndWait();
			} else{
				stage = (Stage) btnAnnulerCredit.getScene().getWindow();
				stage.close();
			}
		} catch(Exception e) {
				e.printStackTrace();
		}
		fillData();
    }
    
    @FXML
    private void openDebitDial(ActionEvent event) {
		try {			
			Stage stage;
			Parent root;
			
			if(event.getSource() == btnDebit) {
				stage = new Stage();
				root = FXMLLoader.load(getClass().getResource("/app/views/DebitDial.fxml"));
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initOwner(btnDebit.getScene().getWindow());
				stage.initStyle(StageStyle.UTILITY);
				stage.showAndWait();
			} else{
				stage = (Stage) btnCancelDebit.getScene().getWindow();
				stage.close();
			}
		} catch(Exception e) {
				e.printStackTrace();
		}
		fillData();
    }
    
    @FXML
    private void openArretDial(ActionEvent event) {
            try {
                    Stage stage;
                    Parent root;

                    if(event.getSource() == btnArret) {
                            stage = new Stage();
                            root = FXMLLoader.load(getClass().getResource("/app/views/Arret.fxml"));
                            stage.setScene(new Scene(root));
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.initOwner(btnArret.getScene().getWindow());
                            stage.initStyle(StageStyle.UNDECORATED);
                            stage.showAndWait();
                    } 
            } catch(Exception e) {
                            e.printStackTrace();
            }
                loadView();
                ec.loadView();
                vc.loadView();
                dashc.loadView();
                depc.loadView();
                lc.loadData();
    }
    
    @SuppressWarnings("unchecked")
	@FXML
    private void fillTableArret() {
    	versData.clear();
    	arretData.clear();
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT vers_id, vers_motif FROM `versement` GROUP BY vers_motif";
    	    
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			versData.add(new Versement(0, "Recettes/Dépenses"));
			versData.add(new Versement(0, "Livres"));
			while(rs.next()) {
				versData.add(new Versement(rs.getInt("vers_id"), rs.getString("vers_motif")));
					//credit = credit + rs.getInt("trans_montant");
			}
			preparedStmt.close();
			rs.close();
			con.close();
		} catch (Exception except) {
			// TODO Auto-generated catch block
			except.printStackTrace();
		}
    	
    	if(tableArret == null || tableArret == null) {
    		System.out.println("Null");
    		//tableTransaction.getScene().getWindow();
    		//tableTransaction.setItems(transData);
    	} else {
    	compteColumn.setCellValueFactory(new PropertyValueFactory<Versement, String>("vers_motif"));
    	
    	
    	Callback<TableColumn<Versement, String>, TableCell<Versement, String>> CellFactory = (param) -> {
    		
    		final TableCell<Versement, String> cell= new TableCell<Versement, String>(){
    			@Override
    			public void updateItem(String item, boolean empty) {
    				super.updateItem(item, empty);
    				
    				if(empty) {
    					setGraphic(null);
    					setText(null);
    				} else {
    					final CheckBox checkbx = new CheckBox();
    					checkbx.setOnAction(event -> {
    						Versement v = getTableView().getItems().get(getIndex());
    						if(checkbx.isSelected()) arretData.add(v.getVers_motif());
    						else arretData.remove(v.getVers_motif());
//    						Alert alert = new Alert(Alert.AlertType.INFORMATION);
//    						alert.setContentText("vous avez checké sur " + v.getVers_motif());
//    						alert.show();
    						//System.out.println(arretData);
    					});
    					
    					setGraphic(checkbx);
    					setText(null);
    				}
    			}
    		};
    		
    		
    		
    		return cell;
    	};
    	
    	selectColumn.setCellFactory(CellFactory);
        tableArret.setItems(versData);
        }
    }
    
    @FXML
    private void arreter() {
    	  	
    	if(arretData.isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setContentText("Séléctionner au moins un compte à arrêter");
			alert.show();
    	} else {
    		try {
                    PreparedStatement preparedStmt = null;
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "UPDATE `transaction` SET arret=TRUE";
        	    String query2 = "UPDATE `versement` SET `arret`=TRUE WHERE vers_motif=?";
        	    String query3 = "UPDATE `livre` SET `arret`=TRUE WHERE vendu = TRUE";
        	    String query4 = "UPDATE livre SET livre_qte=livre_stock WHERE arret=FAlSE";
        	    //con.setAutoCommit(false);
        		for (String data : arretData) {
    				if(data == "Recettes/Dépenses") {
    	    			preparedStmt = con.prepareStatement(query);
    	    			preparedStmt.execute();
    				} 
                                else if(data == "Livres"){
                                System.out.println("Liiiiiivre");
    	    			preparedStmt = con.prepareStatement(query3);
    	    			preparedStmt.execute();
    	    			preparedStmt = con.prepareStatement(query4);
    	    			preparedStmt.execute();
                                }
                                else {
    	    			preparedStmt = con.prepareStatement(query2);
    	    			preparedStmt.setString(1, data);
    	    			preparedStmt.execute();
    				}
    			}
        		        		
        		preparedStmt.close(); con.close();
			} catch (Exception e2) {
				// TODO: handle exception
			}
                                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                                alert.setContentText("Compte arreté avec succès");
                                alert.show();

        }
    }
    
    @FXML
    private void saveSolde(ActionEvent e) {
    	if(txtMontant.getText().equals("")) {
    		lblErreur.setVisible(true);
    	} else {
    		Alert alert = new Alert(AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Ajouter un solde initial ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
        	try {
        		PreparedStatement preparedStmt = null;
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "INSERT INTO solde (solde_init, solde_date) values(?, CURRENT_DATE)";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setInt(1, Integer.parseInt(txtMontant.getText()));
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Added succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
    			//stage.close();
    	    	openSoldeDial(e);	    			
    		} catch (Exception except) {
    			// TODO Auto-generated catch block
    			except.printStackTrace();
    		}
        	}
    	}
		fillData();
                loadView();
    }
    
    @FXML
    private void saveDebit(ActionEvent e) {
    	if(txtMontantDebit.getText().equals("")) {
    		lblErreurDebit.setVisible(true);
    		txtMontantDebit.setStyle("-fx-border-color: red");
    	} 
    	if(txtDesignDebit.getText().equals("")){
    		lblErreurDebitDesign.setVisible(true);
    		txtDesignDebit.setStyle("-fx-border-color: red");
    	}else {
    		Alert alert = new Alert(AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Ajouter un Débit ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "INSERT INTO `transaction`(`trans_design`, `trans_date`, `trans_montant`, `trans_type_id`) VALUES (?,CURRENT_DATE,?,2)";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, txtDesignDebit.getText());
    			preparedStmt.setInt(2, Integer.parseInt(txtMontantDebit.getText()));
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Added succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
    			//stage.close();
    			openDebitDial(e);
    		} catch (Exception except) {
    			// TODO Auto-generated catch block
    			except.printStackTrace();
    		}
            }
    	}
    }
    
    @FXML
    private void saveCredit(ActionEvent e) {
    	if(txtMontantCredit.getText().equals("")) {
    		lblErreurCredit.setVisible(true);
    		txtMontantCredit.setStyle("-fx-border-color: red");
    	} 
    	if(txtDesignCredit.getText().equals("")){
    		lblErreurDesign.setVisible(true);
    		txtDesignCredit.setStyle("-fx-border-color: red");
    	}else {
    		Alert alert = new Alert(AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Ajouter un Crédit ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "INSERT INTO `transaction`(`trans_design`, `trans_date`, `trans_montant`, `trans_type_id`) VALUES (?,CURRENT_DATE,?,1)";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, txtDesignCredit.getText());
    			preparedStmt.setInt(2, Integer.parseInt(txtMontantCredit.getText()));
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Added succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
    			//stage.close();
    			openCreditDial(e);
    		} catch (Exception except) {
    			// TODO Auto-generated catch block
    			except.printStackTrace();
    		}
        	}
    	}
    }
    
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
                loadLivre();
		fillForm();
                fillData();
//		fillTableArret();
	}
}
