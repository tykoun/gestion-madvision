/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.model.Categorie;
import app.model.Transaction;
import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;

/**
 *
 * @author Finaritra
 */
public class DepenseController implements Initializable{
    
    @FXML
    private JFXButton btnPreference;

    @FXML
    private TableView<Transaction> tableTransaction1;

    @FXML
    private TableColumn<Transaction, String> date_column1;

    @FXML
    private TableColumn<Transaction, String> designation_column1;

    @FXML
    private TableColumn<Transaction, Integer> credit_column1;
    
    @FXML
    private TableColumn<Transaction, String> state;
    
    @FXML
    private JFXDatePicker dpDateEntree;

    @FXML
    private JFXTextField txtNumProduit;

    @FXML
    private JFXComboBox<Categorie> cbx_design;

    @FXML
    private JFXButton btnFiltrer;

    @FXML
    private JFXButton btnAjouter;

    @FXML
    private JFXButton btnSupprimer;

    @FXML
    private JFXButton btnModifier;

    @FXML
    private JFXTextField txtSearch;

    @FXML
    private JFXButton btnSearch;
    
    /**** CATEGORIE *******/
    
    @FXML
    private TextField txtDepense;

    @FXML
    private TableView<Categorie> table;

    @FXML
    private TableColumn<Categorie, String> columnCategorie;

    @FXML
    private Button btnCancel;

    private final ObservableList<Categorie> categorieData = FXCollections.observableArrayList();
    private final ObservableList<Transaction> transData2 = FXCollections.observableArrayList();
    
            GestionCompteController gc;
            DashboardController dc;
            
            static String filterString = "month"; //month / year / dates
            static String date1 = LocalDate.now().toString();
            static String date2 = LocalDate.now().toString();
    
    public static void setFilter(String filter){
        filterString = filter;
    }     
    
    public static void setDate1(String date){
        date1 = date;
    }
    public static void setDate2(String date){
        date2 = date;
    }

    @FXML
    public void search() throws SQLException, Exception{
         
         FilteredList<Transaction> filterData = new FilteredList<>(transData2,p->true);
         txtSearch.textProperty().addListener((observable,oldvalue,newvalue)->{
         
            filterData.setPredicate( produit->{
                
                if(newvalue == null || newvalue.isEmpty()){
                	
                    return true;
                }
                String typedText = newvalue.toLowerCase();
                
                if(produit.getTrans_design().toLowerCase().contains(typedText)){
                     return true;
                }
//                if(produit.getForm_nom().toLowerCase().contains(typedText)){
//                     return true;
//                }
                if(produit.getTrans_date_string().toLowerCase().contains(typedText)){
                     return true;
                }               
                if( new String().valueOf(produit.getTrans_montant()).toLowerCase().contains(typedText)){
                     return true;
                }
//                if(produit.getVers_date().toLowerCase().contains(typedText)){
//                     return true;
//                }   
//                if( new String().valueOf(produit.getNum_recu()).toLowerCase().contains(typedText)){
//                     return true;
//                }
                return false;
            });
            
            SortedList<Transaction> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(tableTransaction1.comparatorProperty());
            tableTransaction1.setItems(sortedList);
            
        });
     }
            
    void init(GestionCompteController gcController, DashboardController dashController) {
        gc = gcController;
        dc = dashController;
    }
    
    public void loadView() {
        fillData();
        fillCategorie();
        //gc.loadView();
    }

    @FXML
    public void open_cat_dial(ActionEvent event) {
        try {			
			Stage stage;
			Parent root;
                        
			if(event.getSource() == btnPreference) {
				stage = new Stage();
				root = FXMLLoader.load(getClass().getResource("/app/views/Categorie.fxml"));                                
				stage.setScene(new Scene(root));
//                                System.out.println("////////////////////////////////////////");
//                                System.out.println(root.getChildrenUnmodifiable().get(2).setVisible(false));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initOwner(btnPreference.getScene().getWindow());
				stage.initStyle(StageStyle.UTILITY);
                                
				stage.showAndWait();
			}
//                        else {
//				stage = (Stage) btnCancel.getScene().getWindow();
//				stage.close();
//			}
		} catch(Exception e) {
			e.printStackTrace();
		}
                loadView();
    }
    
    @FXML
    public void open_filter_dial(ActionEvent event) {
        setDate1(LocalDate.now().toString());
        setDate2(LocalDate.now().toString());
        try {			
			Stage stage;
			Parent root;
                        
			if(event.getSource() == btnFiltrer) {
				stage = new Stage();
				root = FXMLLoader.load(getClass().getResource("/app/views/FiltreDepense.fxml"));                                
				stage.setScene(new Scene(root));
//                                System.out.println("////////////////////////////////////////");
//                                System.out.println(root.getChildrenUnmodifiable().get(2).setVisible(false));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initOwner(btnPreference.getScene().getWindow());
				stage.initStyle(StageStyle.UNDECORATED);
                                
				stage.showAndWait();
			}
//                        else {
//				stage = (Stage) btnCancel.getScene().getWindow();
//				stage.close();
//			}
		} catch(Exception e) {
			e.printStackTrace();
		}
                //loadView();
                if(filterString != "none")
                filterData();
                //System.out.println(filterString);
    }
    
    
    @FXML
    public void checkForm(){
        dpDateEntree.setDisable(false);

    }

    @FXML
    void setFormDepense() {
        Transaction transaction = tableTransaction1.getSelectionModel().getSelectedItem();
        
        /**
         * get Transaction index
        */
        int count = 0;
    	for(Categorie x : categorieData) {
    		if(x.getCat_designation().compareToIgnoreCase(transaction.getTrans_design()) == 0) {
    			break;
    		}
    		count++;
    	}
        
        dpDateEntree.setValue(transaction.getTrans_date());
        //dpDateEntree.setDisable(true);
        cbx_design.getSelectionModel().select(count);
        txtNumProduit.setText(Integer.toString(transaction.getTrans_montant()));
    }
    
    //Catégories funtions
    @FXML
    void ajouter(ActionEvent event) {
        if(txtNumProduit.getText().length() == 0 || cbx_design.getSelectionModel().isSelected(-1) || !dpDateEntree.validate()) {
    		System.out.println("Hahahahahhahaha");
    	} else {
    		
    		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Ajouter ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "INSERT INTO \"transaction\" (trans_design, trans_date, trans_montant, trans_type_id) values(?, ?, ?, ?)";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, cbx_design.getSelectionModel().getSelectedItem().getCat_designation());
    			preparedStmt.setString(2, dpDateEntree.getValue().toString());
    			preparedStmt.setInt(3, Integer.parseInt(txtNumProduit.getText()));
    			preparedStmt.setInt(4, 2);
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Add succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
                        transData2.clear();
    			//fillCategorie();
                        txtNumProduit.clear();
                        cbx_design.getSelectionModel().select(-1);
                        dpDateEntree.setValue(null);

    			fillData();
                        
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
                loadView();
        	gc.loadView();
            }
        }
    }

    @FXML
    void modifier(ActionEvent event) {
        if(txtNumProduit.getText().length() == 0 || cbx_design.getSelectionModel().isSelected(-1) || !dpDateEntree.validate()) {
    		System.out.println("Hahahahahhahaha");
    	} else {
    		
    		Alert alert = new Alert(Alert.AlertType.WARNING);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Modifier ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "UPDATE transaction SET trans_design=?, trans_date=?, trans_montant=? WHERE trans_id=?";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setString(1, cbx_design.getSelectionModel().getSelectedItem().getCat_designation());
    			preparedStmt.setString(2, dpDateEntree.getValue().toString());
    			preparedStmt.setInt(3, Integer.parseInt(txtNumProduit.getText()));
    			preparedStmt.setInt(4, tableTransaction1.getSelectionModel().getSelectedItem().getTrans_id());
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Edited succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
                        transData2.clear();
    			//fillCategorie();
                        txtNumProduit.clear();
                        cbx_design.getSelectionModel().select(-1);
                        dpDateEntree.setValue(null);

    			fillData();
                        
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	gc.loadView();
                loadView();
            }
        }
    }

    @FXML
    void setCategorieTxt(MouseEvent event) {
        
    }

    @FXML
    void supprimer(ActionEvent event) {
        if(txtNumProduit.getText().length() == 0 || cbx_design.getSelectionModel().isSelected(-1) || !dpDateEntree.validate()) {
    		System.out.println("Hahahahahhahaha");
    	} else {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Voulez-vous vraiment supprimer ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
    		PreparedStatement preparedStmt = null;
        	try {
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "DELETE FROM transaction WHERE trans_id=?";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setInt(1, tableTransaction1.getSelectionModel().getSelectedItem().getTrans_id());
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Deleted succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
                        transData2.clear();
    			//fillCategorie();
                        txtNumProduit.clear();
                        cbx_design.getSelectionModel().select(-1);
                        dpDateEntree.setValue(null);

    			fillData();
                        
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	gc.loadView();
                loadView();
            }
    }
    }
    
    //FUNCTIONS ...
    @FXML
    void fillCategorie(){
        categorieData.clear();
        StringConverter<Categorie> converter = new StringConverter<Categorie>() {

            @Override
            public Categorie fromString(String arg0) {
                    // TODO Auto-generated method stub
                    return null;
            }

            @Override
            public String toString(Categorie obj) {
                return obj.getCat_designation();
            }
			
        };
    	
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * from categorie";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				categorieData.add(new Categorie(rs.getInt("cat_id"), rs.getString("cat_designation"), rs.getInt("trans_type_id")));
			}
			con.close();
			preparedStmt.close();
			rs.close();
                        
                        cbx_design.setConverter(converter);
                        cbx_design.setItems(categorieData);
                        
                        
//                        columnCategorie.setCellValueFactory(new PropertyValueFactory<Categorie, String>("cat_designation"));
//                        table.setItems(categorieData);
                        

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    @FXML
    private void fillData() {
    	int solde = 0;
    	int debit = 0;
    	int i = 0;
    	String date = LocalDate.now().toString();
    	transData2.clear();
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * FROM (`transaction` JOIN trans_type ON 'transaction'.trans_type_id='trans_type'.trans_type_id) NATURAL JOIN solde where 'solde'.solde_id = (SELECT MAX(solde_id) FROM solde)  AND 'transaction'.arret=FALSE;";
    	    
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				if(rs.getInt("trans_type_id") == 2) {//debit
					transData2.add(new Transaction(rs.getInt("trans_id"), rs.getString("trans_design"), rs.getString("trans_date"), rs.getInt("trans_montant"), rs.getBoolean("arret"), rs.getInt("trans_type_id")));
					debit = debit + rs.getInt("trans_montant");
				}
				
				if(i == 0) {
					solde = rs.getInt("solde_init");
					date = rs.getString("solde_date");
					i++;
				}
			}
			preparedStmt.close();
			rs.close();
			con.close();
		} catch (Exception except) {
			// TODO Auto-generated catch block
			except.printStackTrace();
		}
    	
    	if(tableTransaction1 == null) {
    		System.out.println("Null");
    		//tableTransaction.getScene().getWindow();
    		//tableTransaction.setItems(transData);
    	} else {

        	date_column1.setCellValueFactory(new PropertyValueFactory<Transaction, String>("trans_date_string"));
        	designation_column1.setCellValueFactory(new PropertyValueFactory<Transaction, String>("trans_design"));
        	credit_column1.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("trans_montant"));
        	state.setCellValueFactory(new PropertyValueFactory<Transaction, String>("arret_string"));
                dpDateEntree.setValue(LocalDate.now());
        	tableTransaction1.setItems(transData2);
        	
//        	LocalDate dateR = LocalDate.parse(date);
//        	DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
//        	txtCredit.setText(Integer.toString(credit));
//        	txtDebit.setText(Integer.toString(debit));
//        	txtSolde.setText(Integer.toString(solde));
//        	dateSoldeInitial.setText(dateR.format(dateformatter));
//        	txtSoldeTotal.setText(Integer.toString(solde + credit - debit));
    	}
    }

    @FXML
    public void filterData() {
    	int solde = 0;
    	int debit = 0;
    	int i = 0;
        String query = "";
        System.out.println(filterString);
        System.out.println(date1);
        System.out.println(date2);
    	String date = LocalDate.now().toString();
    	transData2.clear();
    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
            if(filterString == "month"){
                query = "SELECT * FROM (`transaction` JOIN trans_type ON 'transaction'.trans_type_id='trans_type'.trans_type_id) NATURAL JOIN solde where 'solde'.solde_id = (SELECT MAX(solde_id) FROM solde)  AND 'transaction'.trans_date >= date('now', 'start of month','-1 month');";
            } 
            else if(filterString == "year"){
                query = "SELECT * FROM (`transaction` JOIN trans_type ON 'transaction'.trans_type_id='trans_type'.trans_type_id) NATURAL JOIN solde where 'solde'.solde_id = (SELECT MAX(solde_id) FROM solde)  AND 'transaction'.trans_date >= date('now', 'start of year','-1 year');";
            } else if(filterString == "dates"){
                query ="SELECT * FROM (`transaction` JOIN trans_type ON 'transaction'.trans_type_id='trans_type'.trans_type_id) NATURAL JOIN solde where 'solde'.solde_id = (SELECT MAX(solde_id) FROM solde)  AND 'transaction'.trans_date BETWEEN '"+ date1 +"' and '"+date2 +"'";
            }
    	    
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				if(rs.getInt("trans_type_id") == 2) {//debit
					transData2.add(new Transaction(rs.getInt("trans_id"), rs.getString("trans_design"), rs.getString("trans_date"), rs.getInt("trans_montant"), rs.getBoolean("arret"), rs.getInt("trans_type_id")));
					debit = debit + rs.getInt("trans_montant");
				}
				
				if(i == 0) {
					solde = rs.getInt("solde_init");
					date = rs.getString("solde_date");
					i++;
				}
			}
			preparedStmt.close();
			rs.close();
			con.close();
		} catch (Exception except) {
			// TODO Auto-generated catch block
			except.printStackTrace();
		}
    	
    	if(tableTransaction1 == null) {
    		System.out.println("Null");
    		//tableTransaction.getScene().getWindow();
    		//tableTransaction.setItems(transData);
    	} else {
        	date_column1.setCellValueFactory(new PropertyValueFactory<Transaction, String>("trans_date_string"));
        	designation_column1.setCellValueFactory(new PropertyValueFactory<Transaction, String>("trans_design"));
        	credit_column1.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("trans_montant"));
        	state.setCellValueFactory(new PropertyValueFactory<Transaction, String>("arret_string"));
                dpDateEntree.setValue(LocalDate.now());
        	tableTransaction1.setItems(transData2);
    	}
    }
    
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //fillCategorieTable();
        fillData();
        fillCategorie();

        txtNumProduit.textProperty().addListener(new ChangeListener<String>() {

                @Override
                public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                        if(!txtNumProduit.getText().matches("\\d{1,15}")) {
                                //txtStock.setStyle("-fx-background-color: #FF514F");
                                txtNumProduit.setText("");
                        }
                        else {
                                txtNumProduit.setStyle("-fx-border-color: WHITE");
                        }
                }
        });
    }
}
