package app.controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import app.model.Formation;
import app.model.FormationEleve;
import app.model.Type;
import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import java.sql.SQLException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class ElevesController implements Initializable {
	
    @FXML
    private AnchorPane ancpane;
    
    @FXML
    private Label lblAge;

    @FXML
    private JFXToggleButton termine;

    @FXML
    private JFXComboBox<String> cbxSex;

    @FXML
    private JFXComboBox<Formation> cbxFormation;

    @FXML
    private TextField txtPrenom;

    @FXML
    private JFXDatePicker dpNaissance;

    @FXML
    private TextField txtTel;

    @FXML
    private TextField txtAdresse;

    @FXML
    private JFXTextField txtSearch;

    @FXML
    private TextField txtLieuNaissance;

    @FXML
    private JFXComboBox<Type> cbxType;

    @FXML
    private JFXButton btnAdd;

    @FXML
    private JFXDatePicker dpRentree;

    @FXML
    private TableView<FormationEleve> listeEleve;

    @FXML
    private TableColumn<FormationEleve, String> tcType;
    @FXML
    private TableColumn<FormationEleve, Integer> tcNum;
    @FXML
    private TableColumn<FormationEleve, String> tcNom;
    @FXML
    private TableColumn<FormationEleve, String> tcTermine;
    @FXML
    private TableColumn<FormationEleve, String> tcFormation;
    @FXML
    private TableColumn<FormationEleve, String> tcRentree;
    @FXML
    private TableColumn<FormationEleve, String> tcCertificat;
    @FXML
    private TextField txtNom;
    
    @FXML
    private JFXToggleButton tglAnnee;

    @FXML
    private TextField txtAnnee;

    @FXML
    private HBox hbxDateNaiss;
    
    
    private final ObservableList<String> sexe = FXCollections.observableArrayList("Homme", "Femme");
    private final ObservableList<Formation> FormData = FXCollections.observableArrayList();
    private final ObservableList<FormationEleve> eleveData = FXCollections.observableArrayList();
    private final ObservableList<FormationEleve> eleveDataTerminer = FXCollections.observableArrayList();
    private final ObservableList<FormationEleve> eleveDataNonTerminer = FXCollections.observableArrayList();
    private final ObservableList<Type> TypeCbx = FXCollections.observableArrayList();

    
//	private Date Date(String parameter) throws ParseException {
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//		Date date = sdf.parse(parameter);
//		
//		Date sqlDate = new Date(date.getTime());
//		return sqlDate;
//	}
//	
    VersementController vc;
    CertificatController cc;
    DashboardController dc;

    public void loadView(){
        LocalDate date = LocalDate.now();
        dpNaissance.setValue(date);
        dpRentree.setValue(date);
        fillListeEleve();
        fillCbx();
    }
    
    @FXML
    void switchAnnee(ActionEvent event) {
//        if(tglAnnee.isSelected()){
//            txtAnnee.setDisable(false);
//            hbxDateNaiss.setDisable(true);
//        } else {
//            txtAnnee.setDisable(true);
//            hbxDateNaiss.setDisable(false);
//        }
    }
    
    @FXML
    void switchTermine(ActionEvent event) {
 
        if(!termine.isSelected()){
            SortedList<FormationEleve> sortedList = new SortedList<>(eleveDataNonTerminer);
            sortedList.comparatorProperty().bind(listeEleve.comparatorProperty());
            listeEleve.setItems(sortedList);
        }
        if(termine.isSelected()){
            SortedList<FormationEleve> sortedList = new SortedList<>(eleveDataTerminer);
            sortedList.comparatorProperty().bind(listeEleve.comparatorProperty());
            listeEleve.setItems(sortedList);
        }
    }
    @FXML
    void showAll(ActionEvent event) {
            listeEleve.setItems(eleveData);
    }
    
    void init(VersementController vrsController, CertificatController crtController, DashboardController dashController) {
        vc = vrsController;
        cc = crtController;
        dc = dashController;
   }
    
    private void fillListeEleve() {
//    	listeEleve.setItems(null);
        eleveDataTerminer.clear();
        eleveDataNonTerminer.clear();
        eleveData.clear();

    	try {
        	PreparedStatement preparedStmt = null;
        	ResultSet rs = null;
    	    Connection con = ConnectionUtil.conDB();
//    	    String query = "SELECT *, CONCAT(eleve.eleve_nom, ' ',eleve.eleve_prenom)"
    	    String query = "SELECT *, eleve.eleve_nom"
    	    		+ " as 'nom'"
    	    		+ " FROM ((formationeleve JOIN eleve ON formationeleve.eleve_id=eleve.eleve_id) JOIN formation ON formationeleve.form_id=formation.form_id) JOIN type ON type.type_id=formationeleve.type_id";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
                            eleveData.add(new FormationEleve(rs.getInt("eleve_id"), rs.getString("nom"), rs.getString("eleve_nom"), rs.getString("eleve_prenom"), rs.getString("eleve_date_naiss"), rs.getString("eleve_lieu_naiss"), rs.getString("eleve_adresse"),
						rs.getString("eleve_tel"), rs.getString("sexe") ,rs.getInt("form_id"),
						rs.getString("form_nom"), rs.getInt("type_id"),rs.getString("type_nom"), rs.getString("date_rentree"), rs.getString("terminer"), rs.getBoolean("certificat"), rs.getBoolean("eleve_annee_vers")));
                            if(rs.getBoolean("certificat") == true)
				eleveDataTerminer.add(new FormationEleve(rs.getInt("eleve_id"), rs.getString("nom"), rs.getString("eleve_nom"), rs.getString("eleve_prenom"), rs.getString("eleve_date_naiss"), rs.getString("eleve_lieu_naiss"), rs.getString("eleve_adresse"),
						rs.getString("eleve_tel"), rs.getString("sexe") ,rs.getInt("form_id"),
						rs.getString("form_nom"), rs.getInt("type_id"),rs.getString("type_nom"), rs.getString("date_rentree"), rs.getString("terminer"), rs.getBoolean("certificat"), rs.getBoolean("eleve_annee_vers")));
                            else if(rs.getBoolean("certificat") == false)
				eleveDataNonTerminer.add(new FormationEleve(rs.getInt("eleve_id"), rs.getString("nom"), rs.getString("eleve_nom"), rs.getString("eleve_prenom"), rs.getString("eleve_date_naiss"), rs.getString("eleve_lieu_naiss"), rs.getString("eleve_adresse"),
						rs.getString("eleve_tel"), rs.getString("sexe") ,rs.getInt("form_id"),
						rs.getString("form_nom"), rs.getInt("type_id"),rs.getString("type_nom"), rs.getString("date_rentree"), rs.getString("terminer"), rs.getBoolean("certificat"), rs.getBoolean("eleve_annee_vers")));
			}
			con.close();
			preparedStmt.close();
			rs.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	tcFormation.setCellValueFactory(new PropertyValueFactory<FormationEleve, String>("formation_nom"));
    	tcNom.setCellValueFactory(new PropertyValueFactory<FormationEleve, String>("eleve_noms"));
    	tcNum.setCellValueFactory(new PropertyValueFactory<FormationEleve, Integer>("eleve_id"));
    	tcRentree.setCellValueFactory(new PropertyValueFactory<FormationEleve, String>("date_rentree"));
    	tcType.setCellValueFactory(new PropertyValueFactory<FormationEleve, String>("type_nom"));
    	tcTermine.setCellValueFactory(new PropertyValueFactory<FormationEleve, String>("terminer"));
    	tcCertificat.setCellValueFactory(new PropertyValueFactory<FormationEleve, String>("certificat_str"));
        
    	listeEleve.setItems(eleveData);
    }

    @FXML
    void checkAge(ActionEvent event) {
//        int age = LocalDate.now().getYear() - dpNaissance.getValue().getYear();
//        lblAge.setText(""+age);
    }
    
    @FXML
    private void setform() {
    	FormationEleve fe = listeEleve.getSelectionModel().getSelectedItem();
    	txtAdresse.setText(fe.getEleve_adresse());
    	txtLieuNaissance.setText(fe.getEleve_lieu_naiss());
    	txtNom.setText(fe.getEleve_nom());
    	txtPrenom.setText(fe.getEleve_prenom());
    	txtTel.setText(fe.getEleve_tel());
    	dpNaissance.setValue(LocalDate.parse(fe.getEleve_date_naiss()));
    	dpRentree.setValue(LocalDate.parse(fe.getDate_rentree_sql()));
        lblAge.setText(""+fe.getAge());
    	//System.out.println(fe.getDate_rentree_sql());
        if(fe.getAnnee_vers()){
            tglAnnee.setSelected(true);
            //txtAnnee.setVisible(true);
        } else {
            tglAnnee.setSelected(false);
        }
        
    	int indexSex = 0;
    	int indexType = 0;
    	int indexForm = 0;
    	for(Formation x : FormData) {
    		if(x.getForm_id() == fe.getFormation_id()) {
    			break;
    		}
    		indexForm++;
    	}
    	
    	for(Type t : TypeCbx) {
    		if(t.getType_id() == fe.getType_id()) {
    			break;
    		}
    		indexType++;
    	}
    	for(FormationEleve e : eleveData) {
    		if(e.getSexe().equals(fe.getSexe())) {
    			break;
    		}
    		indexSex++;
    	}
    	
    	cbxFormation.getSelectionModel().select(indexForm);
    	cbxSex.getSelectionModel().select(indexSex);
    	cbxType.getSelectionModel().select(indexType);
    	
    }
    
     @FXML
     public void search() throws SQLException, Exception{
         
         FilteredList<FormationEleve> filterData = new FilteredList<>(eleveData,p->true);
         txtSearch.textProperty().addListener((observable,oldvalue,newvalue)->{
         
            filterData.setPredicate( produit->{
                
                if(newvalue == null || newvalue.isEmpty()){
                	
                    return true;
                }
                String typedText = newvalue.toLowerCase();
                
                if(produit.getFormation_nom().toLowerCase().contains(typedText)){
                     return true;
                }
                if(produit.getDate_rentree().toLowerCase().contains(typedText)){
                     return true;
                }
                if(produit.getEleve_noms().toLowerCase().contains(typedText)){
                     return true;
                }               
                if( new String().valueOf(produit.getEleve_id()).toLowerCase().contains(typedText)){
                     return true;
                }
                if(produit.getType_nom().toLowerCase().contains(typedText)){
                     return true;
                }
                return false;
            });
            
            SortedList<FormationEleve> sortedList = new SortedList<>(filterData);
            sortedList.comparatorProperty().bind(listeEleve.comparatorProperty());
            listeEleve.setItems(sortedList);
            
        });
     }
    @FXML
    void addEleve() {
        
        if(txtNom.getText().length() == 0 || txtLieuNaissance.getText().length() == 0
                || txtAdresse.getText().length() == 0 || cbxSex.getSelectionModel().getSelectedIndex() < 0
                || cbxType.getSelectionModel().getSelectedIndex() < 0 || cbxFormation.getSelectionModel().getSelectedIndex() < 0){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setContentText("Veuillez remplire toutes les informations");
            alert.setHeaderText(null);
            alert.show();
        } else {
	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation");
    	alert.setContentText("Ajouter " + txtNom.getText() + " "+ txtPrenom.getText() + " dans la formation "+cbxFormation.getSelectionModel().getSelectedItem().getForm_nom()+" ?");
    	alert.setHeaderText(null);
    	
    	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
    	
    	if(action.get() == javafx.scene.control.ButtonType.OK) {
    	try {
    		PreparedStatement preparedStmt = null;
    	    Connection con = ConnectionUtil.conDB();
    	    con.setAutoCommit(false);
    	    
    	    String query1 = "INSERT INTO eleve (eleve_nom, eleve_prenom, eleve_date_naiss, eleve_lieu_naiss, "
    	    		+ "eleve_adresse, eleve_tel, sexe, date_rentree, eleve_annee_vers) VALUES(?,?,?,?,?,?,?,?,?);";
    	    String query2 = "INSERT INTO formationeleve (type_id, form_id, eleve_id)"
    	    		+ " VALUES(?, ?, (SELECT MAX(eleve_id) FROM eleve));";
//    	    String query = "START TRANSACTION;"
//    	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//    	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//    	    		"ROLLBACK;";
			preparedStmt = con.prepareStatement(query1);
			preparedStmt.setString(1, txtNom.getText());
			preparedStmt.setString(2, txtPrenom.getText());
			preparedStmt.setString(3, dpNaissance.getValue().toString());
			preparedStmt.setString(4, txtLieuNaissance.getText());
			preparedStmt.setString(5, txtAdresse.getText());
			preparedStmt.setString(6, txtTel.getText());
			preparedStmt.setString(7, cbxSex.getSelectionModel().getSelectedItem());
			preparedStmt.setString(8, dpRentree.getValue().toString());
			preparedStmt.setBoolean(9, tglAnnee.isSelected());
			
			preparedStmt.executeUpdate();

			preparedStmt = con.prepareStatement(query2);
			preparedStmt.setInt(1, cbxType.getSelectionModel().getSelectedItem().getType_id());
			preparedStmt.setInt(2, cbxFormation.getSelectionModel().getSelectedItem().getForm_id());
			    			
			preparedStmt.executeUpdate();

			con.commit();
			
			con.close();
			preparedStmt.close();
			
			System.out.println("Module : Add succesfully !! ");
			//FormData.add(new Formation(txtFormation.getText()));
			//fillModule();

		} catch (Exception e) {
                    //Alert alert = new Alert(AlertType.ERROR);
                    alert.alertTypeProperty().set(AlertType.ERROR);
                    alert.setTitle("Erreur");
                    alert.setContentText("Une erreur s'est produite. Veuillez remplire toutes les informations...");
                    alert.setHeaderText(null);
                    alert.show();
                    e.printStackTrace();
		}
    	refreshTable();
        vc.loadView();
        dc.loadView();
        cc.loadView();
//    	txtFormation.clear();
//    	txtDroit.clear();
//    	txtNomModule.clear();
    	cbxFormation.getSelectionModel().clearSelection();;
    	cbxType.getSelectionModel().clearSelection();
	}
        }
        
    }

	
	private void clearForm(){
		txtAdresse.clear();
		txtLieuNaissance.clear();
		txtNom.clear();
		txtPrenom.clear();
		txtTel.clear();
		LocalDate date = LocalDate.of(1990, 01, 01);
		dpNaissance.setValue(date);
                lblAge.setText("..");
	}
	@FXML
    private void refreshTable() {
    	FormData.clear();
    	eleveData.clear();
    	TypeCbx.clear();
    	//TypeCbx.clear();
    	fillListeEleve();
    	fillCbx();
    	clearForm();
	}

    @FXML
    private void editEleve() {
        
        if(txtNom.getText().length() == 0 || txtLieuNaissance.getText().length() == 0
                || txtAdresse.getText().length() == 0 || cbxSex.getSelectionModel().getSelectedIndex() < 0
                || cbxType.getSelectionModel().getSelectedIndex() < 0 || cbxFormation.getSelectionModel().getSelectedIndex() < 0){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setContentText("Veuillez remplire toutes les informations");
            alert.setHeaderText(null);
            alert.show();
        } else {
    	FormationEleve fe = listeEleve.getSelectionModel().getSelectedItem();		
		Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation");
    	alert.setContentText("Appliquer les modifications ?");
    	alert.setHeaderText(null);
    	
    	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
    	
    	if(action.get() == javafx.scene.control.ButtonType.OK) {
    	try {
    		PreparedStatement preparedStmt = null;
    	    Connection con = ConnectionUtil.conDB();
    	    con.setAutoCommit(false);
    	    
    	    String query1 = "UPDATE eleve SET eleve_nom=?,eleve_prenom=?,eleve_date_naiss=?,"
    	    		+ "eleve_lieu_naiss=?,eleve_adresse=?,eleve_tel=?,sexe=?,"
    	    		+ "date_rentree=?, eleve_annee_vers =? WHERE eleve_id=?;";
    	    
    	    String query2 = "UPDATE formationeleve SET type_id=?, form_id=?"
    	    		+ " WHERE eleve_id=?";
//    	    String query = "START TRANSACTION;"
//    	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//    	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//    	    		"ROLLBACK;";

			preparedStmt = con.prepareStatement(query2);
			preparedStmt.setInt(1, cbxType.getSelectionModel().getSelectedItem().getType_id());
			preparedStmt.setInt(2, cbxFormation.getSelectionModel().getSelectedItem().getForm_id());
			preparedStmt.setInt(3, fe.getEleve_id());
    			
			preparedStmt.executeUpdate();
    	    
			preparedStmt = con.prepareStatement(query1);
			preparedStmt.setString(1, txtNom.getText());
			preparedStmt.setString(2, txtPrenom.getText());
			preparedStmt.setString(3, dpNaissance.getValue().toString());
			preparedStmt.setString(4, txtLieuNaissance.getText());
			preparedStmt.setString(5, txtAdresse.getText());
			preparedStmt.setString(6, txtTel.getText());
			preparedStmt.setString(7, cbxSex.getSelectionModel().getSelectedItem());
			preparedStmt.setString(8, dpRentree.getValue().toString());
			preparedStmt.setBoolean(9, tglAnnee.isSelected());
			preparedStmt.setInt(10, fe.getEleve_id());
			preparedStmt.executeUpdate();
                        
			con.commit();
			
			con.close();
			preparedStmt.close();
			
			System.out.println(tglAnnee.isSelected());
			System.out.println("Module : Edited succesfully !! ");
			//FormData.add(new Formation(txtFormation.getText()));
			//fillModule();

		} catch (Exception e) {
			e.printStackTrace();
		}
    	refreshTable();
        vc.loadView();
        dc.loadView();
        cc.loadView();
//    	txtFormation.clear();
//    	txtDroit.clear();
//    	txtNomModule.clear();
    	cbxFormation.getSelectionModel().clearSelection();;
    	cbxType.getSelectionModel().clearSelection();
	}
        }

    }

    @FXML
    void delEleve() {
        if(listeEleve.getSelectionModel().isEmpty()){
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setContentText("Veuillez remplire toutes les informations");
            alert.setHeaderText(null);
            alert.show();
        } else {
    	FormationEleve fe = listeEleve.getSelectionModel().getSelectedItem();		
		Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Confirmation");
    	alert.setContentText("Supprimer vraiment ?");
    	alert.setHeaderText(null);
    	
    	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
    	
    	if(action.get() == javafx.scene.control.ButtonType.OK) {
    	try {
    		PreparedStatement preparedStmt = null;
    	    Connection con = ConnectionUtil.conDB();
    	    con.setAutoCommit(false);
    	    
    	    String query1 = "DELETE FROM eleve WHERE eleve_id=?;";
    	    String query2 = "DELETE FROM formationeleve WHERE eleve_id=?";
//    	    String query = "START TRANSACTION;"
//    	    		+ "INSERT INTO module (module_nom, form_id) VALUES(?,?);"
//    	    		+ "INSERT INTO moduletype (module_id, type_id, droit) VALUES((SELECT MAX(module_id) FROM module), ?, ?);\n" + 
//    	    		"ROLLBACK;";
			preparedStmt = con.prepareStatement(query2);
			preparedStmt.setInt(1, fe.getEleve_id());
			
			preparedStmt.executeUpdate();

			preparedStmt = con.prepareStatement(query1);
			preparedStmt.setInt(1, fe.getEleve_id());
    			
			preparedStmt.executeUpdate();

			con.commit();
			
			con.close();
			preparedStmt.close();
			
			System.out.println("Module : Deleted succesfully !! ");
			//FormData.add(new Formation(txtFormation.getText()));
			//fillModule();

		} catch (Exception e) {
			e.printStackTrace();
		}
    	refreshTable();
        vc.loadView();
        dc.loadView();
        cc.loadView();
//    	txtFormation.clear();
//    	txtDroit.clear();
//    	txtNomModule.clear();
    	cbxFormation.getSelectionModel().clearSelection();;
    	cbxType.getSelectionModel().clearSelection();
	}
    }
    }
    /*
     * Fill cbxs
     */
    private void fillCbx() {
    	fillSexe();
    	fillFormation();
    	fillType();
    }
    private void fillSexe() {
    	cbxSex.setItems(sexe);
    }
    
    private void fillFormation() {
    	FormData.clear();
		StringConverter<Formation> converter = new StringConverter<Formation>() {

			@Override
			public Formation fromString(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String toString(Formation obj) {
				// TODO Auto-generated method stub
				return obj.getForm_nom();
			}
			
		};
		
    	PreparedStatement preparedStmt = null;
    	ResultSet rs = null;
    	try {
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * from formation";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				//Formation f = new Formation(rs.getInt("form_id"), rs.getString("form_nom"));
				//System.out.println(f.getForm_id() + " - "+ f.getForm_nom());
				FormData.add(new Formation(rs.getInt(1), rs.getString(2)));
			}
			con.close();
			preparedStmt.close();
			rs.close();
						
			cbxFormation.setConverter(converter);
			cbxFormation.setItems(FormData);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    private void fillType() {
        TypeCbx.clear();
		// TODO Auto-generated method stub
		StringConverter<Type> converter = new StringConverter<Type>() {

			@Override
			public Type fromString(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String toString(Type obj) {
				// TODO Auto-generated method stub
				return obj.getType_nom();
			}
		};
		
    	PreparedStatement preparedStmt = null;
    	ResultSet rs = null;
    	try {
    	    Connection con = ConnectionUtil.conDB();
    	    String query = "SELECT * from type";
			preparedStmt = con.prepareStatement(query);
			rs = preparedStmt.executeQuery();
			while(rs.next()) {
				TypeCbx.add(new Type(rs.getInt(1), rs.getString(2)));
			}
			con.close();
			preparedStmt.close();
			rs.close();
						
			cbxType.setConverter(converter);
			cbxType.setItems(TypeCbx);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		LocalDate date = LocalDate.now().minusYears(20);
		dpNaissance.setValue(date);
                dpRentree.setValue(LocalDate.now());
		fillListeEleve();
		fillCbx();
                
            dpNaissance.valueProperty().addListener((ov, oldValue, newValue) -> {
                int age = LocalDate.now().getYear() - dpNaissance.getValue().getYear();
                lblAge.setText(""+age);
            });
        txtTel.textProperty().addListener(new ChangeListener<String>() {

                    @Override
                    public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                            if(!txtTel.getText().matches("\\d{1,15}")) {
                                    //txtStock.setStyle("-fx-background-color: #FF514F");
                                    txtTel.setText("");
                            }
                            else {
                                    txtTel.setStyle("-fx-border-color: WHITE");
                            }
                    }
            });	        }
}
