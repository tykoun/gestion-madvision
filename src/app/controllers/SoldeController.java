/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.utils.ConnectionUtil;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author Finaritra
 */
public class SoldeController implements Initializable{

    @FXML
    private JFXTextField txtMontant;

    @FXML
    private JFXButton btnCancel;

    @FXML
    private JFXButton btnSave;

    @FXML
    private Label lblErreur;

    @FXML
    void close() {
        Stage stage;
	stage = new Stage();
        stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }

    @FXML
    void saveSolde(ActionEvent event) {
    	if(txtMontant.getText().equals("")) {
    		lblErreur.setVisible(true);
    	} else {
    		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        	alert.setTitle("Confirmation");
        	alert.setContentText("Ajouter un solde initial ?");
        	alert.setHeaderText(null);
        	
        	Optional<javafx.scene.control.ButtonType> action = alert.showAndWait();
        	
        	if(action.get() == javafx.scene.control.ButtonType.OK) {
        	try {
        		PreparedStatement preparedStmt = null;
        	    Connection con = ConnectionUtil.conDB();
        	    String query = "INSERT INTO solde (solde_init, solde_date) values(?, CURRENT_DATE)";
    			preparedStmt = con.prepareStatement(query);
    			preparedStmt.setInt(1, Integer.parseInt(txtMontant.getText()));
    			
    			preparedStmt.execute();
    			preparedStmt.close();
    			con.close();
    			System.out.println("Added succesfully !! ");
    			//FormData.add(new Formation(txtFormation.getText()));
    			//stage.close();
    		} catch (Exception except) {
    			// TODO Auto-generated catch block
    			except.printStackTrace();
    		}
                    close();
        	}
    	}
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txtMontant.textProperty().addListener(new ChangeListener<String>() {

                    @Override
                    public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
                            if(!txtMontant.getText().matches("\\d{1,15}")) {
                                    //txtStock.setStyle("-fx-background-color: #FF514F");
                                    txtMontant.setText("");
                            }
                            else {
                                    txtMontant.setStyle("-fx-border-color: WHITE");
                            }
                    }
            });
    }
    
}
