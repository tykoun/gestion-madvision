/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
//import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.ibatis.jdbc.ScriptRunner;

/**
 *
 * @author Finaritra
 */
public class GMadvision extends Application  implements EventHandler<Event>{
    Stage stage = new Stage();
//    Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        
    @Override
    public void start(Stage primaryStage) throws Exception {
            Path path = Paths.get("C:\\db\\madvision\\");
            Path path2 = Paths.get("C:\\db\\data.sqlite");
            Files.createDirectories(path);


//            Path path1 = Paths.get("src/app/data.sqlite");
            Path path1 = Paths.get("data.sqlite");
            System.out.println(path1.toAbsolutePath());
////            Files.createDirectories(path);
////            Files.createDirectories(path);   
            File file = new File("C:\\db\\data.sqlite");
            if(!file.exists()){
                Files.copy(path1, path2);		
            }

        //Parent root = FXMLLoader.load(getClass().getResource("App.fxml"));
        //Parent root = FXMLLoader.load(getClass().getResource("views/Login.fxml"));
        Parent root = FXMLLoader.load(getClass().getResource("views/Splash.fxml"));
        
        Scene scene = new Scene(root);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
        
        
        stage = primaryStage;
        
//		try {
//			FXMLLoader loader = new FXMLLoader(getClass().getResource("views/Login.fxml"));
//			AnchorPane pane = loader.load();
//			LoginController controller = loader.getController();
//			controller.main(stage, this);
//			Scene scene = new Scene(pane);
//			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
//			
//			stage.initStyle(StageStyle.UNDECORATED);
//			stage.setScene(scene);
//			stage.show();
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
    }

    	public void closeWindow() {
		stage.close();
	}
	
	public void closeStage() {
		//Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		try {
			FXMLLoader loader2 = new FXMLLoader(getClass().getResource("App.fxml"));
			AnchorPane pane = loader2.load();
			Scene scene = new Scene(pane);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			stage.setScene(scene);
//			stage.setX(visualBounds.getMinX());
//			stage.setY(visualBounds.getMinY());
//			stage.setWidth(visualBounds.getWidth());
//			stage.setHeight(visualBounds.getHeight());

			stage.show();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		//stage.close();
	}
    
        
    /**
     * Connect to a sample database
     *
     * @param fileName the database file name
     */
    public static void createNewDatabase(String fileName) {
 
        String sql = "CREATE DATABASE IF NOT EXISTS DEMODB";
        
        try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con2 = DriverManager.getConnection("jdbc:mysql://localhost/", "root", "");
                        PreparedStatement stmt = con2.prepareStatement(sql);
                        stmt.execute();
                        con2.close();
                        
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/demodb", "root", "");
             
                       //Initialize the script runner
                        ScriptRunner sr = new ScriptRunner(con);
                        //Creating a reader object
                        
                        //Reader reader = new BufferedReader(new FileReader("C:\\db\\madvision\\madvision.sql"));
                        Reader reader = new BufferedReader(new FileReader("C:\\db\\madvision\\madvision.sql"));
                        //Reader reader = new BufferedReader(new FileReader(url.getPath()));
                        //Running the script
                        sr.runScript(reader);                   
                        con.close();
                        stmt.close();                        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
    }        
    
    
    /**
     * Create a new table in the test database
     *
     */
    public static void createNewTables() {
        // SQLite connection string
        String url = "jdbc:sqlite:C://sqlite/db/tests.db";
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS `categorie` (\n" +
        "  `cat_id` integer NOT NULL,\n" +
        "  `cat_designation` varchar(50) NOT NULL,\n" +
        "  `trans_type_id` int(11) NOT NULL,\n" +
        "  PRIMARY KEY (`cat_id`),\n" +
        "  KEY `categorie_ibfk_1` (`trans_type_id`)\n" +
        ") ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //createNewDatabase("data.sqlite");
        launch(args);
    }

    @Override
    public void handle(Event event) {
	System.out.println(event.getEventType());
    }
    
}
